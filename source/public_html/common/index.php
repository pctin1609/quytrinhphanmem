<?php

// This file can't be called directly
defined('WWW') or die('Invalid request!');

define('YII_DEBUG', true);
define('YII_TRACE_LEVEL', 1);

$protectedDir = WWW . '/../../protected/';
$publicDir = WWW . '/../../public_html/';

$comDir = $protectedDir . 'common';
$appDir = $protectedDir . 'apps/' . APP_NAME;

//$uploadDir = $publicDir = $publicDir . "/common/upload/";
define('uploadPath', $publicDir . "common/upload/");

// Include yii framework
require($protectedDir . 'globals.php');
require($comDir . '/vendors/yii/1.1.14/yii.php');

// Path alias
Yii::setPathOfAlias('www', WWW);
Yii::setPathOfAlias('common', $comDir);
Yii::setPathOfAlias('app', $appDir);

// Load configs
$appConfigDir = $appDir . '/configs/';
$comConfigDir = $comDir . '/configs/';

$baseName = 'main.php';
$localName = 'local/' . $baseName;

$appBaseConfigs = require($appConfigDir . $baseName);
$appLocalConfigs = array();
if (is_file($appConfigDir . $localName))
    $appLocalConfigs = require($appConfigDir . $localName);

$comBaseConfigs = require($comConfigDir . $baseName);
$comLocalConfigs = array();
if (is_file($comConfigDir . $localName))
    $comLocalConfigs = require($comConfigDir . $localName);

$config = CMap::mergeArray($comBaseConfigs, $comLocalConfigs, $appBaseConfigs, $appLocalConfigs);

// Create and run application
Yii::createWebApplication($config)->run();
