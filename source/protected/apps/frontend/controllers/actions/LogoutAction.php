<?php

class LogoutAction extends CAction
{
    public function run()
    {
		//remove current order
	    user()->logout();
        $this->controller->redirect(array('site/index'));
    }
}