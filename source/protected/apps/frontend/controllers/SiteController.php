<?php

class SiteController extends Controller
{
    public $meta_description;

    public function actions()
    {
        $maps = parent::actions();
        $maps['sendMail'] = 'common.extensions.ceres.actions.SendMailAction';
        return $maps;
    }

    public function actionIndex()
    {
        $this->pageTitle = 'Home';
        $criteria = new CDbCriteria();
        $criteria->condition = 'status != ' . File::STATUS_DELETE;
        $criteria->order = 'created_date DESC';

        $dataProvider = new CActiveDataProvider('Film', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize')
            )
        ));

        $this->render('index', array('dataProvider' => $dataProvider));
    }
    
    public function actionResetPassword()
    {
        $this->layout = 'main';
        $this->cssFile = 'reset';
        $model = new ResetPasswordForm();

        if (isset($_POST['ResetPasswordForm'])) {
            $model->attributes = $_POST['ResetPasswordForm'];
            if ($model->validate()) {
                $user = User::model()->find('email = "' . $model->email . '" AND status = ' . User::STATUS_ACTIVE);
                if ($user) {
                    $user->activation_key = Utils::md5(time());
                    if ($user->save()) {
                        user()->setFlash('success', 'An email explaining how to reset your password has been sent to you!');

                        $params['to'] = array($user->getFullName(), $user->email);
                        $params['data'] = array(
                            "RESET_LINK" => $this->createAbsoluteUrl("user/forgotPassword", array('fg' => $user->activation_key))
                        );

                        CeresMail::queue('forgotPass', $params);

                        $this->redirect("resetPassword");
                    }
                } else {
                    user()->setFlash('error', 'Sorry, but we can not find this email address in our records. You can try another email address, or contact us for help!');
                }
            }
        }

        $this->render('reset_password', array('model' => $model));
    }

    public function actionForgotPassword()
    {
        $forgotPass = app()->request->getParam('fg', '');
        $user = User::model()->find('activation_key = "' . $forgotPass . '" AND status = ' . User::STATUS_ACTIVE);
        if ($user) {
            $this->layout = 'main';
            $this->cssFile = 'signup';
            $model = new ForgotPasswordForm();

            if (isset($_POST['ForgotPasswordForm'])) {
                $model->attributes = $_POST['ForgotPasswordForm'];
                if ($model->validate()) {
                    $user->password = Utils::md5($model->password);
                    $user->activation_key = NULL;
                    if ($user->save()) {
                        $params['to'] = array($user->getFullName(), $user->email);
                        $params['data'] = array(
                            "PASSWORD" => $model->password
                        );
                        CeresMail::queue('changePass', $params);

                        user()->setFlash('success', 'Update new password sucessful. You can sign in here!');
                        $this->redirect(array('user/login'));
                    }
                }
            }

            $this->render('forgot_password', array('model' => $model));
        } else {
            $this->redirect(param('siteUrl'));
        }
    }

    public function actionError()
    {
        if ($error = app()->errorHandler->error) {
            if (app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', array('error' => $error));
            }
        }
    }

    public function actionLogin()
    {
        $model=new LoginForm;
        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
               $this->redirect(url('site/index'));
        }
    }
}