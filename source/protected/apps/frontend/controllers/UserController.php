<?php

class UserController extends Controller
{
    public function actionRegister() {
        $model = new RegisterForm();
        $user = new User();
        if (isset($_POST['RegisterForm'])) {

            $model->attributes = $_POST['RegisterForm'];
            if ($model->validate()) {
                $user->attributes = $model->attributes;
                $user->password = $password = randomString(8);
                if ($user->save()) {
                    user()->setFlash('success', $model->email . ' has been created successfully! Check your Email to get your password');
                    $params = array(
                        'to' => array($model->email, $model->email),
                        'data' => array(
                            'EMAIL' => $model->email,
                            'PASSWORD' => $password,
                            'FULLNAME' => $model->first_name . ' ' . $model->last_name,
                        ),
                    );
                    CeresMail::queue('userCreate', $params);
                }
            }
        }

        $this->render('_form_register', array('model' => $model));
    }

    public function actionLogin()
    {
        //var_dump(Utils::md5('123456'));
        //die();
        $model = new LoginForm();
        
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $model->username = $_POST['LoginForm']['username'];
            
            if ($model->validate() && $model->login())
                $this->redirect(url('/'));
        }
        
        $this->render('login', array('model' => $model));
    }

    public function actionProfile()
    {
        $user = User::model()->findByPk(user()->id);
        
         if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            
            if ($user->save())
                Yii::app()->user->setFlash('success', 'Update profile successfully');
        }
        
        $this->render('profile',array('model' => $user));
    }
    
    public function actionDownLoadHistory()
    {
        $criteria = new CDbCriteria();        
        $criteria->order = 'created_date DESC';
        $criteria->condition = 'user_id = ' . user()->id;
        
        $dataProvider = new CActiveDataProvider('DownloadHistory', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10
            )
        ));
        
        $this->render('downloadHistory', array('dataProvider' => $dataProvider));
    }
    
    public function actionWatchTrailerHistory()
    {
        $criteria = new CDbCriteria();        
        //$criteria->order = 'created_date DESC';
        $criteria->condition = 'user_id = ' . user()->id;
        
        $dataProvider = new CActiveDataProvider('WatchTrainerHistory', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10)
        ));
        $this->render('watchTrailerHistory', array('dataProvider' => $dataProvider));
    }
    
    public function actionWatchFullHistory()
    {
        $criteria = new CDbCriteria();        
        //$criteria->order = 'created_date DESC';
        $criteria->condition = 'user_id = ' . user()->id;
        
        $dataProvider = new CActiveDataProvider('WatchFullHistory', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10)
        ));
        $this->render('watchFulllHistory', array('dataProvider' => $dataProvider));
    }
            
    public function actionPaymentHistory()
    {
        $criteria = new CDbCriteria();        
        //$criteria->order = 'created_date DESC';
        $criteria->condition = 'id_User = ' . user()->id;
        
        $dataProvider = new CActiveDataProvider('Paymenthistory', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 10)
        ));
        $this->render('paymentHistory', array('dataProvider' => $dataProvider));
    }
    
    public function actionBuyCredit()
    {
        $user = User::model()->findByPk(user()->id);
        
        if (isset($_GET['token'])) {
            $paypal = new Paypal();

            $params = array(
                'PAYERID' => $_GET['PayerID'],
                'TOKEN' => $_GET['token'],
                'CURRENCYCODE' => param('CURRENCY_CODE'),
                'PAYMENTACTION' => 'Sale',
                'AMT' => app()->session['amount'],
            );

            $response = $paypal->request('DoExpressCheckoutPayment', $params);
            
            if ($response['ACK'] == "Success") {
                $user->credit += intval(app()->session['amount'] / param('creditPrice'));
                if ($user->save()) {
                    app()->user->setFlash('success', 'Purchase Credit Successfull.');
                    $this->redirect(url('user/profile'));
                }
            } else
                Yii::log(json_encode ($response), CLogger::LEVEL_ERROR);
            
        }
        
        if (isset($_POST['credit'])) {
            $amount = number_format($_POST['credit'] * param('creditPrice'), 2);
            $paypal = new Paypal();
                        
            //Our request parameters
            $urls = array(
                'RETURNURL' => url('/user/buyCredit') . '/',
                'CANCELURL' => url('/shoppingCart/view') . '/',
            );

            $parrams = array(
                'PAYMENTACTION' => 'Sale',
                'AMT' => $amount,
                'CURRENCYCODE' => param('paypal', 'CURRENCY_CODE'),
            );

            $response = $paypal->request('SetExpressCheckout', $urls + $parrams);
           
            if (is_array($response) && $response['ACK'] == 'Success') { //Request successful
                $token = $response['TOKEN'];
                app()->session['amount'] = $amount;
                if (param('paypal', 'PAYPAL_IS_TEST'))
                    header('Location: https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
                else
                    header('Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
            } else
                app()->user->setFlash('error', "Paypal error: " . $response['L_LONGMESSAGE0'] );
        }
        
        $this->render('credit', array('user' => $user));
    }
    
    public function actionChangePassword()
    {        
        $model = new ChangePasswordForm();
        $user = User::model()->findByPk(user()->id);
        
         if (isset($_POST['ChangePasswordForm'])) {
            $model->attributes = $_POST['ChangePasswordForm'];
            if ($model->validate()) {
                $user->password = Utils::md5($model->new_password);
                if ($user->save(false)) {
                    Yii::app()->user->setFlash('success', 'Update profile successfully');
                    $this->refresh();
                }
            }
        }
        
        $this->render('changePassword',array('model' => $model));
    }

}