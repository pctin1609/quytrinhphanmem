<?php

class FilmController extends Controller
{

    public function actionViewCat()
    {
        // get post
        //$id = Yii::app()->request->getPost(); 
        $id = Yii::app()->getRequest()->getQuery('id');

        if (isset($id)) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'status != ' . File::STATUS_DELETE . ' and category_id = ' . $id;
            $criteria->order = 'created_date DESC';

            $dataProvider = new CActiveDataProvider('Film', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => param('pageSize')
                )
            ));

            $this->render('view_cat', array('dataProvider' => $dataProvider->getData()));
        }
    }

    public function actionSearch()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'created_date DESC';

        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $query = $_GET['query'];
            $str = explode(" ", $query);
            foreach ($str as $v) {
                $criteria->compare("title", $v, true, 'OR');
                $criteria->compare("director", $v, true, 'OR');
                $criteria->compare("author", $v, true, 'OR');
            }
        }
        else
            $criteria->condition = 'id = 0';

        $dataProvider = new CActiveDataProvider('Film', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize'),
            )
        ));

        $this->render('index', array('dataProvider' => $dataProvider));
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'status != ' . File::STATUS_DELETE;
        $criteria->order = 'created_date DESC';

        $dataProvider = new CActiveDataProvider('Film', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize')
            )
        ));

        $this->render('index', array('dataProvider' => $dataProvider));
    }

    public function actionView()
    {
        $id = Yii::app()->getRequest()->getQuery('id');

        if (isset($id)) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'status != ' . File::STATUS_DELETE . ' AND id = ' . $id;
            $criteria->order = 'created_date DESC';

            $dataProvider = new CActiveDataProvider('Film', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => param('pageSize')
                )
            ));

            $this->render('view', array('dataProvider' => $dataProvider));
        }
    }

    public function actionWatch()
    {
        $id = Yii::app()->getRequest()->getQuery('id');

        if (isset($id)) {
            $criteria = new CDbCriteria();

            $criteria->condition .= 'status != ' . File::STATUS_DELETE . ' AND id=' . $id;
            $criteria->order = 'created_date DESC';

            $dataProvider = new CActiveDataProvider('Film', array(
                'criteria' => $criteria,
            ));

            $this->render('watch', array('dataProvider' => $dataProvider));
        }
    }

    //url('film/detail', array('id' => 1));
    public function actionDetail($id)
    {
        $film = Film::model()->findByPk($id);
        $this->render('detail', array('model' => $film));
    }

    public function actionDownload()
    {
        $id = Yii::app()->getRequest()->getQuery('id');

        if (isset($id)) {
            $criteria = new CDbCriteria();

            $criteria->condition = ' id=' . $id;
            $dataProvider = new CActiveDataProvider('File', array(
                'criteria' => $criteria,
            ));
            $data = $dataProvider->getData();
            foreach ($data as $file){
                $fPath = $file->source;
                $fExtension = $file->extension;
                $fName = $file->name;
            }
            
            $path = Utils::uploadPath($fPath);

            header("Content-type: {$fExtension}");
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false); // required for certain browsers
            header("Content-Disposition: attachment; filename=\"" . basename($fName) . "\";");
            header("Content-Transfer-Encoding: binary");
            //header("Content-Length: ".$fileSize);
            ob_clean();
            flush();
            readfile($path);
            exit;
        }
    }

}

