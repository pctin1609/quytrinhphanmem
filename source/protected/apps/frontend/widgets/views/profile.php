<h3>Contact Infomation</h3>
<div class="span8">
    <div class="row">
        <ul class="span4" style=" list-style: none; ">
            <?php if ($model->company_name != ""): ?>
            <h5><?php echo $model->getAttributeLabel('company_name'); ?></h5>
            <address>
                <p><?php echo $model->company_name; ?></p>
            </address>
            <?php endif;?>
           
            <?php if ($model->phone_contact != ""): ?>
            <h5><?php echo $model->getAttributeLabel('phone_contact'); ?></h5>
            <address>
                <p><?php echo $model->phone_contact; ?></p>
            </address>
            <?php endif;?>
        </ul>
        <ul class="span4">
            <?php if ($model->address != ""): ?>
            <h5><?php echo $model->getAttributeLabel('address'); ?></h5>
            <address>
                <p><?php echo $model->address; ?></p>
            </address>
            <?php endif;?>
            <?php if ($model->city != ""): ?>
            <h5><?php echo $model->getAttributeLabel('city'); ?></h5>
            <address>
                <p><?php echo $model->city; ?></p>
            </address>
            <?php endif;?>
        </ul>
    </div>
</div>