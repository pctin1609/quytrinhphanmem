<div id="box_sign">
    <div class="container">
        <div class="span12 box_wrapper">
            <div class="span12 box">
                <div>
                    <div class="head">
                        <h4>Create your account</h4>
                    </div>
                    <div class="form">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id'                   => 'user-form',
                            'enableAjaxValidation' => false
                        ));
                        ?>

                        <div class="rowText">
                            <?php echo $form->textField($model, 'email', array('size' => 50, 'maxlength' => 128, 'placeholder' => 'Email')); ?>
                            <?php echo $form->error($model, 'email'); ?>
                        </div>

                        <div class="rowText">
                            <?php echo $form->textField($model, 'first_name', array('size' => 50, 'maxlength' => 128, 'placeholder' => 'First Name')); ?>
                            <?php echo $form->error($model, 'first_name'); ?>
                        </div>

                        <div class="rowText">
                            <?php echo $form->textField($model, 'last_name', array('size' => 50, 'maxlength' => 128, 'placeholder' => 'Last Name')); ?>
                            <?php echo $form->error($model, 'last_name'); ?>
                        </div>

                        <div class="rowText">
                            <?php echo $form->passwordField($model, 'password', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'Password')); ?>
                            <?php echo $form->error($model, 'password'); ?>
                        </div>

                        <div class="rowText">
                            <?php echo $form->passwordField($model, 'password_repeat', array('size' => 32, 'maxlength' => 32, 'placeholder' => 'Confirm Password')); ?>
                            <?php echo $form->error($model, 'password_repeat'); ?>
                        </div>
                        
                        <?php if (CCaptcha::checkRequirements()) { ?>
                        <div class="rowText">
                            <?php echo $form->textField($model, 'verify_code', array('placeholder' => 'Verification Code')); ?>
                            <?php $this->widget('CCaptcha'); ?>
                            <?php echo $form->error($model, 'verify_code'); ?>
                        </div>
                        <?php } ?>

                        <div class="rowText button">
                            <?php echo CHtml::submitButton('Create', array('class' => 'btn')); ?>
                        </div>

                    <?php $this->endWidget(); ?>

                    </div><!-- form -->
                </div>
            </div>
        </div>
    </div>
</div>