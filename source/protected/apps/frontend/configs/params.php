<?php

return array(
    /**
     * Image thumb sizes
     */
    'thumb' => array(
        'banner' => array(
            '1170x240'
        ),
        'logo' => array(
            '163x34',
        ),
        'auction' => array(
            '180x135',
            '360x270',
            '570x348',
            '570x320'
        ),
        'sponsor' => array(
            '60x45',
            '155x116',
        )
    ),

    /**
     * Maximum auction which will be shown on site index
     */
    'featuredAuctions' => 5
); 
