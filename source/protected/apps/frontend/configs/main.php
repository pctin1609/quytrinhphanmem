<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'LearnOgy',
	'params' => require(dirname(__FILE__) . '/params.php'),
    'import' => array(
        'application.modules.hybridauth.controllers.*'
    ),
    'components' => array(
        'widgetFactory' => array(
            'widgets' => array(
                'ERedactorWidget' => array(
                    'options' => array(
                        'buttons' => array(
                            'formatting', '|', 'bold', 'italic', 'deleted', '|', 'link', '|', 'fontcolor', 'backcolor',
                            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'html'
                        )
                    )
                ),
                'XUpload' => array(
                    'formView' => 'app.views.layouts.partial.upload_form',
                    'downloadView' => 'app.views.layouts.partial.download_template',
                    'options' => array(
                        'maxFileSize' => 3000000,
                        'acceptFileTypes' => 'js:/\.(gif|jpg|jpeg|png)$/i'
                    )
                ),
                'CJuiDateTimePicker' => array(
                    'mode' => 'datetime',
                    'htmlOptions' => array('class' => 'span2'),
                    'language' => ''
                )
            )
        ),
        'user' => array(
            'loginUrl' => '/user/login',
        ),
        'request' => array(
            'class' => 'HttpRequest',
        ),
    ),
    'aliases' => array(
        'xupload' => 'common.widgets.xupload'
    ),
    'modules' => array(
        'hybridauth' => array(
            'baseUrl' => 'http://'. $_SERVER['SERVER_NAME'] . '/hybridauth', 
            'withYiiUser' => true, // Set to true if using yii-user
            'providers' => array (
                'openid' => array(
                    'enabled' => false
                ),
                'twitter' => array ( 
                    'enabled' => true,
                    'keys'    => array ( 
                        'key' => 'IqUpWKtblPlJmlfOMi7Uw', 
                        'secret' => 'sZtgTT4xGsGlGDbrrqemaJSRkxbvVAbZhOLP2YHbk' 
                    ) 
                ),
                'facebook' => array (
                    'enabled' => true,
                    'keys' => array (
                        'id' => '157053121140079', 
                        'secret' => '5ea5a57240f1d0e357a7dd16eee663e5'
                    ),
                    'scope' => 'email, publish_stream',
                    'display' => ''
                )
            )
        ),
    ),
);