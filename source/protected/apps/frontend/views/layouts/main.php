<?php cs()->registerCoreScript('jquery');?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> 
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $this->pageTitle ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/calendar.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/select2.css">
        <?php cs()->registerCoreScript('jquery')?>
        <!--[if lt IE 9]>
            <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
        <![endif]-->
    </head>

<body id="index">
		<header id="header">
            <?php $this->renderPartial('//layouts/partial/header') ?>
		</header>
		
        <div id="main">	
			<?php echo $content ?>
		</div>


        <script src="/js/vendor/bootstrap.min.js"></script>
        <script src="/js/vendor/fullcalendar.min.js"></script>
        <script src="/js/vendor/bootstrap-datepicker.js"></script>
        <script src="/js/vendor/calendar.js"></script>
		<script src="/js/vendor/select2.js"></script>
		<script src="/js/vendor/jquery.raty.js"></script>
        <script src="/js/application.js"></script>
    </body>
</html>