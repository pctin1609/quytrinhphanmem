            <div class="container">
				<h1 class=""><a href="<?php echo url("")?>">Learnogy</a></h1>
				<div class="col-lg-7 pull-right">
					<ul id="menu-top" class="nav navbar-nav navbar-right">
						<li><a class="btn btn-menu" href="#">Contact Us</a></li>
						<li><a class="btn btn-menu" href="#">We're Hiring</a></li>
                        <?php if(!user()->isGuest) { ?>
                            <li>
                                Hello,<?php echo user()->name?>
                                <a class="btn btn-menu btn-login" data-toggle="dropdown" href="<?php  echo url('site/logout');?>">Logout</a>
                            </li>
                        <?php } else { ?>
						<li class="dropdown"><a class="btn btn-menu btn-login" data-toggle="dropdown" href="#">Login</a>
							<?php $this->renderPartial("//layouts/partial/login_form")?>
						</li>
                        <?php } ?>
					</ul>
					<div class="navbar-header">
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="#"class="navbar-brand">Menu</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul id="menu" class="nav navbar-nav">
							<li><a href="#">Search for a Teacher</a></li>
							<li><a href="#">How It Works</a></li>
							<li><a href="#">FAQ</a></li>
                            <li><a href="<?php echo url("user/signup")?>">Sign-up (Free)</a></li>
						</ul>
						<ul id="language" class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a data-toggle="dropdown" href="#"><span class="icon"></span>English</a>
								<ul class="dropdown-menu">
									<li><a href="#">Vietnam</a></li>
									<li><a href="#">German</a></li>
								</ul>	
							</li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>