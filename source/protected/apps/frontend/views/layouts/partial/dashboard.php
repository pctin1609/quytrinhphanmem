﻿<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/calendar.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/select2.css">

    <!--[if lt IE 9]>
    <script src="js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
    <![endif]-->
</head>
<body id="dashboard">
<header id="header" class="clearfix">
    <div class="container">
        <h1 class="col-lg-3"><a href="#">Learnogy</a></h1>
        <div id="notification" class="col-lg-offset-2">
            <a href="#"><strong class="t-notification">Notifications</strong></a>
            <span class="pull-right nav-toggle">-</span>
            <div class="c-notification">It's cool, nothing new</div>
        </div>
        <div class="dropdown my-account">
            <a href="#" data-toggle="dropdown">
                <img class="avatar-small" src="/img/avatar-default.png" alt="Marco Nespeca">
                <span>Marco Nespeca</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu clearfix">
                <li class="active">
                    <a href="myacount">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span>Account</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span>Profile</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span>Help</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>
<nav id="nav" class="clearfix navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#"class="navbar-brand">Menu</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="dashboard.html">Dashboard</a></li>
                <li><a href="calendar-mycalendar.html">calendar</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="myteacher.html">My Teachers</a></li>
                <li><a href="#">Messages</a></li>
                <li><a href="#">Progress</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="enter-class">
                    <button type="submit" class="btn btn-default">Enter Classroom</button>
                </li>
                <li class="lesson-time">
                    <em>Your next lesson: Fri 31 May 20:00<br>
                        Lesson begins in: 1h, 47 min, 6 sec</em>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div id="main">
<div class="container">
<div id="dash-left" class="col-lg-9">
<div class="panel panel-default">
    <div class="panel-heading">
        <h3>My upcoming classes</h3>
    </div>
    <div class="panel-body">
        <p>No lessons booked yet. <a href="#">Book your next lesson right now</a></p>
    </div>
</div>
<div class="row">
    <div class="panel-item col-lg-6">
        <div id="myteacher" class="panel panel-default">
            <div class="panel-heading">
                <h3>My Teacher</h3>
            </div>
            <div class="panel-body">
                <div class="col-lg-6 top-20">
                    <p class="pull-left">
                        <a href="#">
                            <span class="flag"></span>
                            <span class="name">Marco</span>
                        </a>
                    </p>
                    <ul class="action-top pull-right">
                        <li><a href="mailto:abc@demo.com" class="mail">abc@demo.com</a></li>
                        <li><a href="#" class="delete">Delete</a></li>
                    </ul>
                    <img class="img-thumbnail" alt="" src="/img/teacher1.png">
                </div>
                <div class="col-lg-6 top-20 pull-right">
                    <img class="top-20 img-thumbnail" alt="" src="/img/find-teacher.png">
                    <a href="#" class="btn btn-default top-10 pull-right">Find a Teacher</a>
                </div>
                <div class="btn-bottom">
                    <a href="#" class="btn btn-default">Book a lessons</a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-item col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>My Account</h3>
            </div>
            <div class="panel-body">
                <p>You currently have <span class="color">0</span> lesson(s) remaining in your account.</p>
                <p>Number of lessons purchased: <span class="color">0</span><br>
                    Total lessons taken (and booked): <span class="color">0</span></p>
                <div class="btn-bottom"><a href="#" class="btn btn-default">Credit my account</a></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel-item col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>My Certifications</h3>
            </div>
            <div class="panel-body">
                <img src="/img/certifications.png" alt="">
                <a class="view-c" href="#">View certificate<span></span></a>
                <div class="btn-bottom"><a href="#" class="btn btn-default">View my certificate</a></div>
            </div>
        </div>
    </div>
    <div class="panel-item col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>My Messages</h3>
            </div>
            <div class="panel-body">
                <p>You can access your last messages here.</p>
                <p>You currently have <span class="color">0</span> new messages.</p>
                <div class="btn-bottom"><a href="#" class="btn btn-default">View my messages</a></div>
            </div>
        </div>
    </div>
</div>
<div id="courses" class="clearfix panel panel-default">
    <div class="panel-heading">
        <h3 class="pull-left">My Courses (last active)</h3>
        <div class="pull-right">
            <button class="btn btn-default" type="submit">See all my courses</button>
        </div>
    </div>
    <div class="panel-body">
        <div class="image col-lg-2">
            <img src="/img/speakout.png" alt="">
        </div>
        <ul class="col-lg-10">
            <li><span class="col-lg-5"><strong>Courses name:</strong></span><span class="col-lg-7">Unit 1.1</span></li>
            <li><span class="col-lg-5"><strong>Courses code:</strong></span><span class="col-lg-7">SO-UI-1.1</span></li>
            <li><span class="col-lg-5"><strong>Category:</strong></span><span class="col-lg-7">../SpeakOut/Upper-Intermediate</span></li>
            <li><span class="col-lg-5"><strong>Description:</strong></span><span class="col-lg-7">SpeakOut</span></li>
            <li><span class="col-lg-5"><strong>Enrolled:</strong></span><span class="col-lg-7">01/05/13</span></li>
            <li><span class="col-lg-5"><strong>Status:</strong></span><span class="col-lg-7"><em class="accepted">In Progress</em>-Task 5</span></li>
        </ul>
    </div>
    <!--Timeline-->
    <div class="timeline clearfix">
        <div class="col-lg-10 timeline-group" id="accordion">
            <div id="l-january" class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#april">
                                                April
                                            </a>
										</span>
                </div>
                <div id="april" class="collapse">
                    <div class="timeline-body">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div id="l-march" class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#march">
                                                March
                                            </a>
										</span>
                </div>
                <div id="march" class="collapse">
                    <div class="timeline-body">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div id="l-february" class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#february">
                                                February
                                            </a>
										</span>
                </div>
                <div id="february" class="collapse">
                    <div class="timeline-body">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div id="l-january" class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#january">
                                                January
                                            </a>
										</span>
                </div>
                <div id="january" class="collapse">
                    <div class="timeline-body">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                </div>
            </div>
            <div class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#december">
                                                December
                                            </a>
										</span>
                </div>
                <div id="december" class="collapse">
                    <div class="timeline-body">
                        December
                    </div>
                </div>
            </div>
            <div class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#november">
                                                November
                                            </a>
										</span>
                </div>
                <div id="november" class="collapse">
                    <div class="timeline-body">
                        November
                    </div>
                </div>
            </div>
            <div class="timeline-default">
                <div class="timeline-heading">
										<span class="timeline-title">
											<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#start">
                                                Start
                                            </a>
										</span>
                </div>
                <div id="start" class="collapse">
                    <div class="timeline-body">
                        Start
                    </div>
                </div>
            </div>
        </div>
        <div class="bs-sidebar col-lg-2">
            <ul class="list-timeline">
                <li class="active"><a href="#"><span></span>Now</a></li>
                <li><a href="#"><span></span>May</a></li>
                <li><a href="#"><span></span>April</a></li>
                <li><a href="#"><span></span>March</a></li>
                <li><a href="#"><span></span>February</a></li>
                <li><a href="#l-january"><span></span>January</a></li>
                <li><a href="#"><span></span>December</a></li>
                <li><a href="#"><span></span>November</a></li>
                <li><a href="#"><span></span>Start</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
<aside id="sidebar" class="col-lg-3">
    <section>
        <h4 class="b-dotted">How to use Learnogy</h4>
        <ul>
            <li><span class="mark"></span><a href="#">Choose your instructor</a></li>
            <li><span class="mark"></span><a href="#">Buy your Lesson Pack</a></li>
            <li><span class="mark"></span><a href="#">Reserve your next lesson</a></li>
            <li><span class="mark"></span><a href="#">Start learning!</a></li>
        </ul>
    </section>
    <p class="satisfaction"><span class="icon"></span><a class="text" href="#">Satisfaction guaranteed</a></p>
    <section class="clearfix">
        <h4 class="b-dotted">Give your opinion</h4>
        <p>Participate in the development of our service by completing this survey.</p>
        <ul>
            <li><span class="mark"></span><a href="#">Give your opinion</a></li>
        </ul>
    </section>
</aside>
</div>
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/vendor/fullcalendar.min.js"></script>
<script src="js/vendor/calendar.js"></script>
<script src="js/vendor/select2.js"></script>
<script src="js/vendor/jquery.raty.js"></script>
<script src="js/vendor/mediaelement-and-player.min.js"></script>
<script src="js/application.js"></script>
</body>