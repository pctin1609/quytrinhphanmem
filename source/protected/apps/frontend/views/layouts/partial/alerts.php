<!-- Render alerts -->
<?php if (app()->user->hasFlash('success')) : ?>
    <div class="alert alert-success"><?php echo app()->user->getFlash('success') ?></div>
<?php endif; ?>

<?php if (app()->user->hasFlash('info')) : ?>
    <div class="alert alert-info"><?php echo app()->user->getFlash('info') ?></div>
<?php endif; ?>

<?php if (app()->user->hasFlash('warning')) : ?>
    <div class="alert alert-warning"><?php echo app()->user->getFlash('warning') ?></div>
<?php endif; ?>

<?php if (app()->user->hasFlash('danger')) : ?>
    <div class="alert alert-danger"><?php echo app()->user->getFlash('danger') ?></div>
<?php endif; ?>