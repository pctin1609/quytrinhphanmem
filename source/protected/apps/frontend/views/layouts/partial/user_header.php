<div class="container">
    <h1 class="col-lg-3"><a href="#">Learnogy</a></h1>
    <div id="notification" class="col-lg-offset-2">
        <a href="#"><strong class="t-notification">Notifications</strong></a>
        <span class="pull-right nav-toggle">-</span>
        <div class="c-notification">It's cool, nothing new</div>
    </div>
    <div class="dropdown my-account">
        <a href="#" data-toggle="dropdown">
            <img class="avatar-small" src="/img/avatar-default.png" alt="Marco Nespeca">
            <span>Marco Nespeca</span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu clearfix">
            <li class="active">
                <a href="myacount">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span>Account</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span>Profile</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span>Help</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span>Logout</span>
                </a>
            </li>
        </ul>
    </div>
</div>