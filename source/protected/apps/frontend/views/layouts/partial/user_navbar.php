<div class="container">
    <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#"class="navbar-brand">Menu</a>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">calendar</a></li>
            <li><a href="#">Courses</a></li>
            <li><a href="#">My Teachers</a></li>
            <li><a href="#">Messages</a></li>
            <li><a href="#">Progress</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="enter-class">
                <button type="submit" class="btn btn-default">Enter Classroom</button>
            </li>
            <li class="lesson-time">
                <em>Your next lesson: Fri 31 May 20:00<br>
                    Lesson begins in: 1h, 47 min, 6 sec</em>
            </li>
        </ul>
    </div><!--/.nav-collapse -->
</div>