<script type="text/javascript">
//    function check_null() {
//        var email = $('#inputEmail1').val();
//        var pass = $('#inputPassword1').val();
//        if (email == '' && pass == ''){
//            $('#require_e').html("<b style='color: red'>(Require)</b>");
//            $('#require_p').html("<b style='color: red'>(Require)</b>");
//            return false;
//        }
//        else if(pass == ''){
//            $('#require_p').html("<b style='color: red'>(Require)</b>");
//            return false;
//        }
//        else if(email == '')
//        {
//            $('#require_e').html("<b style='color: red'>(Require)</b>");
//            return false;
//        }
//        else {
//            return true;
//        }
//    }
</script>
<div id="form-login" class="dropdown-menu">
    <?php
        $model = new LoginForm();
    /** @var  CActiveForm $form  */
        $form = app()->controller->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'action' => url('site/login'),
            'htmlOptions' => array(
                'class' => 'form-horizontal',
                'role' => 'form',
            ),
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'afterValidate' => "js:function(form, data, hasError) {
                    $('#LoginForm_username').parent().parent().addClass('has-success');
                    $('#LoginForm_password').parent().parent().addClass('has-success');
                    if (hasError) {
                        for(var key in data) {
                            $('#'+key).parent().parent().removeClass('has-success').addClass('has-error');
                        }
                    }
                    return true;
                }"
            ),
        ));
    ?>
        <div class="form-group">
            <label  class="col-xs-3 control-label">Email <span id="require_e"></span> </label>
            <div class="col-xs-9">
                <?php
                echo    $form->emailField($model, 'username', array('placeholder' => 'Email', 'class' =>  'form-control'));
                echo $form->error($model, 'username');
                ?>
            </div>
        </div>
        <div class="form-group">
            <label  class="col-xs-3 control-label">Password <span id="require_p"></span></label>
            <div class="col-xs-9">
                <?php
                    echo $form->passwordField($model, 'password', array('placeholder' => 'Password', 'class' => 'form-control'));
                    echo $form->error($model, 'password');
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <?php echo $form->checkBox($model, 'rememberMe') ?> Remember me
                    </label>
                </div>
            </div>
            <div class="button-login">
                <button type="submit" name="submit" class="btn btn-success">Login</button>
            </div>
        </div>
   <?php app()->controller->endWidget() ?>
    <div class="success" style="display:none;"></div>
        <a href="#">Forgot password?</a>

</div>