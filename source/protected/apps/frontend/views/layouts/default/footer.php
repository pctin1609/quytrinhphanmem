<footer>
    <div role="contentinfo" class="footer">
        <div class="footer_top">
            <div class="footer_wrap">
                <dl class="parental">
                    <dt>
                    Parents:
                    </dt>
                    <dd>
                        <a >Parental Controls</a>
                    </dd>
                    <dd>
                        <a >Parental Resources</a>
                    </dd>
                </dl>
                <dl class="language">
                    <dt>
                    Change Language Preference to:
                    </dt>
                    <dd>
                        <a class="locale locale_es"  href="">Español</a>
                    </dd>
                </dl>
                <dl class="social">
                    <dt>
                    Follow Us:
                    </dt>
                    <dd>
                        <a target="_blank" class="icon facebook" ><span class="access_aid">Facebook</span></a>
                    </dd>
                    <dd>
                        <a target="_blank" class="icon twitter" ><span class="access_aid">Twitter</span></a>
                    </dd>
                    <dd>
                        <a >What to Watch Newsletters</a>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="footer_wrap">
            <div class="footer_list ">
                <h6>XFINITY</h6>
                <ul>
                    <li>
                        <a href="" rel="default">Shop/Upgrade</a>
                    </li>
                    <li>
                        <a  rel="default">Mover's Edge</a>
                    </li>
                    <li>
                        <a  rel="default">XFINITY Bundles</a>
                    </li>
                    <li>
                        <a  rel="default">Comcast Business Class</a>
                    </li>
                    <li>
                        <a  rel="default">Home Security &amp; Control</a>
                    </li>
                    <li>
                        <a  rel="default">Customer Guarantee</a>
                    </li>
                </ul>
            </div>
            <div class="footer_list ">
                <h6>XFINITY TV</h6>
                <ul>
                    <li>
                        <a href="" rel="default">Shop/Upgrade</a>
                    </li>
                    <li>
                        <a  rel="default">Mover's Edge</a>
                    </li>
                    <li>
                        <a  rel="default">XFINITY Bundles</a>
                    </li>
                    <li>
                        <a  rel="default">Comcast Business Class</a>
                    </li>
                    <li>
                        <a  rel="default">Home Security &amp; Control</a>
                    </li>
                    <li>
                        <a  rel="default">Customer Guarantee</a>
                    </li>
                </ul>
            </div>
            <div class="footer_list ">
                <h6>XFINITY CONNECT</h6>
                <ul>
                    <li>
                        <a href="" rel="default">Shop/Upgrade</a>
                    </li>
                    <li>
                        <a  rel="default">Mover's Edge</a>
                    </li>
                    <li>
                        <a  rel="default">XFINITY Bundles</a>
                    </li>
                    <li>
                        <a  rel="default">Comcast Business Class</a>
                    </li>
                    <li>
                        <a  rel="default">Home Security &amp; Control</a>
                    </li>
                    <li>
                        <a  rel="default">Customer Guarantee</a>
                    </li>
                </ul>
            </div>
            <div class="footer_list ">
                <h6>Comcast Services</h6>
                <ul>
                    <li>
                        <a href="" rel="default">Shop/Upgrade</a>
                    </li>
                    <li>
                        <a  rel="default">Mover's Edge</a>
                    </li>
                    <li>
                        <a  rel="default">XFINITY Bundles</a>
                    </li>
                    <li>
                        <a  rel="default">Comcast Business Class</a>
                    </li>
                    <li>
                        <a  rel="default">Home Security &amp; Control</a>
                    </li>
                    <li>
                        <a  rel="default">Customer Guarantee</a>
                    </li>
                </ul>
            </div>
            <div class="footer_list last">
                <h6>Support  Information</h6>
                <ul>
                    <li>
                        <a  rel="default">XFINITY Learning Center</a>
                    </li>
                    <li>
                        <a  rel="default">Help FAQs</a>
                    </li>
                    <li>
                        <a  rel="default">Register a Comcast ID</a>
                    </li>
                    <li>
                        <a  rel="default">Create New Email Address</a>
                    </li>
                    <li>
                        <a rel="default">My Account</a>
                    </li>
                    <li>
                        <a rel="default">Pay My Bill</a>
                    </li>
                    <li>
                        <a href="" rel="default">[+] Submit Feedback</a>
                    </li>
                    <li>
                        <a href="" rel="default">Parental Resources</a>
                    </li>
                </ul>
            </div>
            <div class="footer_bot">
                <div class="icon logo_xfinity">
                    <span class="access_aid">XFINITY</span>
                </div>
                <ul>
                    <li class="first">
                        &copy; 2013 Comcast
                    </li>
                    <li>
                        <a href="" >Privacy Policy</a><span class="update">(Update)</span>
                    </li>
                    <li>
                        <a href="" id="adchoices">Ad Choices</a>
                    </li>
                    <li>
                        <a  href="">Terms of Service</a>
                    </li>
                    <li>
                        <a >Advertise with Us</a>
                    </li>
                    <li>
                        <a >Contact Us</a>
                    </li>
                </ul>
                <div class="icon logo_comcast">
                    <span class="access_aid">Comcast</span>
                </div>
                <ul class="legal">
                    <li>
                        &copy; 2013 Comcast Interactive Media, LLC. All Rights Reserved
                    </li>
                    <li>
                        2003 Bruce Glikas
                    </li>
                    <li>
                        &copy; 2004 ABC, INC.
                    </li>
                    <li>
                        &copy; 2004, Tommy Miyasaki for HGTV
                    </li>
                    <li>
                        &copy; 2006, Jack Parker for DIY Network
                    </li>
                    <li>
                        ABC
                    </li>
                    <li>
                        ABC Family
                    </li>
                    <li>
                        Andrew Eccles/The WB Television Network
                    </li>
                    <li>
                        Andrew MacPherson
                    </li>
                    <li>
                        Bob D'Amico
                    </li>
                    <li>
                        Cartoon Network
                    </li>
                    <li>
                        CBS
                    </li>
                    <li>
                        Chris Cuffaro/NBC
                    </li>
                    <li>
                        &copy; FOX (1193)
                    </li>
                    <li>
                        Evans Vestal Ward/Syfy
                    </li>
                    <li>
                        Fox
                    </li>
                    <li>
                        Frank Carroll/NBC
                    </li>
                    <li>
                        Getty Images
                    </li>
                    <li>
                        Nintendo/Cartoon Network
                    </li>
                    <li>
                        SCI FI Channel
                    </li>
                    <li>
                        Space Productions
                    </li>
                    <li>
                        THE CW
                    </li>
                    <li>
                        The N
                    </li>
                    <li>
                        The WB Television Network
                    </li>
                    <li class="last">
                        USA Network
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>