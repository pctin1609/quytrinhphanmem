<nav aria-label="Global Navigation" role="navigation" class="is-dark" id="cim-global-nav">
    <div class="cim-global-nav-wrapper">
        <div class="cim-global-nav-main">
            <ul role="menubar">
                <li role="menuitem">
                    <a title="Xfinity" href="<?php echo url('/') ?>" target="_top"  class="cim-global-nav-logo"> <b></b> </a>
                </li>
                <li class="is-active" role="menuitem">
                    <a title="TV" target="_blank" rel="external" > TV </a>
                </li>
                <li class="" role="menuitem">
                    <a title="Email" target="_blank" rel="external" > Email </a>
                </li>
                <li class="" role="menuitem">
                    <a title="Voicemail" target="_blank" rel="external" > Voicemail </a>
                </li>
                <li class="" role="menuitem">
                    <a title="Home Security" rel="default" > Home Security </a>
                </li>
                <li aria-haspopup="true" class="" role="menuitem">
                    <a title="More" target="_blank" rel="external" > More <b></b> </a>
                    <div class="sub-menu" aria-hidden="true">
                        <ul role="menu">
                            <li role="menuitem">
                                <a target="_blank" title="Money" > Money </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="News" > News </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Sports" > Sports </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Jobs" > Jobs </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Autos"> Autos </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Travel" > Travel </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Real Estate" > Real Estate </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Lifestyle" > Lifestyle </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Entertainment" > Entertainment </a>
                            </li>
                        </ul>
                        <ul role="menu">
                            <li role="menuitem">
                                <a target="_blank" title="TV Listings" > TV Listings </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Watchlist" > Watchlist </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="TV Shows" > TV Shows </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Movies" > Movies </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Streampix" > Streampix </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="Live TV" > Live TV </a>
                            </li>
                            <li role="menuitem">
                                <a target="_blank" title="DVR Manager" > DVR Manager </a>
                            </li>
                            <li role="menuitem">
                                <a data-cim-behavior="default" title="More..." > More... </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>

        </div>
        <div class="cim-global-nav-personal">
            <ul role="menubar">
                <li data-global-nav-item="ipauth" role="menuitem" aria-hidden="true">
                    <a  href="#"> <b class="font-icon-ipa-sm"></b> </a>
                </li>
                <li class="xp_shop_upgrade_link" role="menuitem">
                    <a title="Shop/Upgrade" > Shop/Upgrade </a>

                </li>
                <li class="xp_account_link" role="menuitem">
                    <a title="Profile" href="/user/profile">Profile</a>
                </li>
                <li class="xp_support_link" role="menuitem">
                    <a title="Buy Credit" href="/user/buyCredit">Buy Credit</a>
                </li>
                <li class="xp_signin has-username" role="menuitem">
                    <?php if (user()->isGuest) { ?>
                        <a title="Sign In" rel="signin" href="<?php echo url('/user/login') ?>"> Sign In</a>
                    <?php } else { ?>
                        <div class="username" class="hiden">
                            Hi <a title="Hi %s, My Profile" ><span></span></a>
                        </div>
                        <a title="Sign Out" rel="signout" href="<?php echo url('/user/logout') ?>"> Sign Out </a>
                    <?php } ?>
                </li>
            </ul>

        </div>

    </div>
</nav>
<!-- END NAV top -->

<nav class="mod-site-nav" role="navigation">
    <ul>
        <li data-nav-name="watch-online" class="nav-item is-active">
            <a title="Watch Online" href="index.htm" >Watch Online</a>
            <ul class="mod-site-subnav">
                <li data-subnav-name="tv-shows">
                    <a title="TV Shows" href="" >TV Shows</a>
                </li>
                <li data-subnav-name="movies">
                    <a title="Movies" href="movies.htm" >Movies</a>
                </li>
                <li data-subnav-name="family-&amp;-kids">
                    <a title="Family Kids" href="" >Family Kids</a>
                </li>
                <li data-subnav-name="networks">
                    <a title="Networks" href="" >Networks</a>
                </li>
                <li data-subnav-name="live-tv">
                    <a title="Live TV" href="" >Live TV</a>
                </li>
                <li data-subnav-name="streampix">
                    <a title="Streampix" href="" >Streampix</a>
                </li>
                <li data-subnav-name="latino">
                    <a title="Latino" href="" >Latino</a>
                </li>
            </ul>
        </li>
        <li data-nav-name="watch-on-tv" class="nav-item">
            <a title="Watch On TV" href="" >Watch On TV</a>
        </li>
        <li data-nav-name="saved" class="nav-item">
            <a title="Saved" href="" >Saved</a>
        </li>
        <li class="nav-item nav-item-queue">
            <a href="" >Watchlist</a>
        </li>
    </ul>
    <?php $this->renderPartial('/layouts/default/_search') ?>
</nav>
<!-- END nav -->