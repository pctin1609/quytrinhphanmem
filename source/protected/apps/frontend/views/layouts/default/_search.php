<form id="mod-site-search" action="<?php echo url('/film/search') ?>" type="get" class="mod-search mod-search-autocomplete" name="search" role="search">
    <fieldset>
        <label for="search_field" class="access_aid">Search TV Movies</label>
        <input id="v" class="search_field mod-search-field" type="text" name="query" placeholder="Search TV &amp; Movies" autocomplete="off" value="<?php if (isset($_GET['query'])) echo $_GET['query'] ?>">
        <button type="submit" id="search_button" class="button search">
            <b></b>
            <span class="access_aid">Search</span>
        </button>
        <div class="mod-search-autocomplete-results"></div>
    </fieldset>
</form>