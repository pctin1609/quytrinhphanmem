<div class="core-inner">
    <div class="home-row" data-cim-row-index="1">
        <div class="minicover">
            <ul class="minicover-items">
                <li class="is-active">
                    <h2>AAA</h2>
                    <a href="" title=""> <img src="/img/hinh1.jpg" alt="" /> </a>
                </li>
                <li>
                    <h2>Brand New</h2>
                    <a href="" title=""> <img src="/img/hinh2.jpg" alt="" /> </a>
                </li>
                <li>
                    <h2>Now Available</h2>
                    <a href="" title=""> <img src="/img/hinh3.jpeg" alt="" /> </a>
                </li>
                <li>
                    <h2>Just In</h2>
                    <a href="" title=""> <img src="/img/hinh3.jpg" alt="" /> </a>
                </li>
                <li>
                    <h2>Watch For Free</h2>
                    <a href="" title=""> <img src="/img/hinh1.jpg" alt="" /> </a>
                </li>
            </ul>
            <div role="listbox">
                <div class="minicover-nav">
                    <span role="option" title="Blacklist" class="is-active"></span>
                    <span role="option" title="Walking Dead" class=""></span>
                    <span role="option" title="Man of Steel" class=""></span>
                    <span role="option" title="Castle" class=""></span>
                    <span role="option" title="Watch For Free" class=""></span>
                </div>
                <div class="minicover-actions">
                    <button data-cim-action="pause" title="pause" class="font-icon-pause"></button>
                </div>
            </div>
        </div>
        <!-- End Minislide -->

        <div class="video-gallery">
            <h2 class="title">Coming Soon</h2>
            <div class="carousel-container carousel-theme-poster ">
                <div class="carousel-outer" style="height: 246px;">
                    <div class="carousel-nav carousel-prev">
                        <b></b>
                    </div>
                    <div class="carousel-inner" style="overflow-x: auto; padding-bottom: 30px;">
                        <ul class="carousel-items" style="width: 2490px;">
                            <?php foreach ($dataProvider->data as $key => $film) { ?>
                            <li class="entity poster">
                                <!-- entity thumb -->
                                <div class="entity-thumb" >
                                    <!-- entity thumb image wrapper -->
                                    <div class="entity-thumb-wrapper">
                                        <a class="entity-thumb-image posterArt thumbnail" href="<?php echo url('film/detail', array('id' => $film->id)) ?>" > <img class="thumb disable-right-click" src="/img/<?php echo $key ?>.jpg" alt="Sleepy Hollow">
                                            <div class="imageError">
                                                <?php echo $film->title ?>
                                            </div> 
                                        </a>
                                        <div class="entity-thumb-overlay">
                                            <a href="" tabindex="-1"> <div class="entity-thumb-overlay-back"></div> <img class="entity-thumb-overlay-network disable-right-click" alt="FOX" src="" >
                                                <div class="entity-thumb-overlay-icon">
                                                    <b> <span class="font-icon-play"></span> <span class="font-icon-locked"></span> <span class="font-icon-info"></span> </b>
                                                    <div class="entity-thumb-overlay-icon-text">
                                                        <div class="ellipsis">
                                                            <?php echo $film->title ?>
                                                        </div>
                                                        <div class="ellipsis">
                                                            <?php echo date('M d, Y', strtotime($film->date_publish))?>
                                                        </div>
                                                    </div>
                                                </div> </a>
                                            <a class="entity-thumb-overlay-btn button alt5" href="<?php echo url('film/detail', array('id' => $film->id)) ?>" tabindex="-1"> View Detail </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="carousel-nav carousel-next">
                        <b></b>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END HOME ROW -->

    <!-- Start Home Row -->


    <!-- End Home Row -->

    <!-- Start Home Row -->

    <!-- End Home Row -->

    <!-- Start Home Row -->
    <div  class="home-row">
        <div class="video-gallery">
            <h2 class="title">Premium TV</h2>
            <div class="carousel-container carousel-theme-poster ">
                <div class="carousel-outer" style="height: 246px;">
                    <div class="carousel-nav carousel-prev disabled">
                        <b></b>
                    </div>
                    <div class="carousel-inner" style="overflow-x: auto; padding-bottom: 30px;">
                        <ul class="carousel-items" style="width: 2490px;">
                            <?php foreach ($dataProvider->data as $key => $film) { ?>
                            <li data-cim-entity-index="1" data-cim-provider-codes="d" class="entity poster protected">
                                <!-- entity thumb -->
                                <div class="entity-thumb">
                                    <!-- entity thumb image wrapper -->
                                    <div class="entity-thumb-wrapper">
                                        <a href="" class="entity-thumb-image posterArt thumbnail"> <img alt="Eastbound &amp; Down" src="/img/<?php echo $key ?>.jpg" class="thumb disable-right-click">
                                            <noscript>
                                            <img class="thumb disable-right-click is-noscript" src="/img/<?php echo $key ?>.jpg"  alt="Eastbound &amp;amp; Down">
                                            </noscript>
                                            <div class="imageError">
                                                Eastbound &amp; Down
                                            </div>
                                            <noscript>
                                            <div class="imageError is-noscript">
                                                Eastbound &amp; Down
                                            </div>
                                            </noscript> </a>
                                        <div class="entity-thumb-overlay">
                                            <a href=""> <div class="entity-thumb-overlay-back"></div> <img src="" alt="HBO" class="entity-thumb-overlay-network disable-right-click">
                                                <div class="entity-thumb-overlay-icon">
                                                    <b> <span class="font-icon-play"></span> <span class="font-icon-locked"></span> <span class="font-icon-info"></span> </b>
                                                    <div class="entity-thumb-overlay-icon-text">
                                                        <div class="ellipsis">
                                                            <?php echo $film->title ?>
                                                        </div>
                                                        <div class="ellipsis">
                                                            <?php echo date('M d, Y', strtotime($film->date_publish))?>
                                                        </div>
                                                    </div>
                                                </div> </a>
                                            <a href="" class="entity-thumb-overlay-btn button alt5"> All Episodes </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="carousel-nav carousel-next">
                        <b></b>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>