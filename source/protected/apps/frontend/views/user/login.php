<link rel="stylesheet" href="<?php echo app()->getBaseUrl() ?>/css/formLoginStyle.css" type="text/css" media="screen" />

<div id="form_wrapper" class="form_wrapper">
    
    
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login',
        'action' => url('user/login'),
        'htmlOptions' => array(
            'style' => '',      
            'class' => 'login active'  ,
        )
    ))
    ?>
        <h3>Login</h3>
        <div>
            <?php echo $form->labelEx($model, 'username') ?>
            <?php echo $form->textField($model, 'username') ?>
            <?php echo $form->error($model, 'username') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'password') ?>
            <?php echo $form->passwordField($model, 'password') ?>
            <?php echo $form->error($model, 'password') ?>
        </div>
        <a href="<?php echo url('site/forgotPassword') ?>" rel="forgot_password" class="forgot linkform">Forgot your password?</a>
        
        <div class="bottom">
            <div class="remember">
                <?php echo $form->checkBox($model, 'rememberMe') ?>
                <span>Keep me logged in</span>
            </div>
            <?php echo CHtml::submitButton('Login') ?>
            </input>
            <a href="<?php echo url('/user/register') ?>" rel="register" >You don't have an account yet? Register here</a>
            <div class="clear"></div>
        </div>
    <?php $this->endWidget() ?>
        
    <?php $this->renderPartial('_form_forgot_password') ?>
</div>