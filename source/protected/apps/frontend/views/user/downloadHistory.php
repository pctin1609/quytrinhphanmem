
<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/film_detail.css');
$data = $dataProvider->getData(); 
$stt = 0;
?>


<div class="core-inner" style="margin: auto; margin-bottom: 30px;"> 
    <div class="row" style="border-bottom: 2px solid #e1e1e1; background-color: #ccc; font-size: x-small;">
        <label style="width: 30px; margin:0px">STT</label>
        <label style="width: 470px; margin:0px; border-left: 2px solid #e1e1e1;">TÊN PHIM</label>
        <label style="width: 120px; margin:0px; border-left: 2px solid #e1e1e1; float:right;">NGÀY DOWN</label>       
    </div>
    <?php  foreach ($data as $item) 
        { $stt = $stt + 1; 
        ?>
    
     <div class="row" style="border-bottom: 2px solid #DcDCDC;">
         <label style="width: 30px; margin:0px"><?php echo $stt ?></label>
         <label style="width: 470px;margin:0px;border-left: 2px solid #e1e1e1;">
             <a href="/film/detail/id/<?php echo $item->film->id; ?>"><?php echo $item->film->title ?></a>
         </label>
        <label style="width: 120px;margin:0px; background-color: #DBD9D9;  border-left: 2px solid #e1e1e1; float:right;"> 
            <?php echo $item->created_date ?> 
        </label>       
    </div>
    
    <?php } ?>
</div>