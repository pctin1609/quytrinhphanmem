
<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/film_detail.css');
$data = $dataProvider->getData(); 
$stt = 0;
?>


<div class="core-inner" style="margin: auto; width: 900px; margin-bottom: 30px;"> 
    <div class="row" style="border-bottom: 2px solid #e1e1e1; background-color: #ccc; font-size: x-small;">
        <label style="width: 30px; margin:0px">STT</label>
        <label style="width: 470px; margin:0px; border-left: 2px solid #e1e1e1;text-align: center;">TÊN PHIM</label>
        <label style="width: 80px; margin:0px; border-left: 2px solid #e1e1e1;">LƯỢT XEM</label>   
        <label style="width: 120px; margin:0px; border-left: 2px solid #e1e1e1;">LẦN CUỐI XEM</label>    
    </div>
    <?php  foreach ($data as $item) 
        { $stt = $stt + 1; 
        ?>
    
     <div class="row" style="border-bottom: 2px solid #DcDCDC;">
         <label style="width: 30px; margin:0px"><?php echo $stt ?></label>
         <label style="width: 470px;margin:0px;border-left: 2px solid #e1e1e1;">
             <a href="/film/detail/id/<?php echo $item->film->id; ?>"><?php echo $item->film->title ?></a>
         </label>
        <label style="width:80px;margin:0px; background-color: #DBD9D9;  border-left: 2px solid #e1e1e1; text-align: center;"> 
            <?php echo $item->watched_times ?> 
        </label>  
         <label style="width:120px;margin:0px; background-color: #DBD9D9;  border-left: 2px solid #e1e1e1;text-align: center; "> 
            <?php echo $item->lasted_date ?> 
        </label>
    </div>
    
    <?php } ?>
</div>