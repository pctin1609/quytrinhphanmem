<?php
/**
 * 
 */
?>

<?php $this->renderPartial('menu');?>

<div class="main-content" id="myaccount">
    <div class="item">
        <a class="contact-me" href="#">
            <span class="mail"></span>Contact me
        </a>
        
        <ul class="col-lg-offset-3 tab nav nav-tabs">
            <li class="active"><a href="#general">General</a></li>
            <li><a href="#credit-history">Credit History</a></li>
            <li><a href="#invoices">My Invoices</a></li>
            <li class=""><a href="#credit">Buy Credit</a></li>
        </ul>
        
        <div class="tab-content">
            <div class="tab-pane active" id="general">
                <p>Total lessons taken: <span>265 </span>(30 min slots)</p>
                <p>Total lessons paid: <span>-265 </span>(30 min slots)</p>
                <p>Account balance: <span>0.00 </span>(30 min slots)</p>
                <a href="#credit" class="col-lg-offset-2 btn btn-success">Buy Credit</a>
            </div><!--general-->
            
            <div class="tab-pane" id="credit-history">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Lesson/Credit:</th>
                            <th>Comment:</th>
                            <th>Teacher:</th>
                            <th>School:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>05/06/2013</td>
                            <td>1.00</td>
                            <td>Lesson taken</td>
                            <td>Marco Nespeca</td>
                            <td>The Victoria Company</td>
                        </tr>
                        <tr>
                            <td>05/06/2013</td>
                            <td>1.00</td>
                            <td>Lesson taken</td>
                            <td>Marco Nespeca</td>
                            <td>The Victoria Company</td>
                        </tr>
                        <tr>
                            <td>05/06/2013</td>
                            <td>1.00</td>
                            <td>Lesson taken</td>
                            <td>Marco Nespeca</td>
                            <td>The Victoria Company</td>
                        </tr>
                        <tr>
                            <td>05/06/2013</td>
                            <td>1.00</td>
                            <td>Lesson taken</td>
                            <td>Marco Nespeca</td>
                            <td>The Victoria Company</td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="footer clearfix">
                    <a href="#" class="download"><span class="glyphicon glyphicon-save"></span></a>
                    <ul class="pagination">
                        <li><a href="#">←</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">→</a></li>
                    </ul>
                </div>
            </div><!--credit-history-->
            
            <div class="tab-pane" id="invoices">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Invoice Number:</th>
                            <th>Date:</th>
                            <th>Total price (incl.VAT):</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>4205TFR30062013</td>
                            <td>20/03/2013</td>
                            <td>$120</td>
                            <td class="action">
                                <a href="#"><span class="glyphicon glyphicon-search"></span></a>
                                <a href="#"><span class="glyphicon glyphicon-save"></span></a>											
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="footer clearfix">
                    <a href="#" class="download"><span class="glyphicon glyphicon-save"></span></a>
                    <ul class="pagination">
                        <li><a href="#">←</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">→</a></li>
                    </ul>
                </div>
            </div><!--invoices-->
            
            <div class="tab-pane" id="credit">
                <section class="step step1 clearfix">
                    <header>
                        <img src="img/step1.png" alt="">
                        <h2>Buy Lessons</h2>
                    </header>
                    <p>Buy Learnissimo lessons&nbsp;(secure payment by credit card, Visa and Mastercard
                        <span class="paypal-off"> or Paypal</span>) to book your next lessons. You can replenish 
                        your account at any time.</p>
                    <em>* Prices mentioned below are value added tax (VAT) excluded. VAT fees (19,6%) are only applicable to EU residents.</em>
                    <form id="pricing">
                        <fieldset class="col-pricing">
                            <div class="bg-pricing">
                                <header>
                                    <h4>Refresh</h4>
                                    <span>180 €<sup>VAT excl. *</sup></span>
                                </header>
                                <div class="body-pricing">
                                    <p><span>10 lessons</span> of 30 minutes</p>
                                    <ul><li>Unlimited languages</li>
                                        <li>Unlimited teachers</li>
                                        <li>Expires after: <span>90 days</span></li></ul>
                                </div>
                                <label class="radio-pricing">
                                    <input type="radio">
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="col-pricing">
                            <div class="bg-pricing">
                                <header>
                                    <h3>Advanced</h3>
                                    <strong>$399 €<sup>VAT excl. *</sup></strong>
                                </header>
                                <div class="body-pricing">
                                    <p><strong>30 lessons</strong> of 30 minutes</p>
                                    <ul><li>Unlimited languages</li>
                                        <li>Unlimited teachers</li>
                                        <li>Expires after: <strong>180 days</strong></li></ul>
                                </div>
                                <label class="radio-pricing">
                                    <input type="radio">
                                </label>
                            </div>
                        </fieldset>
                        <fieldset class="col-pricing last">
                            <header>
                                <h4>Intensive</h4>
                                <span>$749 €<sup>VAT excl. *</sup></span>
                            </header>
                            <div class="body-pricing">
                                <p><span>60 lessons</span> of 30 minutes</p>
                                <ul><li>Unlimited languages</li>
                                    <li>Unlimited teachers</li>
                                    <li>Expires after: <span>365 days</span></li></ul>
                            </div>
                            <label class="radio-pricing">
                                <input type="radio">
                            </label>
                        </fieldset>
                        <p><input type="radio">You can also purchase half an hour individual classes for $20 <sup>VAT excl. *</sup> (Expires after: 7 days)</p>
                        <input type="submit" class="col-lg-offset-4 btn btn-success" value="Continue (Secure payment)">
                    </form>
                    <form id="coupon" class="col-lg-offset-5">
                        <input class="form-control" type="text" placeholder="Enter your promo code here :">
                        <input class="btn btn-success" type="submit" value="OK">
                    </form>
                </section>
                
                <section class="step step2 row top-20">
                    <div class="col-lg-6">
                        <div class="tab-content">
                            <h4 class="color">Buy Lessons</h4>
                            <div class="center"><img src="img/step2.png" alt=""></div>
                            <p class="clearfix"><span class="pull-left">Number of lessons</span><strong class="color pull-right">30.0</strong></p>
                            <hr><p class="clearfix"><span class="pull-left">Price (without VAT)<br> VAT (19.6%)</span>
                                <span class="pull-right">
                                    <span>399.0 €</span><br><span>78.20€</span>
                                </span>
                            </p><hr>
                            <p class="clearfix color"><strong class="pull-left">Price (incl.VAT)</strong><strong class="pull-right">477.20€</strong></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="tab-content">
                            <h4 class="color">Payment methods</h4>
                            <p>Select the payment method of your choice</p>
                            <div class="clearfix">
                                <p class="pull-left">
                                    <input type="radio">
                                    <img src="img/CB.gif" alt="">
                                    <img src="img/visa.gif" alt="">
                                    <img src="img/MASTERCARD.gif" alt="">
                                </p>
                                <p class="pull-right">Payment by VISA - via BNP Paribas</p>
                            </div>
                            <div class="clearfix">
                                <p class="pull-left">
                                    <input type="radio">
                                    <img src="img/paypal.png" alt="">
                                </p>
                                <p class="pull-right col-lg-9">Pay directly with your Paypal account. Paypal is a secure 
                                    payment method used by more than 90 million users throughout the world. </p>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> 
                                    By ticking this box, I accept the <a href="#" class="iframe">general sales conditions</a>
                                </label>
                            </div>
                            <input type="submit" value="Continue (Secure payment)" class="col-lg-offset-4 btn btn-success">
                        </div>
                    </div>
                </section>
                
                <section class="step step3">
                    <div class="tab-content">
                        <div class="center">
                            <img src="img/step3.png" alt="">
                            <p>Select one of the payment methods below, you will be connected to the bank’s website (step 3/3) :</p>
                            <a href="#"><img src="img/CB.gif" alt=""></a>
                            <a href="#"><img src="img/visa.gif" alt=""></a>
                            <a href="#"><img src="img/MASTERCARD.gif" alt=""></a>
                        </div>
                    </div>
                </section>							
            </div><!--credit-->
            
        </div>
    </div>
</div>