<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/user_profile.css');
?>
<div class="core-inner" style="margin: auto" style="text-align: center">    
    <h1 style="text-align: center">Purchase Credit</h1>
    
    <?php if (app()->user->hasFlash('error')) { ?>
        <div class="error-message">
            <?php echo app()->user->getFlash('error') ?>
        </div>
    <?php } ?>
    
    <div style="text-align: center">
        <span style="font-size: 15px; font-weight: bold">Credit Price: <span style="font-size: 18px">$<?php echo param('creditPrice') ?></span> / credit</span>
    </div>
    
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'credit',
        'action' => url('user/buyCredit'),
        'htmlOptions' => array( 'style' => '', 'class' => 'login active' )
    ))
    ?>
    <div class="row" style="text-align: center">
        <label>Number Credit</label>
        <?php echo CHtml::dropDownList('credit', isset($_POST['credit']) ? $_POST['credit'] : null, array("5" => 5, "10" => 10,"20" =>  20,"30" =>  30,"50" =>  50,"100" =>  100), array('style' => 'width: 100px')) ?>
    </div>

    <div class="row" style="text-align: center;">
        <button type="submit" class="btn btn-primary">Purchase</button>
    </div>

    <?php $this->endWidget(); ?>

</div>