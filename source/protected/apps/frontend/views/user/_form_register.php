<link rel="stylesheet" href="<?php echo app()->getBaseUrl() ?>/css/formLoginStyle.css" type="text/css" media="screen" />
<div id="form_wrapper" class="form_wrapper" style="width: 550px;  height: 430px ;">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'register',
        'action' => url('user/register'),
        'htmlOptions' => array(
            'style' => '',
            'class' => 'register active',
        )
            ))
    ?>

    <h3>Register</h3>
    <?php if (Yii::app()->user->hasFlash('success')){ ?>

        <div class="flash-success" style="margin: -8px 29px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
            <a href="<?php echo url('/user/login') ?>" rel="login">You have an account already? Log in here</a>
        </div>

    <?php }
        else{?>
    <div class="column">
        <div>
            <?php echo $form->labelEx($model, 'email') ?>
            <?php echo $form->textField($model, 'email') ?>
            <?php echo $form->error($model, 'email') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'first_name') ?>
            <?php echo $form->textField($model, 'first_name') ?>
            <?php echo $form->error($model, 'first_name') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'address') ?>
            <?php echo $form->textField($model, 'address') ?>
            <?php echo $form->error($model, 'address') ?>
        </div>
    </div>
    <div class="column">
        <div>
            <?php echo $form->labelEx($model, 'phone') ?>
            <?php echo $form->textField($model, 'phone') ?>
            <?php echo $form->error($model, 'phone') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'last_name') ?>
            <?php echo $form->textField($model, 'last_name') ?>
            <?php echo $form->error($model, 'last_name') ?>
        </div>
        <div>
            <div style="margin-left: 30px;"><?php $this->widget('CCaptcha') ?></div>
            <?php echo $form->textField($model, 'verifyCode') ?>
            <?php echo $form->error($model, 'verifyCode') ?>
        </div>
    </div>
    <div class="bottom">
        <div class="remember">
            <input type="checkbox" />
            <span>Send me updates</span>
        </div>
        <input type="submit" value="Register" />
        <a href="<?php echo url('/user/login') ?>" rel="login" class="linkform" onClick="Change_Form()" >You have an account already? Log in here</a>
        <div class="clear"></div>
    </div>
    
    <?php } ?>

    <?php $this->endWidget() ?>
</div>
