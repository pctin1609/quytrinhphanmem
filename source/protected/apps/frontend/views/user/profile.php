<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/user_profile.css');
?>
<div class="core-inner" style="margin: auto"> 
    
    <div class="avatar">
        <img src="/img/avatar.jpg" width="" height=""/>
        <h1> User profile</h1>
         <?php if (Yii::app()->user->hasFlash('success')) { ?>
            <div class="message">
                <?php echo Yii::app()->user->getFlash('success')?>
            </div>
        <?php } ?>
    </div>    
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'profile',
        'action' => url('user/profile'),
        'htmlOptions' => array( 'style' => '', 'class' => 'login active' )
            ))
    ?>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'first_name') ?>
        <?php echo $form->textField($model, 'first_name', array('style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'last_name') ?>
        <?php echo $form->textField($model, 'last_name', array('style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'email') ?>
        <?php echo $form->textField($model, 'email', array('style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'phone') ?>
        <?php echo $form->textField($model, 'phone', array('style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'address') ?>
        <?php echo $form->textField($model, 'address', array('style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'credit') ?>
        <?php echo $form->textField($model, 'credit', array('disabled'=> 'disabled', 'style' => 'float: right; width: 270px; margin-top: -10px;')) ?>
    </div>

    <div class="row" style="text-align: center;">
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>

    <?php $this->endWidget(); ?>
    <div class="row"> 
        <lable><a href="/user/changePassword"> Change password</a></lable>
    </div>
</div>