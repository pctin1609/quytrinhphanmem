<link rel="stylesheet" href="<?php echo app()->getBaseUrl() ?>/css/formLoginStyle.css" type="text/css" media="screen" />
<div id="form_wrapper" class="form_wrapper" style="width: 550px;  height: 430px ;">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'register',
        'action' => url('user/changePassword'),
        'htmlOptions' => array(
            'style' => '',
            'class' => 'register active',
        )
            ))
    ?>

    <h3>Change Password</h3>
    <?php if (Yii::app()->user->hasFlash('success')){ ?>

        <div class="flash-success" style="margin: -8px 29px;">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>

    <?php } ?>
    <div class="column">
        <div>
            <?php echo $form->labelEx($model, 'old_password') ?>
            <?php echo $form->passwordField($model, 'old_password') ?>
            <?php echo $form->error($model, 'old_password') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'new_password') ?>
            <?php echo $form->passwordField($model, 'new_password') ?>
            <?php echo $form->error($model, 'new_password') ?>
        </div>
        <div>
            <?php echo $form->labelEx($model, 'confirm_new_password') ?>
            <?php echo $form->passwordField($model, 'confirm_new_password') ?>
            <?php echo $form->error($model, 'confirm_new_password') ?>
        </div>
    </div>
    <div class="bottom">
        <input type="submit" value="Submit" />
        <div class="clear"></div>
    </div>

    <?php $this->endWidget() ?>
</div>
