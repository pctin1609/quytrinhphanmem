<section role="main">
    <div class="list_row carousel-container carousel-theme-poster ">
        <div style="height: 255px;" class="carousel-outer">
            <div style="overflow-x: auto; padding-bottom: 30px;" class="carousel-inner">
                <ul class="carousel-items">
                    <?php
                    $baseUrl = Yii::app()->baseUrl;
                    foreach ($dataProvider as $item) {
                    ?>
                        <li class="entity poster protected" data-cim-provider-codes="d" data-cim-entity-index="1">
                            <!-- entity thumb -->
                            <div class="entity-thumb">
                                <!-- entity thumb image wrapper -->
                                <div class="entity-thumb-wrapper">
                                    <a class="entity-thumb-image posterArt thumbnail" href=""> <img class="thumb disable-right-click" src="images/123.jpg" alt="Missing picture">
                                        <noscript>
                                        <img class="thumb disable-right-click is-noscript" src="images/123.jpg"  alt="Missing picture">
                                        </noscript>
                                        <div class="imageError">
                                            Eastbound  Down
                                        </div>
                                        <noscript>
                                        <div class="imageError is-noscript">
                                            Eastbound  Down
                                        </div>
                                        </noscript> </a>
                                    <div class="entity-thumb-overlay">
                                        <a href="<?php echo $baseUrl . '/film/view/id/' . $item->id ?>"> <div class="entity-thumb-overlay-back"></div> <img class="entity-thumb-overlay-network disable-right-click" alt="HBO" src="">
                                            <div class="entity-thumb-overlay-icon">
                                                <b> <span class="font-icon-play"></span> <span class="font-icon-locked"></span> <span class="font-icon-info"></span> </b>
                                                <div class="entity-thumb-overlay-icon-text">
                                                    <div class="ellipsis">
                                                        <?php echo $item->title ?>
                                                    </div>
                                                    <div class="ellipsis">
                                                        <?php echo $item->category->name ?>
                                                    </div>
                                                </div>
                                            </div> </a>
                                        <a class="entity-thumb-overlay-btn button alt5" href=""> Buy </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
            </div>

        </div>
    </div>

    <!-- END list Row -->



    <!-- START pagging -->
    <div class="pagination mycustompage">
        <ul>
            <li class="previous">
                <a href="" title=""><</a>
            </li>
            <?php
            $i = 0;
            foreach ($dataProvider as $item) {
                $i++;
                ?>
                <li class="">
                    <a href="" title=""><?php echo $i ?></a>
                </li>
                <?php
            }
            ?>

            <li class="next">
                <a href="" title="">></a>
            </li>
        </ul>
    </div>



    <!-- END pagging -->


</section>
