<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/film_detail.css');
?>
<div class="core-inner" style="margin: auto; margin-bottom: 30px;"> 

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'detail',
        'action' => url('film/detail'),
        'htmlOptions' => array('style' => '', 'class' => 'film_detail')
            ))
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title') ?>
        <label style="width: 200px;font-size: 20px; color: #F00;border-left: 2px solid #e1e1e1"> <?php echo $model->title ?> </label>       
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'credits') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->credits . " $" ?> </label>       
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->description ?> </label>       
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'author') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->author ?> </label>       
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'date_publish') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->date_publish ?> </label>       
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'updated_date') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->updated_date ?> </label>       
    </div>    

    <div class="row">
        <?php echo $form->labelEx($model, 'director') ?>
        <label style="width: 200px;border-left: 2px solid #e1e1e1"> <?php echo $model->director ?> </label>       
    </div>

    <div class="row">
        <label style="width: 98px;border-left: 2px solid #e1e1e1">
            <a href="<?php echo Yii::app()->baseUrl . '/film/watch/id/' . $model->id ?>" >Xem</a> 
        </label>  
        <label style="width: 200px;border-left: 2px solid #e1e1e1">
            <a href="<?php echo Yii::app()->baseUrl . '/film/download/id/' . $model->file->id ?>" >Tải</a>
        </label> 

    </div>

    <?php $this->endWidget(); ?>

</div>