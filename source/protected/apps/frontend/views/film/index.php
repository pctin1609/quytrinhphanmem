<?php if (count($dataProvider->data) > 0) { ?>
<div class="list_row carousel-container carousel-theme-poster ">
    <div style="height: 255px;" class="carousel-outer">
        <div style="overflow-x: auto; padding-bottom: 30px;" class="carousel-inner">
            <ul class="carousel-items">
                <?php
                $baseUrl = Yii::app()->baseUrl; 
                foreach ($dataProvider->data as $key => $item) {
                    ?>
                    <li class="entity poster protected" data-cim-provider-codes="d" data-cim-entity-index="1">
                        <!-- entity thumb -->
                        <div class="entity-thumb">
                            <!-- entity thumb image wrapper -->
                            <div class="entity-thumb-wrapper">
                                <a class="entity-thumb-image posterArt thumbnail" href="<?php echo $baseUrl.'/film/detail/id/'.$item->id?>"> <img class="thumb disable-right-click" src="/img/<?php echo $key ?>.jpg" alt="Eastbound  Down">
                                    <noscript>
                                    <img class="thumb disable-right-click is-noscript" src="/img/hinh2.jpg"  alt="Eastbound amp; Down">
                                    </noscript>
                                    <div class="imageError">
                                        Eastbound  Down
                                    </div>
                                    <noscript>
                                    <div class="imageError is-noscript">
                                        Eastbound  Down
                                    </div>
                                    </noscript> </a>
                                <div class="entity-thumb-overlay">
                                    <a href="<?php echo $baseUrl.'/film/detail/id/'.$item->id?>"> <div class="entity-thumb-overlay-back"></div> <img class="entity-thumb-overlay-network disable-right-click" alt="HBO" src="<?php echo app()->baseUrl; ?>/img/hinh2.jpg">
                                        <div class="entity-thumb-overlay-icon">
                                            <b> <span class="font-icon-play"></span> <span class="font-icon-locked"></span> <span class="font-icon-info"></span> </b>
                                            <div class="entity-thumb-overlay-icon-text">
                                                <div class="ellipsis">
                                                    Play Latest
                                                </div>
                                                <div class="ellipsis">
                                                    Nov 10, 2013
                                                </div>
                                            </div>
                                        </div> </a>
                                    <a class="entity-thumb-overlay-btn button alt5" href=""> All Episodes </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php
                }
                ?>

            </ul>
        </div>

    </div>
</div>
<div class="pagination mycustompage">
<?php $this->widget('CLinkPager', array(
    'header' => '', 
    'pages' => $dataProvider->pagination,
    'htmlOptions' => array('class' => '')
));
?>
</div>
<?php } else { ?>
<h2 style="margin-left: 50px;">No search found.</h2>
<?php } ?>
