<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$baseUrl = Yii::app()->baseUrl;
$filePath = Yii::app()->createAbsoluteUrl('common/upload/');
$playerLink = Yii::app()->createAbsoluteUrl('frontend/js/swfobject/mediaplayer.swf');

$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/jwplayer/jwplayer.js');
$cs->registerScriptFile($baseUrl . '/js/jwplayer/jwplayer.html5.js');
$cs->registerScriptFile($baseUrl . '/js/jwplayer/swfobject.js');
$cs->registerScriptFile($baseUrl . '/js/swfobject/player.js');

$data = $dataProvider->getData();
foreach ($data as $item) {
    $title = $item->title;
    $thumb = $item->thumb;
    $file = $item->file;
    $cat = $item->category;
    $description = $item->description;
}
if (Yii::app()->user->getId() !== null) {
    $filePath .= '/' . $file->source;
}
else
    $filePath .= '/' . $file->source;
$path = Utils::uploadPath($filePath);
?>

<div id="container">
    <section role="main">

        <div  class="player playerGradient" style="width: 916px; height: 600px; margin:0px auto;">
            <div id="player"></div><script language="javascript">
                function chieuphim(t, h)
                {


                    var s1 = new SWFObject("<?php echo $playerLink ?>", "mediaplayer", "500", "500", "7");
                    s1.addParam("allowfullscreen", "true");
                    //s1.addVariable("autostart",true);
                    //s1.addVariable("displayclick",true);
                    s1.addVariable("width", "500");
                    s1.addVariable("height", "500");
                    s1.addVariable("file", t);
                    s1.addVariable("image", h);
                    s1.write("player");

                }

            </script>

            <script type="text/javascript">
                chieuphim('<?php echo $filePath ?>', '');
            </script>
            <div class="hgroup">

                <hgroup>
                    <h1 id="metaTitle">Title: <span><?php echo $title ?></span></h1>
                    <p><span>Category: </span><a href="<?php echo $baseUrl . '/film/viewcat/id/' . $cat->id ?>"><?php echo $cat->name ?></a></p>
                </hgroup>

            </div>
        </div>
    </section>
</div>
