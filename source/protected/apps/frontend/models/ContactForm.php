<?php
class ContactForm extends CFormModel
{
    public $name;
    public $email;
    public $phone;
    public $address;
    public $message;
    public $subject;
    public $verifyCode;

    public function rules()
    {
        return array(
            array('name, email, message, subject', 'required'),
            array('name, email, subject', 'length', 'max' => 255),
            array('email', 'email'),
            array('address, phone', 'safe'),
            array('phone', 'phoneNumber'),
            array('verifyCode', 'required'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    public function phoneNumber($attribute, $params = '')
    {
        if (preg_match('/^([+]?[0-9\s\.()]+)$/', $this->$attribute) === 0 && $this->$attribute != null) {
            $this->addError($attribute,
                'Your number was wrong');
        }
    }

    public function attributeLabels()
    {
        return array(
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'message' => 'Message',
            'subject' => 'Subject',
            'verifyCode' => 'Verify Code',
        );
    }
}
