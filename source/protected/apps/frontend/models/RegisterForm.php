
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class RegisterForm extends CFormModel
{

    public $email;
    public $address;
    public $first_name;
    public $last_name;
    public $phone;
    public $verifyCode;

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('first_name, last_name, email', 'length', 'max' => 255),
            array('email', 'email'),
            array('address, phone', 'safe'),
            array('phone', 'phoneNumber'),
            array('verifyCode', 'required'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    public function phoneNumber($attribute, $params = '')
    {
        if (preg_match('/^([+]?[0-9\s\.()]+)$/', $this->$attribute) === 0 && $this->$attribute != null) {
            $this->addError($attribute, 'Your number was wrong');
        }
    }

    public function attributeLabels()
    {
        return array(
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'address' => 'Address',
            'verifyCode' => 'Verify Code',
        );
    }

}

?>

