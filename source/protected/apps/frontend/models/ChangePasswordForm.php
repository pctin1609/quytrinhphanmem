<?php

class ChangePasswordForm extends CFormModel {

    public $old_password;
    public $new_password;
    public $confirm_new_password;

    public function rules() {
        return array(
            array('old_password, new_password, confirm_new_password', 'required'),
            array('new_password', 'length', 'min' => 6, 'max' => 32),
            array('old_password', 'checkPassword'),
            array('new_password', 'match', 'pattern' => '/^[A-Za-z0-9_!@#$%^&*()+=?.,]+$/u', 'message' => 'Spaces characters are not allowed'),
            array('confirm_new_password', 'compare', 'compareAttribute' => 'new_password'),
        );
    }

    public function attributeLabels() {
        return array(
            'old_password' => 'Old password',
            'new_password' => 'New password',
            'confirm_new_password' => 'Repeat password'
        );
    }

    public function checkPassword() {
        $user = User::model()->findByPk(user()->id);

        if (Utils::md5($this->old_password) != $user->password) {
            $this->addError('old_password', 'Your password is incorrect');
        }
    }

}
