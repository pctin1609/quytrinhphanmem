<?php
class ForgotPasswordForm extends CFormModel 
{
	public $password;
	public $password_repeat;
	
	
	public function rules() {
		return array(
			array('password, password_repeat', 'required'),
			array('password', 'length', 'min' => 6),
            array('password', 'match', 'pattern' => '/^[A-Za-z0-9_!@#$%^&*()+=?.,]+$/u', 'message' => 'Spaces characters are not allowed'),
			array('password_repeat', 'compare', 'compareAttribute' => 'password'),
		);
	}
    
    public function attributeLabels() {
		return array(
			'password' => 'Password',
			'password_repeat' => 'Repeat password'
		);
	}
}
