<?php

class HttpRequest extends CHttpRequest
{
	public function getBaseUrl($absolute = false)
	 {
	  	return $this->getHostInfo();
	 }
	
	 public function getPathInfo()
	 {
		return trim(str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']), '/');
	 }
}