<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

    const ERROR_ACCOUNT_DEACTIVATE = 3;
    const ERROR_ACCOUNT_DELETED = 4;

    public function authenticate()
    {
        $user = User::model()->findByAttributes(array('email' => $this->username));
        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($user->password !== Utils::md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
//        } elseif ($user->status == User::STATUS_DEACTIVE) {
//            $this->errorCode = self::ERROR_ACCOUNT_DEACTIVATE;
//        } elseif ($user->status == User::STATUS_DELETE) {
//            $this->errorCode = self::ERROR_ACCOUNT_DELETED;
        } else {
            $this->_id = $user->id;
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId()
    {
        return $this->_id;
    }

}