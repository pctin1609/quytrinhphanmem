<?php

class Controller extends GController
{
    public $layout = 'default';
    
    public function actions()
    {
        $userId = user()->id;
        $maps = parent::actions();
        $maps['upload'] = array(
            'class' => 'xupload.actions.XUploadAction',
            'path' => Utils::uploadTmpPath($userId),
            'publicPath' => Utils::uploadTmpUrl($userId),
            'subfolderVar' => 'folder',
            'secureFileNames' => true,
            'stateVariable' => 'uploadFiles'
        );
        $maps['captcha'] = array(
            'class' => 'common.actions.CaptchaAction',
            'backColor' => 0xFFFFFF,
            'testLimit' => 1,
        );
        $maps['error'] = array(
            'class' => 'common.actions.ErrorAction',
        );
        $aliasPath = 'app.controllers.actions.';
        $maps['dashboard'] = array(
            'class' 		=> $aliasPath . 'DashboardAction',
        );
        $maps['logout'] = array(
            'class' 		=> $aliasPath . 'LogoutAction',
        );
        //
        return $maps;
    }

    public function filters()
    {
        return array(
            'accessControl'
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('login', 'signin', 'register', 'captcha', 'verifyAccount', 'view', 'resetPassword', 'forgotPassword', 'assume', 'callback', 'widget'),
                'users' => array('?')
            ),
            array(
                'allow',
                'controllers' => array('rss', 'film'),
                'users' => array('?')
            ),
            array(
                'allow',
                'controllers' => array('site'),
                'users' => array('?')
            ),
            array(
                'allow',
                'actions' => array('index'),
                'controllers' => array('auction'),
                'users' => array('?')
            ),
            array(
                'allow',
                'users' => array('@')
            ),
            array(
                'deny',
                'users' => array('*')
            )
        );
    }

    /*
    public function beforeAction($action)
    {
        $subDomain = Utils::getSubDomain();
        if (!empty($subDomain)) {
            $user = User::model()->findByAttributes(array('domain' => $subDomain));
            if (!$user) {
                $this->redirect(param('siteUrl'));
                app()->end();
            } else {
                $this->userId = $user->id;
                $this->userCompany = $user->company_name;

                if (!empty($user->logo))
                    $this->logo = $user->getImageUrl('logo');
                if (!empty($user->description))
                    $this->description = $user->description;
                if (!empty($user->banner))
                    $this->banner = $user->getImageUrl('banner', '1170x240');
            }
        }
        return parent::beforeAction($action);
    }
     * 
     */

}