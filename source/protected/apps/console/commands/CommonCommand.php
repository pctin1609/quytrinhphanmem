<?php

class CommonCommand extends GConsoleCommand
{
    /**
     * @desc send urgent emails 
     */
    public function actionUrgentMail()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = param('sendMailLimit');
        $criteria->addCondition("is_sent = 0");
        $criteria->addCondition("type = " . Email::URGENT);

        $queues = Email::model()->findAll($criteria);

        foreach ($queues as $email) {
            $email->sendMail();
        }
    }
    
    /**
     * @desc send normal emails 
     */
    public function actionNormalMail()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = param('sendMailLimit');
        $criteria->addCondition("is_sent = 0");
        $criteria->addCondition("type = " . Email::NORMAL);

        $queues = Email::model()->findAll($criteria);

        foreach ($queues as $email) {
            $email->sendMail();
        }
    }
    
    /**
     * @desc send daily email 
     */
    public function actionDailyMail()
    {
        $criteria = new CDbCriteria;
        $criteria->limit = param('sendMailLimit');
        $criteria->addCondition("is_sent = 0");
        $criteria->addCondition("type = " . Email::DAILY);

        $queues = Email::model()->findAll($criteria);

        foreach ($queues as $email) {
            $email->sendMail();
        }
    }

    public function actionEmailTemplate()
    {
        EmailHelper::templateDefault(1, $this);
    }
}
