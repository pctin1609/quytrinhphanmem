<?php

// Include yii framework
$comDir = dirname(__FILE__) . '/../../common/';
require_once($comDir . 'vendors/yii/1.1.14/yii.php');

$protectedDir = $comDir.'../';
require($protectedDir . 'globals.php');
$appDir = $protectedDir . 'apps/';
$componentDir = dirname(__FILE__).'/components';

//set alias
Yii::setPathOfAlias('common', $comDir);
Yii::setPathOfAlias('app', $appDir);
Yii::setPathOfAlias('components', $componentDir);

// Load configs
$comConfigDir = $comDir . 'configs/';
$appConfigDir = dirname(__FILE__) . '/configs/';

$baseName = 'main.php';
$localName = 'local/' . $baseName;

$appBaseConfigs = require($appConfigDir . $baseName);
$appLocalConfigs = array();
if (is_file($appConfigDir . $localName))
    $appLocalConfigs = require($appConfigDir . $localName);

$comBaseConfigs = require($comConfigDir . $baseName);
$comLocalConfigs = array();
if (is_file($comConfigDir . $localName))
    $comLocalConfigs = require($comConfigDir . $localName);

$config = CMap::mergeArray($comBaseConfigs, $comLocalConfigs, $appBaseConfigs, $appLocalConfigs);

require_once($comDir . 'vendors/yii/1.1.14/yiic.php');