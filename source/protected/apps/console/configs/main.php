<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'params' => require(dirname(__FILE__) . '/params.php'),
    'import' => array(
        'components.*',
	),
);