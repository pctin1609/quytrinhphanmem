<?php
echo "<?php\n";
echo "\$label = \$model->isNewRecord ? 'Create' : 'Update';\n";
echo "\$form = \$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => '{$this->controller}-form',
	'type' => 'horizontal',
));\n";
echo "?>\n";
?>
<fieldset>
	<legend><?php echo "<?php echo \$label ?>" ?> <?php echo $this->modelClass ?></legend>
	<?php
	echo "<?php\n";
	foreach ($this->tableSchema->columns as $column) {
		if ($column->autoIncrement)
			continue;
		echo "\techo " . $this->generateActiveRow($this->modelClass, $column) . ";\n";
	}
	echo "\t?>\n";
    ?>
</fieldset>
<div class="form-actions">
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => \$label)); ?>\n"; ?>
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>\n" ?>
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('{$this->controller}/index'), 'label' => 'Cancel')); ?>\n" ?>
</div>
<?php echo "<?php \$this->endWidget(); ?>" ?>