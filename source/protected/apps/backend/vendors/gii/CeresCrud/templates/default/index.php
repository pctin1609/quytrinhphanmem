<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
echo "\$this->widget('app.widgets.BreadCrumbs');\n";
echo "\$this->widget('app.widgets.Tabs');\n";
echo "\$this->renderPartial('//layouts/partial/notification');\n";
echo "?>\n";

echo "<h4>\n";
echo "List " . $this->modelClass . "\n";
echo "<?php\n";
echo "\$this->widget('bootstrap.widgets.TbButton', array(
	'label' => \"Create {$this->modelClass}\",
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('{$this->controller}/create'),
	'htmlOptions' => array('class' => 'pull-right'),
	));\n";
echo "?>\n";
echo "</h4>\n";

echo "<?php\n";
echo "\$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => \$model->search(),
	'filter' => \$model,
	'pagerCssClass' => 'pagination pull-right',
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'name' => 'id',
			'selectableRows' => 2,
		),\n";
$count = 0;
foreach($this->tableSchema->columns as $column) {
	if ($count < 7) {
		echo "\t\t'{$column->name}',\n";
		$count++;
	}
}
echo "\t\tarray(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header' => 'Actions',
		)
	),
));\n";

echo "\$this->widget('app.widgets.BulkActions');\n";
echo "?>";
?>