<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php 
echo "<?php\n";
echo "\$this->widget('app.widgets.BreadCrumbs');\n";
echo "\$this->widget('app.widgets.Tabs');\n";
echo "?>\n";
?>
<h4>
View <?php echo $this->modelClass." #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>\n"; ?>
<?php 
echo "<?php\n";
echo "\$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update $this->modelClass',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('{$this->controller}/update', array('id' => \$model->{$this->tableSchema->primaryKey})),
	'htmlOptions' => array('class' => 'pull-right'),
));\n";
echo "?>\n";
?>
</h4>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
<?php
foreach ($this->tableSchema->columns as $column)
	echo "\t\t'" . $column->name . "',\n";
?>
	),
)); ?>