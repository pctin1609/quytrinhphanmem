<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php 
echo "<?php\n";
echo "\$this->widget('app.widgets.BreadCrumbs');\n";
echo "\$this->widget('app.widgets.Tabs');\n";
echo "\$this->renderPartial('//layouts/partial/notification');\n";
echo "\$this->renderPartial('_form', array('model' => \$model));\n";
echo "?>";
?>