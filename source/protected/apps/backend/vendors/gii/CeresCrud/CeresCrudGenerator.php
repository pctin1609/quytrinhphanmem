<?php

Yii::import('gii.generators.crud.CrudGenerator');

class CeresCrudGenerator extends CrudGenerator
{
	public $codeModel = 'app.vendors.gii.CeresCrud.CeresCrudCode';
}