<?php

Yii::import('gii.generators.crud.CrudCode');

class CeresCrudCode extends CrudCode
{
	public function generateActiveRow($modelClass, $column)
	{
		if ($column->type === 'boolean') {
			return "\$form->checkBoxRow(\$model, '{$column->name}')";
		}
		
		switch ($column->dbType) {
			case 'tinytext':
				return "\$form->textAreaRow(\$model, '{$column->name}', array('rows' => 6, 'cols' => 50, 'class' => 'span8'))";
				break;
			case 'text':
				return "\$form->redactorRow(\$model, '{$column->name}', array('class' => 'span4', 'rows' => 5));";
				break;
		}
		
		$inputField = 'textFieldRow';
		if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
			$inputField = 'passwordFieldRow';
		}

		if ($column->type !== 'string' || $column->size === null) {
			return "\$form->{$inputField}(\$model, '{$column->name}', array('class' => 'span5'))";
		}
		
		return "\$form->{$inputField}(\$model, '{$column->name}', array('class' => 'span5', 'maxlength' => $column->size))";
	}
}