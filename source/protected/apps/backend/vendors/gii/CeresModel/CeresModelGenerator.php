<?php

Yii::import('gii.generators.model.ModelGenerator');

class CeresModelGenerator extends ModelGenerator
{
	public $codeModel = 'app.vendors.gii.CeresModel.CeresModelCode';
}