<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Quy Trinh Phan Mem Administration',
    'preload' => array('bootstrap'),
    'defaultController' => 'user',
    'components' => array(
        'user' => array(
            'loginUrl' => 'user/login',
        ),
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            'responsiveCss' => true,
        ),
        'widgetFactory' => array(
             'widgets' => array(
                 'XUpload' => array(
                    'formView' => 'app.views.layouts.partial.upload_form',
                    'downloadView' => 'app.views.layouts.partial.download_template',
                    'options' => array(
                        'maxFileSize' => 3000000,
                        'acceptFileTypes' => 'js:/\.(gif|jpg|jpeg|png|pdf|doc|docx|txt|xls|xlsx|zip)$/i'
                    )
                ),
             )
        )
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'generatorPaths' => array(
                'app.vendors.gii',
                'bootstrap.gii',
            ),
            'password' => '123456',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    'params' => require(dirname(__FILE__) . '/params.php'),
    'aliases' => array(
        'xupload' => 'common.widgets.xupload'
    ),
);