<?php

class ChangeStatusAction extends CAction
{
	public function run()
	{
		if (app()->request->isAjaxRequest) {
            $id = app()->request->getParam('id');
			// we only allow deletion via POST request
			$model = $this->controller->getModel(null)->findByPk($id);
            
            if ($model->status == GActiveRecord::STATUS_ACTIVE)
                $model->status = GActiveRecord::STATUS_DEACTIVE;
            elseif ($model->status == GActiveRecord::STATUS_DEACTIVE)
                $model->status = GActiveRecord::STATUS_ACTIVE;
            
            $model->save();
            echo $model->status;
            app()->end();
		}
	}
}