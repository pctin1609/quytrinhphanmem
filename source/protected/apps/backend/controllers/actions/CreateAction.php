<?php

class CreateAction extends CAction
{

    public function run()
    {
        $model = $this->controller->getModel('create');

        if (isset($_POST[$this->controller->modelClass])) {
            $model->attributes = $_POST[$this->controller->modelClass];
            if ($model->save()) {
                user()->setFlash('success', $this->controller->modelClass . ' has been created successfully!');
                $this->controller->redirect(url($this->controller->id));
            }
        }

        $this->controller->render('create', array(
            'model' => $model,
        ));
    }

}