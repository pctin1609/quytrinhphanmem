<?php

class UpdateAction extends CAction
{
	public function run($id)
	{
        $model = $this->controller->getModel(null)->findByPk($id);
        $model->scenario = 'update';

        if (isset($_POST[$this->controller->modelClass])) {
            $model->attributes = $_POST[$this->controller->modelClass];
            if ($model->save()) {
                user()->setFlash('success', $this->controller->modelClass . ' has been updated successfully!');
                $this->controller->redirect(url($this->controller->id));
            }
        }

        $this->controller->render('update', array(
            'model' => $model,
        ));
	}
}