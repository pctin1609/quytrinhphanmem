<?php

class EmailTimeframeController extends Controller
{
	public $modelClass = 'EmailTimeframe';

    public function actionDelete($id)
    {
        if (app()->request->isPostRequest) {
            $result = EmailTemplate::model()->count(array('condition' => 'timeframe_id = ' . $id));

            if ($result == 0) {
                // we only allow deletion via POST request
                $this->getModel(null)->findByPk($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if (!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                else app()->end();
            }
        }
    }
}