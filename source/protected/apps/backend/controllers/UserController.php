<?php
/*
  * @property User $model
 * */
class UserController extends UserControllerBase
{
    public $modelClass = 'User';

    public function actionView($id)
    {
        $model = $this->getModel(null)->findByPk($id);
        
        $this->render('view', array('model' => $model));
    }

    public function actionCreate()
    {
        $model = $this->getModel('create');
        Utils::ajaxValidation($model, 'user-create-form');
        
        if (isset($_POST[$this->modelClass])) {
            $model->attributes = $_POST[$this->modelClass];
            $model->status = User::STATUS_ACTIVE;
            // create random password for user
            $model->password = $password = randomString(8);
            
            if ($model->save()) {
                user()->setFlash('success', $this->modelClass . ' has been created successfully!');
                $params = array(
                    'to' => array($model->email, $model->email),
                    'data' => array(
                        'EMAIL' => $model->email,
                        'PASSWORD' => $password,
                        'FULLNAME' => $model->getFullName(),
                    ),
                );
                CeresMail::queue('userCreate', $params);
                $this->redirect(array('index'));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel('update')->findByPk($id);
        $model->scenario = 'updateUser';
        if (isset($_POST[$this->modelClass])) {
            $model->attributes = $_POST[$this->modelClass];
            if ($model->save()) {
                if ($model->status == User::STATUS_DELETE) {
                    $params['to'] = array($model->getFullName(), $model->email);
                    $params['data'] = array(
                        "NAME" => $model->getFullName()
                    );
                    CeresMail::queue('deleteAccount', $params);
                } 
                user()->setFlash('success', $this->modelClass . ' has been created successfully!');
                $this->redirect(array('index'));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

}