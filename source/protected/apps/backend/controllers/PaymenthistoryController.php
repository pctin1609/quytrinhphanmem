<?php

class PaymentHistoryController extends Controller
{
	public $modelClass = 'PaymentHistory';
        public function actionCreate()
        {
            $model = $this->getModel('create');

            if (isset($_POST[$this->modelClass])) {
            $model->attributes = $_POST[$this->modelClass];
            if ($model->save()) {
                user()->setFlash('success', $this->modelClass . ' has been created successfully!');
                $this->redirect(url($this->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
        }
}