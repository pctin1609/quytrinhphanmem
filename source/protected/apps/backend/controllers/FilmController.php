<?php

class FilmController extends Controller
{
    public $modelClass = 'Film';

    public function actionCreate()
    {
        Yii::import('xupload.models.XUploadForm');
        $xUploadForm = new XUploadForm();
        $model = $this->getModel('create');
        
        Utils::ajaxValidation(array($xUploadForm, $model), 'film-form');

        if (isset($_POST[$this->modelClass])) {
            $model->attributes = $_POST[$this->modelClass];
            $model->file_id = $this->createFile();
            if ($model->save()) {
                user()->setFlash('success', $this->modelClass . ' has been created successfully!');
                $this->redirect(url($this->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'xUploadForm' => $xUploadForm,
        ));
    }
    
    public function createFile($folder = 'films', $file_id = 0)
    {
        $path = $folder;

        if (app()->user->hasState('uploadFiles')) {
            // Delete Old file
            if ($file_id > 0) {
                $file = File::model()->findByPk($file_id);
                $uploadFolder = Utils::uploadPathBackend();
                if (is_file($uploadFolder . $file->source))
                    unlink ($uploadFolder . $file->source);
                $file->delete();
            }
            $files = app()->user->getState('uploadFiles');
            $file = end($files);
            if (is_file($file["path"])) {
                $filePath = Utils::uploadPath($path);
                @mkdir($filePath, 0777, true);
                $filePath .= '/' . $file["filename"];
                if (rename($file["path"], $filePath)) { // Move from tmp folder to real folder
                    chmod($filePath, 0777);

                    $fileModel = new File();
                    $fileModel->name = $file['name'];
                    $fileModel->size = $file['size'];
                    $fileModel->extension = pathinfo($file['name'], PATHINFO_EXTENSION);
                    $fileModel->source = $path . '/' . $file["filename"];
                    if ($fileModel->save()) {
                        Utils::resetUploadFiles();
                        return $fileModel->id;
                    }
                }
            }
        }
    }
    
    public function actionUpdate($id)
    {
        Yii::import('xupload.models.XUploadForm');
        $xUploadForm = new XUploadForm();
        $model = $this->getModel(null)->findByPk($id);
        $model->scenario = 'update';

        Utils::ajaxValidation(array($xUploadForm, $model), 'film-form');
        
        if (isset($_POST[$this->modelClass])) {
            $model->attributes = $_POST[$this->modelClass];
            
            $newFile = $this->createFile('films', $model->file_id);
            if(!empty($newFile))
                $model->file_id = $newFile;
            
            if ($model->save()) {
                user()->setFlash('success', $this->modelClass . ' has been updated successfully!');
                $this->redirect(url($this->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'xUploadForm' => $xUploadForm,
        ));
    }
}