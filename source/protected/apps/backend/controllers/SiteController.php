<?php

class SiteController extends Controller
{
    public function actionError()
    {
        if ($error = app()->errorHandler->error) {
            if (app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', array('error' => $error));
            }
        }
    }
}