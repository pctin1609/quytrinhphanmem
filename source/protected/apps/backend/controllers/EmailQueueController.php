<?php

class EmailQueueController extends Controller
{
	public $modelClass = 'EmailQueue';

    public function actionIndex()
    {
        $model = $this->getModel('search');
        $model->unsetAttributes();
        if (isset($_GET[$this->modelClass])) {
            $model->attributes = $_GET[$this->modelClass];
        }
        $this->render('index', array('model' => $model));
    }
}