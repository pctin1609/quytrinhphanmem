<?php

class NavBar extends CWidget
{
	public $autoActive = true;
	public $type = 'main'; // can be "main" or "sub"

	public function run()
	{
		$dataPath = Utils::dataViewPath('navigations');
		
		switch ($this->type) {
			case 'main':
				$items = include $dataPath . '/main.php';
				$items = $this->prepareMainData($items);
				$this->render('navigations/main', array('items' => $items));
				break;
			case 'sub':
				$items = include $dataPath . '/sub.php';
				$items = $this->prepareSubData($items);
				$this->render('navigations/sub', array('items' => $items));
				break;
		}
	}
	
	public function prepareMainData($data)
	{
		$items = array();
		if (!empty($data)) {
			foreach ($data as $index => $menu) {
				if (is_array($menu)) {
					$i = 0;
					foreach ($menu as $r => $title) {
						$items[$index][$i]['label'] = t($title);
						$items[$index][$i]['url'] = url($r);
						
						$r = explode('/', $r);
						
						$isActive = null;
						if (!$this->autoActive) {
							$isActive = app()->controller->isActiveMainItem($r[0], $r[1]);
						}
						if ($isActive === null)
							$isActive = $this->isActiveControllerAndAction($r[0], $r[1]);
							
						if ($isActive === true)
							$items[$index][$i]['itemOptions'] = array('class' => 'active');
						$i++;
					}
				}
			}
		}
		
		return $items;
	}
	
	public function prepareSubData($data)
	{
		$items = array();
		if (!empty($data)) {
			$i = 0;
			foreach ($data as $index => $itemInfo) {
				if ($i % 2) {
					$items[$i++] = '';
				}
				
				$cons = array();
				if (is_array($itemInfo[1])) { // have sub-items
					$items[$i]['label'] = t($itemInfo[0]);
					$j = 0;
					foreach ($itemInfo[1] as $c => $title) {
						if ($j % 2) {
							$items[$i]['items'][$j++] = '';
						}
						
						$cons[] = $c;
						
						$items[$i]['items'][$j]['label'] = t($title);
						$items[$i]['items'][$j]['url'] = url($c);
						
						$j++;
					}
				} else { // no sub-items
					$items[$i]['label'] = t($itemInfo[1]);
					$items[$i]['url'] = url($itemInfo[0]);
					
					$cons[] = $itemInfo[0];
				}
				
				foreach ($cons as $c) {
                    $isActive = null;
					if (!$this->autoActive)
						$isActive = app()->controller->isActiveSubItem($c);
					if ($isActive === null)
						$isActive = $this->isActiveControllerAndAction($c, null);
						
					if ($isActive === true) {
						$items[$i]['itemOptions'] = array('class' => 'active');
						break;
					}
				}
				
				$i++;	
			}
		}
		
		return $items;
	}
	
	public function isActiveControllerAndAction($c, $a = null)
	{
		$valid = false;
		if (!empty($c)) {
			$cId = app()->controller->id;
			if ($cId === $c) {
				if (!empty($a)) {
					$cA = app()->controller->action->id;
					if ($cA === $a)
						$valid = true;
				} else $valid = true;
			}
		}
		
		return $valid;
	}
}