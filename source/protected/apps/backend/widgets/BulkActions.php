<?php

class BulkActions extends CWidget 
{
	public $actions = null;
	public $emptyMsg = 'Please select at least one item to perform this action!';
	public $confirmMsg = 'Are you sure you want to {action} selected {model}?';
	
	public function run()
	{
		clearstatcache();

		$dataPath = Utils::dataViewPath('bulkActions');
		
		// Common actions
		$commonActions = array();
		if ($this->actions !== array()) {
			$commonActions = include $dataPath . '/' . '_common.php';			
			if (is_array($this->actions)) {
				$commonActions = Utils::subArrayByKeys($commonActions, $this->actions);
			}
		}
		
		// Private actions
		$privateActions = array();
		$dataFile = $dataPath . '/' . app()->controller->id . '.php';
		if (is_file($dataFile)) {
			$privateActions = include $dataFile;
		}
				
		// Standardize data
		$commonActions = $this->standardizeData($commonActions);
		$privateActions = $this->standardizeData($privateActions);
		if (!empty($commonActions) && !empty($privateActions))
			$commonActions[] = '---';
		
		// Register scripts
		$this->registerScripts();
		
		// Render
		$this->render('bulk_actions', array('items' => array_merge($commonActions, $privateActions)));
	}
	
	private function standardizeData($data)
	{
		$result = array();
		if (is_array($data) && !empty($data)) {
			$i = 0;
			$model = str_replace('_', ' ', app()->controller->getModel(null)->tableName());
			foreach ($data as $item) {
				// Title
				if (!isset($item['title']))
					$item['title'] = ucfirst($item['action']);
				
				// Confirm message
				if (!isset($item['confirm'])) {
					$action = strtolower($item['title']);
					$item['confirm'] = str_replace(array('{action}', '{model}'), array($action, $model), $this->confirmMsg);
				}
					
				// Url
				if (!isset($item['params']))
					$item['params'] = array();
                if (app()->controller->action->id == 'users')
				    $item['url'] = url(app()->controller->id . '/backendBulk' . ucfirst($item['action']), $item['params']);
                else
                    $item['url'] = url(app()->controller->id . '/bulk' . ucfirst($item['action']), $item['params']);
				
				$result[$i]['label'] = t($item['title']);
				$result[$i]['url'] = '#';
				$result[$i]['linkOptions']['bulkUrl'] = $item['url'];
				if (isset($item['confirm']))
					$result[$i]['linkOptions']['confirmMsg'] = $item['confirm'];
				$i++;
			}
		}
		return $result;
	}
	
	public function registerScripts()
	{
		$chkName = BulkAction::$_chkName;
		$script = <<<EOD
		jQuery(document).on('click', 'div.dropup ul.dropdown-menu a', function() {
			// Hide menu
			jQuery('div.dropup').removeClass('open');
			
			// Process the action
			var selectedItems = jQuery('div.grid-view tbody td.checkbox-column input:checked');
			if (selectedItems.length == 0) {
				alert('{$this->emptyMsg}');
				return false;
			}
			var linkObj = jQuery(this);	
			if (!confirm(linkObj.attr('confirmMsg'))) return false;
			data = {
				{$chkName}: new Array()
			}
			for (var i = 0; i < selectedItems.length; i++)
				data.{$chkName}.push(selectedItems.eq(i).val());
			
			jQuery('div.grid-view').yiiGridView('update', {
				type: 'POST',
				data: data,
				url: linkObj.attr('bulkUrl'),
				success: function(data) {
					jQuery('div.grid-view').yiiGridView('update');
				}
			});			
			return false;
		});
EOD;
		cs()->registerScript('grid-view-bulk-actions-scripts', $script); 
	}
}