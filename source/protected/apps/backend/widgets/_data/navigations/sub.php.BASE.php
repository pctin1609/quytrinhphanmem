<?php

return array(
    array('user', 'Manage Users'),
    array('film', 'Manage Films'),
    array('Manage Emails',
        array(
            'emailTemplate' => 'Email Templates',
            'emailTimeframe' => 'Timeframes',
            'emailQueue' => 'Email Queues'
        )
    ),
);