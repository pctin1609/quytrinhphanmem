<?php

return array(
    array('user', 'Manage Users'),
    array('film', 'Manage Films'),
    array('downloadHistory', 'Download History'),
    array('Manage Emails',
        array(
            'emailTemplate' => 'Email Templates',
            'emailTimeframe' => 'Timeframes',
            'emailQueue' => 'Email Queues'
        )
    ),
);