<?php

return array(
    array('user', 'Manage Users'),
    array('downloadHistory', 'Download History'),
    array('packetOfCretdit', 'Packet Of Cretdit'),
    array('paymentHistory', 'Payment History'),
    array('Manage Films', array(
        'film' => 'Manage Films',
        'category' => 'Manage Category',
    )),
    array('Manage Emails',
        array(
            'emailTemplate' => 'Email Templates',
            'emailTimeframe' => 'Timeframes',
            'emailQueue' => 'Email Queues'
        )
    ),
    array('watchFullHistory', 'Watch Full History'),
    array('watchTrainerHistory', 'Watch Trailler History'),
);