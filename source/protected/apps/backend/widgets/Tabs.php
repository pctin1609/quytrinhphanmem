<?php

class Tabs extends CWidget
{
	public function run()
	{
		$dataPath = Utils::dataViewPath('navigations');
		$items = include $dataPath . '/sub.php';
		$items = $this->prepareData($items);
		
		$this->render('tabs', array('items' => $items));
	}
	
	public function prepareData($data)
	{
		$items = array();
		if (!empty($data)) {
			$tmpItems = array();
			$cId = app()->controller->id;
			foreach ($data as $index => $itemInfo) {
				if (is_array($itemInfo[1])) {
					foreach ($itemInfo[1] as $c => $title) {
						if ($cId == $c) {
							$tmpItems = $itemInfo[1];
							break;
						}
					}
				}
				if (!empty($tmpItems))
					break;
			}
			
			if (!empty($tmpItems)) {
				$i = 0;
				foreach ($tmpItems as $c => $title) {
					$items[$i]['label'] = t($title);
					$items[$i]['url'] = url($c);
					
					if ($cId == $c)
						$items[$i]['active'] = true;

					$i++;
				}
			}
		}
		
		return $items;
	}
}