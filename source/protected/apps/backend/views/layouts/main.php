<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?php echo Yii::app()->name ?></title>
		<link href="/backend/css/app.css" rel="stylesheet" />
        <meta charset="utf-8">
	</head>
	<body>
		<?php
		$this->widget(
			'app.widgets.NavBar',
			array('autoActive' => $this->autoActiveNav)
		);		
		?>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span2">
					<div class="well sidebar-nav">
						<?php
						$this->widget(
							'app.widgets.NavBar', 
							array('type' => 'sub', 'autoActive' => $this->autoActiveNav)
						);
						?>
					</div>
				</div>
				<div class="span10">
					<?php echo $content ?>
				</div>
			</div>
		</div>
	</body>
</html>