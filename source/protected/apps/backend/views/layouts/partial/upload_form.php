<div class="row fileupload-buttonbar">
    <div class="span7">
		<span class="btn btn-success fileinput-button">
            <i class="icon-plus icon-white"></i>
            <span><?php echo $this->t('1#Add files|0#Choose file', $this->multiple); ?></span>
            <?php
            if ($this->hasModel())
                echo CHtml::activeFileField($this->model, $this->attribute, $htmlOptions);
            else echo CHtml::fileField($name, $this->value, $htmlOptions);
            ?>
		</span>
        <?php if ($this->multiple) { ?>
            <button type="submit" class="btn btn-primary start">
                <i class="icon-upload icon-white"></i>
                <span>Upload</span>
            </button>
            <button type="reset" class="btn btn-warning cancel">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancel</span>
            </button>
            <?php if (app()->controller->id != 'cms/media') { ?>
            <button type="button" class="btn btn-danger delete">
                <i class="icon-trash icon-white"></i>
                <span>Delete</span>
            </button>
            <input type="checkbox" class="toggle">
            <?php } ?>
        <?php } ?>
    </div>
</div>
<div class="fileupload-loading"></div>
<table class="table table-striped">
    <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody>
</table>