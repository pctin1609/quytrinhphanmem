<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
    List User
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => "Create User",
        'icon' => 'icon-plus icon-white',
        'type' => 'primary',
        'size' => 'medium',
        'url' => url('user/create'),
        'htmlOptions' => array('class' => 'pull-right'),
    ));
    ?>
</h4>
<?php
/** @var User $data */
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'pagerCssClass' => 'pagination pull-right',
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'name' => 'id',
            'selectableRows' => 2,
        ),
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '50px'),
        ),
        'first_name',
        'last_name',
        'email',
        'phone',
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()',
            'filter' => CHtml::activedropDownList($model, 'status', Utils::getStatusOptions(true), array('prompt' => '---Choose status---', 'style' => 'min-width:160px')),
            'htmlOptions' => array('style' => 'width: 120px; text-align: center;'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'header' => 'Actions',
            'template' => '{view} {update} {delete}',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'buttons' => array(
                'delete' => array(
                    'visible' => '$data->status != User::STATUS_DELETE'
                ),

            ),
        )
    ),

));
$this->widget('app.widgets.BulkActions');

?>