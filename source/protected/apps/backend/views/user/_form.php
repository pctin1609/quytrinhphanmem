<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'user-create-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
));
?>
<fieldset>
    <legend><?php echo $label ?> User</legend>
    <?php
    echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 255, 'disabled' => $this->action->id == 'update' ? 'disabled' : ''));
    echo $form->textFieldRow($model, 'first_name', array('class' => 'span5'));
    echo $form->textFieldRow($model, 'last_name', array('class' => 'span5'));
    echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 15));
    echo $form->textFieldRow($model, 'address', array('class' => 'span5'));
    ?>
</fieldset>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('user/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>