<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
    View User #<?php echo $model->id; ?>
</h4>
<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        array(
            'name' => 'created_date',
            'value' => date('Y-m-d', $model->created_date)
        ),
        array(
            'name' => 'updated_date',
            'value' => date('Y-m-d', $model->created_date)
        ),
    ),
));
?>