<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'film-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
    ),
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    )
));
?>
<fieldset>
    <legend><?php echo $label ?> Film</legend>
    <?php
    $categoryList = Category::model()->findAll('status = ' . Category::STATUS_ACTIVE);
    echo $form->dropdownListRow($model, 'category_id', CHtml::listData($categoryList, 'id', 'name'), array('class' => 'span3', 'empty' => '--Select Category--'));
    echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 64));
    echo $form->redactorRow($model, 'description', array('class' => 'span4', 'rows' => 5));
    ;
    echo $form->textFieldRow($model, 'credits', array('class' => 'span5'));
    echo $form->textFieldRow($model, 'director', array('class' => 'span5', 'maxlength' => 64));
    echo $form->textFieldRow($model, 'author', array('class' => 'span5', 'maxlength' => 32));
    echo $form->datePickerRow($model, 'date_publish', array('options' => array('language' => 'es', 'format' => 'yyyy-mm-dd'), 'class' => 'span5'));
    ?>
    
    <div class="control-group">
        <?php echo $form->labelEx($model, 'file_id', array('class' => 'control-label')) ?>
        <div class="controls" style="padding-left: 30px;">
            <?php
            $this->widget('xupload.XUpload', array(
                'url' => url('/film/upload', array('folder' => 'films')),
                'model' => $xUploadForm,
                'attribute' => 'file',
                'multiple' => false,
                'htmlOptions' => array('id' => 'film-form'),
                'options' => array(
                    'maxFileSize' => 10 * 1024 * 1024 * 1024,
                    'acceptFileTypes' => 'js:/\.(M4P|AVI|FLV|MP4|M4V|MOV|WMV|MKV)$/i',
                    'success' => "js:function() {
                        " . ($model->isNewRecord ? "$('#Film_file_id').val('-2');" : "" ) . "
                    }",
                    'destroyed' => "js:function() {
                        " . ($model->isNewRecord ? "$('#Film_file_id').val('');" : "" ) . "
                    }",
                )
            ));
            ?>
            <?php echo $form->hiddenField($model, 'file_id') ?>
            <?php echo $form->error($model, 'file_id') ?>
        </div>
    </div>
    
    <?php
    echo $form->dropdownListRow($model, 'status', $model->getStatusOptions(), array('class' => 'span3'));
    ?>
</fieldset>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('film/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>