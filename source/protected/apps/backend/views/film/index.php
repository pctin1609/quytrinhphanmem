<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
$categoryList = Category::model()->findAll('status != ' . Category::STATUS_DELETE);
?>
<h4>
    List Film
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => "Create Film",
        'type' => 'primary',
        'size' => 'medium',
        'url' => url('film/create'),
        'htmlOptions' => array('class' => 'pull-right'),
    ));
    ?>
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'pagerCssClass' => 'pagination pull-right',
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'name' => 'id',
            'selectableRows' => 2,
        ),
        'id',
        array(
            'name' => 'category_id',
            'type' => 'raw',
            'value' => 'CHtml::link($data->category->name, url("/category/view", array("id" => $data->category_id)), array("title" => "View"))',
            'filter' => CHtml::activedropDownList($model, 'category_id', CHtml::listData($categoryList, 'id', 'name'), array('prompt' => '---Choose category---', 'style' => 'min-width:200px')),
        ),
        'title',
        'credits',
        'director',
        'author',
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()',
            'filter' => CHtml::activedropDownList($model, 'status', Utils::getStatusOptions(true), array('prompt' => '---Choose status---', 'style' => 'min-width:160px')),
            'htmlOptions' => array('style' => 'width: 120px; text-align: center;'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'header' => 'Actions',
        )
    ),
));
$this->widget('app.widgets.BulkActions');
?>