<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View Film #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update Film',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('film/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'title',
		'description',
		'credits',
		'director',
		'author',
		'date_publish',
		'path',
		'updated_date',
		'created_date',
		'status',
	),
)); ?>