<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'downloadHistory-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Download History</legend>
	<?php
            echo $form->textFieldRow($model, 'user_id', array('class' => 'span5'));
            echo $form->textFieldRow($model, 'film_id', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('downloadHistory/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>