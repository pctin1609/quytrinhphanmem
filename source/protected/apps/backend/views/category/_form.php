<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'category-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Category</legend>
	<?php
	echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 64));
	echo $form->redactorRow($model, 'description', array('class' => 'span4', 'rows' => 5));;
	echo $form->dropdownListRow($model, 'status', $model->getStatusOptions(), array('class' => 'span3'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('category/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>