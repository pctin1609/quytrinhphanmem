<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
    View Category #<?php echo $model->id; ?>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Update Category',
        'type' => 'primary',
        'size' => 'medium',
        'url' => url('category/update', array('id' => $model->id)),
        'htmlOptions' => array('class' => 'pull-right'),
    ));
    ?>
</h4>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'name',
        'description',
        array(
            'name' => 'created_date',
            'value' => date('Y-m-d', $model->created_date)
        ),
        array(
            'name' => 'updated_date',
            'value' => date('Y-m-d', $model->created_date)
        ),
        array(
            'name' => 'status',
            'value' => $model->getStatusText()
        ),
    ),
));
?>