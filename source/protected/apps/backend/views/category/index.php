<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
    List Category
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => "Create Category",
        'type' => 'primary',
        'size' => 'medium',
        'url' => url('category/create'),
        'htmlOptions' => array('class' => 'pull-right'),
    ));
    ?>
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'pagerCssClass' => 'pagination pull-right',
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'name' => 'id',
            'selectableRows' => 2,
        ),
        'id',
        'name',
        array(
            'name' => 'status',
            'value' => '$data->getStatusText()',
            'filter' => CHtml::activedropDownList($model, 'status', Utils::getStatusOptions(true), array('prompt' => '---Choose status---', 'style' => 'min-width:160px')),
            'htmlOptions' => array('style' => 'width: 120px; text-align: center;'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'header' => 'Actions',
        )
    ),
));
$this->widget('app.widgets.BulkActions');
?>