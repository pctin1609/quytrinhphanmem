<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'emailTimeframe-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> EmailTimeframe</legend>
	<?php
	echo $form->textFieldRow($model, 'desc', array('class' => 'span5', 'maxlength' => 100));
	echo $form->textFieldRow($model, 'value', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('emailTimeframe/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>