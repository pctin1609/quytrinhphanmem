<div id="myflashwrapper" style="display: none;">

</div>
<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
List Email Timeframe
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => "Create Email Timeframe",
    'icon' => 'icon-plus icon-white',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('emailTimeframe/create'),
	'htmlOptions' => array('class' => 'pull-right'),
	));
?>
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'pagerCssClass' => 'pagination pull-right',
	'columns' => array(
		/*array(
			'class' => 'CCheckBoxColumn',
			'name' => 'id',
			'selectableRows' => 2,
		),*/
        array(
            'name' => 'id',
            'htmlOptions' => array('width'=>'50px'),
        ),
		'desc',
		'value',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header' => 'Actions',
            'htmlOptions' => array('class'=>'pull-right', 'style' => 'width:82%'),
            'buttons' => array(
                'delete' => array(
                    'visible' => '$data->allowDelete()'
                ),
            ),
		)
	),
));
//$this->widget('app.widgets.BulkActions');
?>