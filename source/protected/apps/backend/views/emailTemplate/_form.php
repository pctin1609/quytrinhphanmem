<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'emailTemplate-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> Email Template</legend>
	<?php
    echo $form->dropDownListRow($model, 'timeframe_id', CHtml::listData(EmailTimeframe::model()->findAll(), "id", "desc"));
	echo $form->textFieldRow($model, 'desc', array('class' => 'span5', 'maxlength' => 100));
	echo $form->toggleButtonRow($model, 'resend_if_fail');
    echo $form->toggleButtonRow($model, 'delete_if_success');
    //echo $form->textFieldRow($model, 'resend_if_fail', array('class' => 'span5'));
	//echo $form->textFieldRow($model, 'delete_if_success', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'from_name', array('class' => 'span5', 'maxlength' => 50));
	echo $form->textFieldRow($model, 'from_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'to_name', array('class' => 'span5', 'maxlength' => 100));;
	echo $form->textFieldRow($model, 'to_email', array('class' => 'span5', 'maxlength' => 100));;
	echo $form->textFieldRow($model, 'subject', array('class' => 'span5', 'maxlength' => 255));
	echo $form->redactorRow($model, 'content', array('width' => '500px', 'height' => '250px'));;
	echo $form->textFieldRow($model, 'reply_to', array('class' => 'span5', 'maxlength' => 100));
	echo $form->textFieldRow($model, 'cc', array('class' => 'span5', 'maxlength' => 100));;
	echo $form->textFieldRow($model, 'bcc', array('class' => 'span5', 'maxlength' => 100));;
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('emailTemplate/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>