<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
List Email Template
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'pagerCssClass' => 'pagination pull-right',
	'columns' => array(
		/*array(
			'class' => 'CCheckBoxColumn',
			'name' => 'id',
			'selectableRows' => 2,
		),*/
        array(
            'name' => 'id',
            'htmlOptions' => array('width'=>'50px'),
        ),
        array( 'name' => 'timeframeSearch', 'value' => '$data->timeframe->desc' ),
        array(
            'name' => 'desc',
            'htmlOptions' => array('width'=>'200px'),
        ),
		'from_name',
		'from_email',
		'to_name',
		'to_email',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header' => 'Actions',
            'buttons' => array(
                'view' => array('visible' => 'false'),
                'delete' => array('visible' => 'false'),
            ),
		)
	),
));
//$this->widget('app.widgets.BulkActions');
?>