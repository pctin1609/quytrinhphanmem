<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View EmailTemplate #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update EmailTemplate',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('emailTemplate/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'timeframe_id',
		'desc',
		'from_name',
		'from_email',
		'to_name',
		'to_email',
		'reply_to',
		'cc',
		'bcc',
		'subject',
		'content',
		'resend_if_fail',
		'delete_if_success',
	),
)); ?>