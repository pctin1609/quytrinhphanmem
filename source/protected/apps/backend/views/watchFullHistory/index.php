<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
List WatchFullHistory
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => "Create WatchFullHistory",
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('watchFullHistory/create'),
	'htmlOptions' => array('class' => 'pull-right'),
	));
?>
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'pagerCssClass' => 'pagination pull-right',
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'name' => 'id',
			'selectableRows' => 2,
		),
		'id',
		'user_id',
		'film_id',
		'latest_date',
		'watched_times',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header' => 'Actions',
		)
	),
));
$this->widget('app.widgets.BulkActions');
?>