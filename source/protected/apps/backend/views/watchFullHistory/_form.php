<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'watchFullHistory-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> WatchFullHistory</legend>
	<?php
        $listUsers = User::model()->findAll('status = ' . User::STATUS_ACTIVE);
        $listFilms = Film::model()->findAll('status = ' . Film::STATUS_ACTIVE);
   
	echo $form->dropdownListRow($model, 'user_id', CHtml::listData($listUsers, 'id', 'first_name'), array('class' => 'span3', 'empty' => '--Select User--'));
	echo $form->dropdownListRow($model, 'film_id', CHtml::listData($listFilms, 'id', 'title'), array('class' => 'span3', 'empty' => '--Select Film--'));;
	echo $form->datePickerRow($model, 'latest_date', array('options' => array('language' => 'es', 'format' => 'yyyy-mm-dd'), 'class' => 'span5'));
	echo $form->textFieldRow($model, 'watched_times', array('class' => 'span5'));
        echo $form->textFieldRow($model, 'real_credit', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('watchFullHistory/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>