<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View PaymentHistory #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update PaymentHistory',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('paymentHistory/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'id_User',
		'real_Price',
		'id_Packet',
		'date',
	),
)); ?>