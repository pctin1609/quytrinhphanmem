<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'paymentHistory-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> PaymentHistory</legend>
	<?php
	echo $form->textFieldRow($model, 'id_User', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'real_Price', array('class' => 'span5', 'maxlength' => 10));
	echo $form->textFieldRow($model, 'id_Packet', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'date', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('paymentHistory/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>