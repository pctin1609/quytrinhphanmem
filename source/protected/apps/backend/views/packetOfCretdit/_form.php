<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'packetOfCretdit-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> PacketOfCretdit</legend>
	<?php
	echo $form->textFieldRow($model, 'amount', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'price', array('class' => 'span5', 'maxlength' => 10));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('packetOfCretdit/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>