<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
?>
<h4>
View EmailQueue #<?php echo $model->id; ?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
	'label' => 'Update EmailQueue',
	'type' => 'primary',
	'size' => 'medium',
	'url' => url('emailQueue/update', array('id' => $model->id)),
	'htmlOptions' => array('class' => 'pull-right'),
));
?>
</h4>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
        array(
            'name' => 'template_id',
            'type' => 'raw',
            'value' => $model->getTemplate()->desc
        ),
		'from_name',
        array(
            'name' => 'from_email',
            'type' => 'email',
        ),
		'to_name',
        array(
            'name' => 'to_email',
            'type' => 'email',
        ),
        array(
            'name' => 'reply_to',
            'type' => 'email',
        ),
		'subject',
        array(
            'name' => 'content',
            'type' => 'html',
        ),
		'send_time',
		'sent_time',
		array(
            'label' => 'Status',
            'value' => $model->getQueueStatus()
        ),
	),
)); ?>