<?php
$label = $model->isNewRecord ? 'Create' : 'Update';
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'emailQueue-form',
	'type' => 'horizontal',
));
?>
<fieldset>
	<legend><?php echo $label ?> EmailQueue</legend>
	<?php
	echo $form->textFieldRow($model, 'template_id', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'from_name', array('class' => 'span5', 'maxlength' => 50));
	echo $form->textFieldRow($model, 'from_email', array('class' => 'span5', 'maxlength' => 255));
	echo $form->redactorRow($model, 'to_name', array('class' => 'span4', 'rows' => 5));;
	echo $form->redactorRow($model, 'to_email', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'reply_to', array('class' => 'span5', 'maxlength' => 255));
	echo $form->textFieldRow($model, 'subject', array('class' => 'span5', 'maxlength' => 255));
	echo $form->redactorRow($model, 'content', array('class' => 'span4', 'rows' => 5));;
	echo $form->textFieldRow($model, 'created_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'send_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'sent_time', array('class' => 'span5'));
	echo $form->textFieldRow($model, 'status', array('class' => 'span5'));
	?>
</fieldset>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => $label)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'reset', 'label' => 'Reset')); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'url' => url('emailQueue/index'), 'label' => 'Cancel')); ?>
</div>
<?php $this->endWidget(); ?>