<?php
$this->widget('app.widgets.BreadCrumbs');
$this->widget('app.widgets.Tabs');
$this->renderPartial('//layouts/partial/notification');
?>
<h4>
List Email Queue
</h4>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'pagerCssClass' => 'pagination pull-right',
    'columns' => array(
		/*array(
			'class' => 'CCheckBoxColumn',
			'name' => 'id',
			'selectableRows' => 2,
		),*/
        array(
            'name' => 'id',
            'htmlOptions' => array('width'=>'50px'),
        ),
		array(
            'header' => 'Template',
            'name' => 'template_id',
            'value' => '$data->template->desc',
            'filter' => CHtml::activedropDownList($model, 'template_id', CHtml::listData(EmailTemplate::model()->getEmailTemplate(), 'id', 'desc'), array('prompt' => '---Choose type---', 'style' => 'width:150px')),
            'htmlOptions'=>array('width'=>'140px'),
        ),
		'from_name',
		'from_email',
		'to_name',
		'to_email',
		array(
            'name' => 'status',
            'value' => '$data->getQueueStatus()',
            'header' => 'Status',
            'filter' => CHtml::activedropDownList($model, 'status', EmailQueue::model()->getListItem(), array('prompt' => '---Choose Status---', 'style' => 'width:160px')),
            'htmlOptions' => array('style' => 'width:5%')
        ),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'header' => 'Actions',
            'buttons' => array(
                'update' => array('visible' => 'false'),
                'delete' => array('visible' => 'false'),
            ),
		)
	),
));
//$this->widget('app.widgets.BulkActions');
?>