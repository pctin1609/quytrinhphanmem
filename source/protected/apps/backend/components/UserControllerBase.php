<?php

class UserControllerBase extends Controller
{
	public function actions()
	{
		$actionMaps = parent::actions();
		$actionMaps['logout'] = 'common.actions.LogoutAction';
		return $actionMaps;
	}
	
	public function actionLogin()
	{
        $user = user();

        // If user logged in, redirect to default page
        if (!$user->isGuest)
            $this->redirect(app()->baseUrl);

        // Log user in
	    $model = new LoginForm();
	    if (isset($_POST['LoginForm'])) {
	        $model->attributes = $_POST['LoginForm'];
	        $identity = new UserIdentity($model->username, $model->password);
            if ($identity->authenticate()) {
                $user->login($identity);
                $this->redirect(app()->baseUrl);
            } else {
                $user->setFlash('error', 'Invalid Username or Password!');
            }
	    }
	    
	    $this->layout = false;
		$this->render('login', array( 'model' => $model, )); 		
	}
}