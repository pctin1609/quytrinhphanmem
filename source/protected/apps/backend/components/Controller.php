<?php

class Controller extends GController
{

    public $autoActiveNav = true; // used by the app.widgets.NavBar and the layout
    public $modelClass = '';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('login', 'logout'),
                'users' => array('*'),
            ),
            array(
                'allow',
                'users' => array('admin'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        $aliasPath = 'app.controllers.actions.';
        return array(
            'create' => $aliasPath . 'CreateAction',
            'delete' => $aliasPath . 'DeleteAction',
            'index' => $aliasPath . 'IndexAction',
            'update' => $aliasPath . 'UpdateAction',
            'view' => $aliasPath . 'ViewAction',
            'bulkDelete' => $aliasPath . 'BulkDeleteAction',
            'changeStatus' => $aliasPath . 'ChangeStatusAction',
            'upload' => array(
                'class' => 'xupload.actions.XUploadAction',
                'path' => Utils::uploadTmpPath(),
                'publicPath' => Utils::uploadTmpUrl(),
                'subfolderVar' => 'folder',
                'secureFileNames' => true,
                'stateVariable' => 'uploadFiles'
            ),
        );
    }

    public function getModel($scenario = '')
    {
        if (empty($this->modelClass))
            $this->modelClass = ucfirst($this->id);

        $model = null;
        if ($scenario === null) {
            $model = @call_user_func(array($this->modelClass, 'model'));
        } else {
            $model = new $this->modelClass;
            $model->scenario = $scenario;
        }
        return $model;
    }

    /**
     * Control active behavior of the main navigation
     * @param String $c controller id
     * @param String $a action id
     */
    public function isActiveMainItem($c, $a)
    {
        return null;
    }

    /**
     * Control active behavior of the sub navigation
     * @param String $c controller id
     * @param String $a action id
     */
    public function isActiveSubItem($c)
    {
        return null;
    }

}