<?php

class PriceValidator extends CRegularExpressionValidator
{
    // 12,10 OR 12.10
    public $pattern = '/^[¥£$€]?[ ]?[-]?[ ]?[0-9]*[.,]{0,1}[0-9]{0,2}[ ]?[¥£$€]?$/';
}