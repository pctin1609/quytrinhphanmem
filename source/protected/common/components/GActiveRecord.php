<?php

class GActiveRecord extends CActiveRecord 
{
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_DELETE   = 2;
    
    /**
     * @desc set created time for a record
     * @return type 
     */
    protected function beforeSave()
    {
        $parent = parent::beforeSave();
        if ($this->isNewRecord) {
            if ($this->hasAttribute('created_date')) {
                if (empty($this->created_date))
                    $this->created_date = time();
            }
        }
        if ($this->hasAttribute('updated_date'))
            $this->updated_date = time();
        if ($this->hasAttribute('status') && is_null($this->status))
            $this->status = GActiveRecord::STATUS_ACTIVE;
        
        return $parent;
    }
    
    public function getStatusOptions()
    {
        return array(
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_DEACTIVE => 'In Active',
            self::STATUS_DELETE => 'Deleted',
        );
    }
    
    public function getStatusText()
    {
        $options = $this->getStatusOptions();
        return (isset($options[$this->status]) ? $options[$this->status] : 'Unknown');
    }
}