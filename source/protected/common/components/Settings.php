<?php

class Settings extends CApplicationComponent
{
    static $_settings = array();

    public function __get($name)
    {
        if (empty(self::$_settings)) {
            $settings = Setting::model()->findAll();
            foreach ($settings as $s) {
                self::$_settings[strtolower($s->key)] = $s->value;
            }
        }

        return isset(self::$_settings[$name]) ? self::$_settings[$name] : null;
    }
}