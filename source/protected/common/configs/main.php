<?php

return array(
    'preload' => array('log'),
    'import' => array(
        'common.models.*',
        'common.components.*',
        'common.controllers.*',
        // Application included paths must be set after common included paths.
        'app.models.*',
        'app.components.*',
        'common.extensions.ceres.components.*',
        'common.extensions.ceres.models.*',
    ),
    'components' => array(
        'clientScript' => array(
            'class' => 'GClientScript',
        ),
        'user' => array(
            'class' => 'GWebUser',
            'allowAutoLogin' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
            )
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=quytrinhphanmem',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
        'setting' => array(
            'class' => 'common.components.Settings'
        )
    ),
    'params' => require(dirname(__FILE__) . '/params.php'),
);