<?php

return array(
    // Core
    'siteUrl' => 'http://quytrinhphanmem.com',
    'salt' => '791ae620-e2f5-11db-8314-0800200c9a66-13245',
    'dateFormats' => array(
        'short' => 'Y-m-d',
        'long' => 'Y-m-d H:i:s',
        'timePicker' => 'm/d/Y H:i'
    ),
    'paypal' => array(
        'PAYPAL_API_USERNAME' => 'nhom13.business_api1.yahoo.com',
        'PAYPAL_API_PASSWORD' => '1387010098',
        'PAYPAL_API_SIGNATURE' => 'AVbHN.h15F9-Tq9ErdKrNOpyBPu2A6S-Ds4PnRsHJfFqu4AojDVSWtd.',
        'PAYPAL_IS_TEST' => '1',
        'CURRENCY_CODE' => 'USD',
    ),
    'creditPrice' => '2.50',
    'creditPerAuction' => 1, //Price of one auction
    'sizeLimit' => 2 * 1024 * 1024,
    'pageSize' => 10,
    'fileExtensions' => array(
        'image' => array('gif', 'png', 'jpg'),
    ),
    'subDomainExcludes' => array(
        'ourauction'
    ),
    'adminEmail' => 'admin@quytrinhphanmem.com',
    // Projects
    'ceresMail' => array(
        'host' => 'smtp.gmail.com',
        'port' => 465,
        'secure' => 'ssl',
        //'auth' => false,
        'auth' => array(
            'email' => 'pnhudinh@gmail.com',
            'password' => 'chanhtin'
        ),
        'auto' => array(
            'id' => '(auto)',
            'reply-to' => 'admin@quytrinhphanmem.com',
            'from' => array('System', 'admin@quytrinhphanmem.com'),
            'cc' => array(),
            'bcc' => array()
        ),
        'templates' => array(
            /* user personal email */
            'signup' => 1,
            'forgotPass' => 2,
            'changePass' => 3,
            'userCreate' => 4,
        ),
        'emailsPerTime' => 30,
        'httpPass' => '123456',
    )
);