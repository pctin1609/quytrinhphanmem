<?php
/**
* This is the model class for table "email_timeframes".
*
* The followings are the available columns in table 'email_timeframes':
* @property integer $id
* @property string $desc
* @property integer $value
*/
class EmailTimeframe extends ActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return EmailTimeframe the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'email_timeframes';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('desc, value', 'required'),
			array('value', 'numerical', 'integerOnly' => true),
			array('desc', 'length', 'max' => 100),
			array('id, desc, value', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'desc' => 'Desc',
			'value' => 'Value',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('t.desc', $this->desc, true);
		$criteria->compare('value', $this->value);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}

    public function allowDelete()
    {
        $result = EmailTemplate::model()->count(array('condition' => 'timeframe_id = ' . $this->id));
        if ($result == 0)
            return true;
        else
            return false;
    }
}