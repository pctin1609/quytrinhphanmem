<?php
/**
* This is the model class for table "email_queues".
*
* The followings are the available columns in table 'email_queues':
* @property integer $id
* @property integer $template_id
* @property string $from_name
* @property string $from_email
* @property string $to_name
* @property string $to_email
* @property string $reply_to
* @property string $subject
* @property string $content
* @property integer $created_time
* @property integer $send_time
* @property integer $sent_time
* @property integer $status
*/
class EmailQueue extends ActiveRecord
{
    const STATUS_NOT_SENT = 0;
    const STATUS_SENT_FAIL = 1;
    const STATUS_SENT_SUCCESS = 2;

	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return EmailQueue the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'email_queues';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('template_id, from_name, from_email, to_name, to_email, subject, content, created_time, send_time, status', 'required'),
			array('template_id, created_time, send_time, sent_time, status', 'numerical', 'integerOnly' => true),
			array('from_name', 'length', 'max' => 50),
			array('from_email, subject', 'length', 'max' => 255),
			array('id, template_id, from_name, from_email, to_name, to_email, reply_to, subject, content, created_time, send_time, sent_time, status', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
            'template' => array(self::BELONGS_TO, 'EmailTemplate', 'template_id'),
            'attachments' => array(self::HAS_MANY, 'EmailAttachment', 'queue_id'),
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_id' => 'Template',
			'from_name' => 'From Name',
			'from_email' => 'From Email',
			'to_name' => 'To Name',
			'to_email' => 'To Email',
			'reply_to' => 'Reply To',
			'subject' => 'Subject',
			'content' => 'Content',
			'created_time' => 'Created Time',
			'send_time' => 'Send Time',
			'sent_time' => 'Sent Time',
			'status' => 'Status',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

        $criteria->with = array('template');
		$criteria->compare('id', $this->id);
		$criteria->compare('template.id', $this->template_id, true);
		$criteria->compare('t.from_name', $this->from_name, true);
		$criteria->compare('t.from_email', $this->from_email, true);
		$criteria->compare('t.to_name', $this->to_name, true);
		$criteria->compare('t.to_email', $this->to_email, true);
		$criteria->compare('t.reply_to', $this->reply_to, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('created_time', $this->created_time);
		$criteria->compare('send_time', $this->send_time);
		$criteria->compare('sent_time', $this->sent_time);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
            'sort'=>array(
                'attributes'=>array(
                    'template.id'=>array('asc' => 'template.desc', 'desc' => 'template.desc DESC'),
                    '*',
                ),
            ),
		));
	}

    /**
     * Get template description base on template_id
     * @return array|EmailTemplate|mixed|null
     */
    public function getTemplate()
    {
        $result = EmailTemplate::model()->find(array('condition' => "id = {$this->template_id}"));
        return $result;
    }

    /**
     * @Retrieves string status name from status id
     * Integer $i
     */
    public function getStatusName($i)
    {
        if ($i == self::STATUS_NOT_SENT)
            return 'Not sent';
        else if ($i  == self::STATUS_SENT_FAIL)
            return 'Email sending failed';
        else
            return 'Sent successfully';
    }

    /**
     * Get email queues status description from status id
     */
    public function getQueueStatus()
    {
       return self::getStatusName($this->status);
    }

    /**
     * @Retrieves array of list of status items for Status DropDownBox
     */
    public function getListItem()
    {
        return array(
            self::STATUS_NOT_SENT => self::getStatusName(self::STATUS_NOT_SENT ),
            self::STATUS_SENT_FAIL => self::getStatusName(self::STATUS_SENT_FAIL),
            self::STATUS_SENT_SUCCESS => self::getStatusName(self::STATUS_SENT_SUCCESS)
        );
    }
}