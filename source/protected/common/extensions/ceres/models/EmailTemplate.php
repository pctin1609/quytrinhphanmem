<?php
/**
* This is the model class for table "email_templates".
*
* The followings are the available columns in table 'email_templates':
* @property integer $id
* @property integer $timeframe_id
* @property string $desc
* @property string $from_name
* @property string $from_email
* @property string $to_name
* @property string $to_email
* @property string $reply_to
* @property string $cc
* @property string $bcc
* @property string $subject
* @property string $content
* @property integer $resend_if_fail
* @property integer $delete_if_success
*/
class EmailTemplate extends ActiveRecord
{
    public $timeframeSearch;
            
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return EmailTemplate the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'email_templates';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('timeframe_id, desc, from_name, from_email, to_name, to_email, subject, content', 'required'),
			array('timeframe_id, resend_if_fail, delete_if_success', 'numerical', 'integerOnly' => true),
			array('desc', 'length', 'max' => 100),
			array('from_name', 'length', 'max' => 50),
			array('from_email, subject', 'length', 'max' => 255),
			array('reply_to', 'length', 'max' => 10),
			array('timeframeSearch, id, timeframe_id, desc, from_name, from_email, to_name, to_email, reply_to, cc, bcc, subject, content, resend_if_fail, delete_if_success', 'safe', 'on' => 'search'),
            
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
            'timeframe' => array(self::BELONGS_TO, 'EmailTimeframe', 'timeframe_id'),
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'timeframe_id' => 'Timeframe',
			'desc' => 'Desc',
			'from_name' => 'From Name',
			'from_email' => 'From Email',
			'to_name' => 'To Name',
			'to_email' => 'To Email',
			'reply_to' => 'Reply To',
			'cc' => 'Cc',
			'bcc' => 'Bcc',
			'subject' => 'Subject',
			'content' => 'Content',
			'resend_if_fail' => 'Resend If Fail',
			'delete_if_success' => 'Delete If Success',
            'timeframeSearch' => 'Timeframe'
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

        // customize
        $criteria->with = array('timeframe');
        $criteria->compare('timeframe.desc', $this->timeframeSearch, true);
        
		$criteria->compare('id', $this->id);
		//$criteria->compare('timeframe_id', $this->timeframe_id);
		$criteria->compare('t.desc', $this->desc, true);
		$criteria->compare('from_name', $this->from_name, true);
		$criteria->compare('from_email', $this->from_email, true);
		$criteria->compare('to_name', $this->to_name, true);
		$criteria->compare('to_email', $this->to_email, true);
		$criteria->compare('reply_to', $this->reply_to, true);
		$criteria->compare('cc', $this->cc, true);
		$criteria->compare('bcc', $this->bcc, true);
		$criteria->compare('subject', $this->subject, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('resend_if_fail', $this->resend_if_fail);
		$criteria->compare('delete_if_success', $this->delete_if_success);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
            'sort' => array(
                'attributes' => array(
                    'timeframeSearch' => array(
                        'asc' => 'timeframe.desc',
                        'desc' => 'timeframe.desc DESC',
                    ),
                    '*',
                ),
            ),
		));
	}

    /**
     * @return CActiveRecord contains all records in table 'email_templates'
     */
    public function getEmailTemplate() {
        return $this->findAll();
    }
}