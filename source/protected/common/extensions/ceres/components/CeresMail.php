<?php

class CeresMail
{
    /**
     * Add email to queue. Will auto support below params (into $params['data']) if missing:
     *  FROM_NAME
     *  FROM_EMAIL
     *  TO_NAME
     *  TO_EMAIL
     *
     * @param string $name Name of the mail template
     * @param array $params Data for the template
     *  Format: array(
     *      'to' => array('AAA;BBB', 'a@ceres.com.vn;b@ceres.com.vn'),
     *      'attachments' => array(
     *          'path/to/file-1',
     *          'path/to/file-2',
     *      ),
     *      'data' => array(
     *          'EXAMPLE' => 'Test',
     *      ),
     *  )
     */
    public static function queue($name, $params)
    {
        $configs = self::getConfigs();
        $template = EmailTemplate::model()->findByPk($configs['templates'][$name]);
        self::queueFromTemplate($template, $params);
    }

    /**
     * Create email queues from a template
     *
     * @param EmailTemplate $template an instance of the EmailTemplate class
     * @param Array $params data will be parsed into the template
     */
    public static function queueFromTemplate($template, $params)
    {
        $configs = self::getConfigs();
        $params = self::standardizeParams($params);
        if (!empty($template)) {
            $autoStr = $configs['auto']['id'];

            $queue = new EmailQueue();
            $queue->template_id = $template->id;

            // From
            $fromName = self::applyTemplateData(trim($template->from_name), $params['data']);
            $fromEmail = self::applyTemplateData(trim($template->from_email), $params['data']);
            if (empty($fromEmail) || $fromEmail == $autoStr) {
                $fromName = $configs['auto']['from'][0];
                $fromEmail = $configs['auto']['from'][1];
            } else {
                if (empty($fromName))
                    $fromName = $fromEmail;
            }
            $queue->from_name = $fromName;
            $queue->from_email = $fromEmail;
            if (!isset($params['FROM_NAME'])) {
                $params['data']['FROM_NAME'] = $fromName;
                $params['data']['FROM_EMAIL'] = $fromEmail;
            }

            // To
            $toName = self::applyTemplateData(trim($template->to_name), $params['data']);
            $toEmail = self::applyTemplateData(trim($template->to_email), $params['data']);
            if (empty($toEmail) || $toEmail == $autoStr) {
                $toName = $params['to'][0];
                $toEmail = $params['to'][1];
            }
            $queue->to_name = $toName;
            $queue->to_email = $toEmail;
            if (!isset($params['TO_NAME'])) {
                $params['data']['TO_NAME'] = $toName;
                $params['data']['TO_EMAIL'] = $toEmail;
            }

            // Reply-to
            $replyEmail = self::applyTemplateData(trim($template->reply_to), $params['data']);
            if ($replyEmail == $autoStr) {
                if (isset($configs['auto']['reply-to']))
                    $replyEmail = $configs['auto']['reply-to'];
                else $replyEmail = '';
            }
            $queue->reply_to = $replyEmail;

            // Subject
            $queue->subject = self::applyTemplateData($template->subject, $params['data']);
            $queue->content = self::applyTemplateData($template->content, $params['data']);

            $queue->created_time = time();
            $delayTime = $template->timeframe->value;
            $queue->send_time = $queue->created_time + ((int)$delayTime * 60);
            $queue->status = EmailQueue::STATUS_NOT_SENT;

            if ($queue->save() && isset($params['attachments'])) {
                // Attachments
                foreach ($params['attachments'] as $file) {
                    $att = new EmailAttachment();
                    $att->queue_id = $queue->id;
                    $att->file_path = $file;

                    $att->save();
                }
            }
        }
    }

    /**
     * Send emails in the queue
     *
     * @param EmailQueue $email An email in the queue
     * @return Bool Fail or success
     */
    public static function send($email)
    {
        if (!class_exists('PHPMailer', false)) {
            Yii::import('common.extensions.ceres.vendors.PHPMailer.*');
            require_once('class.phpmailer.php');
        }

        $mailer = new PHPMailer(true);
        // $mailer->SMTPDebug = 2;
        $mailer->IsSMTP();
        $mailer->CharSet = 'UTF-8';

        try {
            $configs = self::getConfigs();
            $mailer->Host = $configs['host'];
            
            if ($configs['secure'] != false)
                $mailer->SMTPSecure = $configs['secure'];
            
            $mailer->Port = $configs['port'];

            if ($configs['auth']) {
                $mailer->SMTPAuth = true;
                $mailer->Username = $configs['auth']['email'];
                $mailer->Password = $configs['auth']['password'];
            }

            // From and Reply-To
            if (empty($email->reply_to)) {
                $mailer->SetFrom($email->from_email, $email->from_name); // PHPMailer will auto add this email for Reply-To
            } else {
                $mailer->SetFrom($email->from_email, $email->from_name, false);
                $mailer->AddReplyTo($email->reply_to);
            }

            $mailer->Subject = $email->subject;
            $mailer->AltBody = strip_tags($email->content);
            $mailer->MsgHTML($email->content);

            // Receivers
            $delimiter = ';';
            $to_names = explode($delimiter, $email->to_name);
            $to_emails = explode($delimiter, $email->to_email);
            foreach ($to_emails as $index => $e) {
                $mailer->AddAddress($e, $to_names[$index]);
            }

            // Cc
            if (!empty($email->template->cc)) {
                $cc = explode($delimiter, $email->template->cc);
                foreach ($cc as $e)
                    $mailer->AddCC($e);
            }

            // Bcc
            if (!empty($email->template->bcc)) {
                $bcc = explode($delimiter, $email->template->bcc);
                foreach ($bcc as $e)
                    $mailer->AddBCC($e);
            }

            // Attachments
            if (!empty($email->attachments)) {
                foreach ($email->attachments as $file)
                    $mailer->AddAttachment($file);
            }

            return $mailer->Send();
        } catch (phpmailerException $e) {
            var_dump($e->errorMessage());
        } catch (Exception $e) {
            var_dump($e->errorMessage());
        }

        return false;
    }

    public static function applyTemplateData($data, $params)
    {
        $data = mb_convert_encoding($data, 'UTF-8', mb_detect_encoding($data, 'UTF-8, ISO-8859-1', true));

        $matches = array('');
        while (count($matches) > 0) {
            if (preg_match("/\[([a-zA-Z0-9_])*\]/", $data, $matches)) {
                $key = $matches[0];
                $property = substr($key, 1, strlen($key) - 2);
                if ($property != '') {
                    $value = $params[$property];
                    $data = str_replace($key, $value, $data);
                }
            }
        }

        return $data;
    }

    public static function standardizeParams($params)
    {
        if (!isset($params['to']))
            $params['to'] = '';
        if (!isset($params['attachments']))
            $params['attachments'] = array();
        if (!isset($params['data']))
            $params['data'] = array();
        return $params;
    }

    public static function getConfigs()
    {
        return param('ceresMail');
    }
}