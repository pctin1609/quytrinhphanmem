<?php

class SendMailAction extends CAction
{
    public function run()
    {
        $configs = param('ceresMail');
        if (!isset($_GET) || (isset($_GET) && @$_GET['pass'] == $configs['httpPass'])) {
            $emails = EmailQueue::model()->findAll(array(
                'condition' => 'status = ' . EmailQueue::STATUS_NOT_SENT . ' or (status = ' . EmailQueue::STATUS_SENT_FAIL .
                    ' and template.resend_if_fail = 1)',
                'order' => 'send_time ASC',
                'limit' => $configs['emailsPerTime'],
                'with' => 'template'
            ));

            echo "Total " . count($emails) . " email(s) found! <br />";

            if (!empty($emails)) {
                foreach ($emails as $email) {
                    $rs = CeresMail::send($email);
                    if ($rs) {
                        if ($email->template->delete_if_success) {
                            $email->delete();
                        } else {
                            $email->sent_time = time();
                            $email->status = EmailQueue::STATUS_SENT_SUCCESS;
                            $email->save();
                        }
                    } else {
                        if ($email->template->resend_if_fail) {
                            $email->sent_time = time();
                            $email->send_time = $email->sent_time + ((int)$email->template->timeframe->value * 60);
                            $email->status = EmailQueue::STATUS_SENT_FAIL;
                            $email->save();
                        }
                    }
                }
            }
        } else throw new CHttpException(404, 'Page not found!');
    }
}