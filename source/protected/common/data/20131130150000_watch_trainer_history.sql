-- --------------------------------------------------------

--
-- Table structure for table `watch_trainer_history`
--

CREATE TABLE IF NOT EXISTS `watch_trainer_history` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `film_id` int(10) NOT NULL,
  `lasted_date` datetime NOT NULL,
  `watched_times` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;