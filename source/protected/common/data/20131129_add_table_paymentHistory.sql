CREATE TABLE `Payment_Historys`
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`id_User` int(11) NOT NULL DEFAULT 0,
	`real_Price` decimal NULL,
	`id_Packet` int(11) NOT NULL DEFAULT 0,
	`date` dateTime NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `Packet_Of_Cretdit`
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`amount` int(11) NOT NULL DEFAULT 0,
	`price` decimal NULL,
	PRIMARY KEY (`id`)
);