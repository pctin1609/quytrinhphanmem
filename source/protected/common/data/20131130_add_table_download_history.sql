/*
SQLyog Enterprise - MySQL GUI
MySQL - 5.1.41 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `download_history` (
	`id` int(11) NOT NULL AUTO_INCREMENT  ,
	`user_id` int(11) NOT NULL,
	`film_id` int(11) NOT NULL,
	`created_date` datetime NOT NULL,
	PRIMARY KEY (`id`)
); 
