-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.5.25a


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema quytrinhphanmem
--

CREATE DATABASE IF NOT EXISTS quytrinhphanmem;
USE quytrinhphanmem;

--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` text,
  `updated_date` int(10) DEFAULT NULL,
  `created_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`,`name`,`description`,`updated_date`,`created_date`,`status`) VALUES 
 (1,'Cartoon','',1388210078,1388210078,1),
 (2,'Hành Động','',1388212852,1388212852,1),
 (3,'Phim Hai','',1388221003,1388221003,1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `download_history`
--

DROP TABLE IF EXISTS `download_history`;
CREATE TABLE `download_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `download_history`
--

/*!40000 ALTER TABLE `download_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `download_history` ENABLE KEYS */;


--
-- Definition of table `email_attachments`
--

DROP TABLE IF EXISTS `email_attachments`;
CREATE TABLE `email_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queue_id` int(11) NOT NULL,
  `file_path` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_attachments`
--

/*!40000 ALTER TABLE `email_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_attachments` ENABLE KEYS */;


--
-- Definition of table `email_queues`
--

DROP TABLE IF EXISTS `email_queues`;
CREATE TABLE `email_queues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `from_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `from_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `to_name` text CHARACTER SET utf8 NOT NULL,
  `to_email` text CHARACTER SET utf8 NOT NULL,
  `reply_to` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `created_time` int(10) NOT NULL,
  `send_time` int(10) NOT NULL,
  `sent_time` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_queues`
--

/*!40000 ALTER TABLE `email_queues` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_queues` ENABLE KEYS */;


--
-- Definition of table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timeframe_id` int(11) NOT NULL,
  `desc` varchar(100) CHARACTER SET utf8 NOT NULL,
  `from_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `from_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `to_name` text CHARACTER SET utf8 NOT NULL,
  `to_email` text CHARACTER SET utf8 NOT NULL,
  `reply_to` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `cc` text CHARACTER SET utf8,
  `bcc` text CHARACTER SET utf8,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `resend_if_fail` tinyint(1) NOT NULL DEFAULT '1',
  `delete_if_success` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_templates`
--

/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` (`id`,`timeframe_id`,`desc`,`from_name`,`from_email`,`to_name`,`to_email`,`reply_to`,`cc`,`bcc`,`subject`,`content`,`resend_if_fail`,`delete_if_success`) VALUES 
 (1,1,'User sign up new account','(auto)','(auto)','(auto)','(auto)','(auto)',NULL,NULL,'LearnOgy - Sign up','<p></p><div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\r\n<tbody>\r\n	<tr style=\"background:#DDDCE2\">\r\n		<td><img src=\"\" alt=\"\" height=\"12\" width=\"1\"></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:14px;\"><h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;\">Sign up</h1>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" id=\"logo\" alt=\"logo\" style=\"width: 120px; float: right; margin-left: 60px; cursor: default;\"></a></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td class=\"current\" style=\"padding:10px;\"><div class=\"current\" style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\">\r\n<p class=\"current\" style=\"padding:14px;color:#222222;line-height:18px;\"> Hello [FULLNAME],<br>\r\n<br>\r\n Welcome to <a class=\"current\" href=\"http://ourauction.com.au/\" target=\"_blank\" style=\"color:#7d003f;\">Quy Trinh Phan Mem</a>.<br>\r\n<br>\r\n Thank you for registering! Please follow on the information below to log in:<br>- Email: [EMAIL]<br>- Pasword: [PASSWORD]<br><br>\r\n Sincerely,<br>\r\n Quy Trinh Phan Mem team </p>\r\n</div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;\" cellpadding=\"0\" cellspacing=\"5\" width=\"100%\">\r\n<tbody>\r\n	<tr>\r\n		<td><div style=\"margin-right:auto;margin-left:auto;width:600px;\">\r\n<p class=\"current\" style=\"width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;\"><br>\r\n If you are have any questions, please feel free to contact us at <a class=\"current\" style=\"color:#fff\" href=\"mailto:contact@ourauction.com.au\">contact@quytrinhphanmem.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" alt=\"logo\" style=\"width: 100px; float: right; margin-left: 60px; cursor: default;\"></a></div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n',1,0),
 (2,1,'Forgot Password','(auto)','(auto)','(auto)','(auto)','(auto)',NULL,NULL,'Quy trinh phan mem - Forgot password','<div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\r\n<tbody>\r\n	<tr style=\"background:#DDDCE2\">\r\n		<td><img src=\"\" alt=\"\" height=\"12\" width=\"1\"></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:14px;\"><h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;\">Forgot Password<br></h1>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" id=\"logo\" alt=\"logo\" style=\"width: 120px; float: right; margin-left: 60px; cursor: default;\"></a></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td class=\"\" style=\"padding:10px;\"><div class=\"current\" style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\"> Hello,<br>\r\n<br>\r\n Great. You have just successfully changed your password to [PASSWORD]. Hide it somewhere safe and secure.\r\n<br>\r\n<br>\r\n Sincerely,<br>&nbsp;Quy Trinh Phan Mem team </div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;\" cellpadding=\"0\" cellspacing=\"5\" width=\"100%\">\r\n<tbody>\r\n	<tr>\r\n		<td><div style=\"margin-right:auto;margin-left:auto;width:600px;\">\r\n<p class=\"current\" style=\"width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;\"><br>\r\n If you are have any questions, please feel free to contact us at <a class=\"current\" style=\"color:#fff\" href=\"mailto:contact@ourauction.com.au\">contact@quytrinhphanmem.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" alt=\"logo\" style=\"width: 100px; float: right; margin-left: 60px; cursor: default;\"></a></div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n</div>',1,0),
 (3,1,'Change Password','(auto)','(auto)','(auto)','(auto)','(auto)',NULL,NULL,'Quy trinh phan mem - Changed password','<div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\r\n<tbody>\r\n	<tr style=\"background:#DDDCE2\">\r\n		<td><img src=\"\" alt=\"\" height=\"12\" width=\"1\"></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:14px;\"><h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;\">PasswordChange<br>\r\n</h1>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" id=\"logo\" alt=\"logo\" style=\"width: 120px; float: right; margin-left: 60px; cursor: default;\"></a></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td class=\"\" style=\"padding:10px;\"><div class=\"current\" style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\"> Hello,<br>\r\n<br>\r\n Great. You have just successfully changed your password to [PASSWORD]. Hide it somewhere safe and secure. <br>\r\n<br>\r\n Sincerely,<br>\r\n&nbsp;Quy Trinh Phan Mem team </div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;\" cellpadding=\"0\" cellspacing=\"5\" width=\"100%\">\r\n<tbody>\r\n	<tr>\r\n		<td><div style=\"margin-right:auto;margin-left:auto;width:600px;\">\r\n<p class=\"current\" style=\"width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;\"><br>\r\n If you are have any questions, please feel free to contact us at <a class=\"current\" style=\"color:#fff\" href=\"mailto:contact@ourauction.com.au\">contact@quytrinhphanmem.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" alt=\"logo\" style=\"width: 100px; float: right; margin-left: 60px; cursor: default;\"></a></div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n</div>',1,0),
 (4,1,'Administrator create new account','(auto)','(auto)','(auto)','(auto)','(auto)',NULL,NULL,'Quy trinh phan mem - Your account has been created.','<div style=\"width:100%;height:100%;margin:0;padding:0;background-color:#e6e6e6;padding-bottom:15px;\">\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:18px;color:#333;border:none;margin-right:auto;margin-left:auto;border-collapse:collapse;background-color: #fff;width:600px;\" cellpadding=\"0\" cellspacing=\"5\">\r\n<tbody>\r\n	<tr style=\"background:#DDDCE2\">\r\n		<td><img src=\"\" alt=\"\" height=\"12\" width=\"1\"></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td style=\"padding:14px;\"><h1 class=\"current\" style=\"width:280px;float:left;color:#252B34;font-family:Arial, Helvetica, sans-serif;font-size:34px;line-height:38px;font-weight:normal;margin-top:20px;padding-left:8px;\">New Account<br>\r\n</h1>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" id=\"logo\" alt=\"logo\" style=\"width: 120px; float: right; margin-left: 60px; cursor: default;\"></a></td>\r\n	</tr>\r\n\r\n	<tr>\r\n		<td class=\"\" style=\"padding:10px;\"><div class=\"current\" style=\"background-color: #e6deea;width:100%;height:100%;margin:0;padding:0;\"> Hello [FULLNAME],<br><br>Welcome to Quy Trinh Phan Mem.<br><br>Please login at Quy Trinh Phan Mem with your account:<br>Your email: [EMAIL]<br>Password: [PASSWORD]<br><br>Sincerely,<br>Quy Trinh Phan Mem team<br></div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n\r\n<table style=\"font-family:Arial, Helvetica, sans-serif;font-size:11px;color:#8b8b8b;border:none;border-collapse:collapse;background-color:#ccc;margin-top:20px;\" cellpadding=\"0\" cellspacing=\"5\" width=\"100%\">\r\n<tbody>\r\n	<tr>\r\n		<td><div style=\"margin-right:auto;margin-left:auto;width:600px;\">\r\n<p class=\"current\" style=\"width:440px;float:left;font-family:Arial, Helvetica, sans-serif;font-size:11px;line-height:14px;font-weight:normal;\"><br>\r\n If you are have any questions, please feel free to contact us at <a class=\"current\" style=\"color:#fff\" href=\"mailto:contact@ourauction.com.au\">contact@quytrinhphanmem.com</a> This email is a post only email. Replies to this email are not monitored or answered. </p>\r\n<a href=\"http://ourauction.com.au\"><img src=\"\" alt=\"logo\" style=\"width: 100px; float: right; margin-left: 60px; cursor: default;\"></a></div>\r\n</td>\r\n	</tr>\r\n</tbody>\r\n</table>\r\n</div>',1,0);
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;


--
-- Definition of table `email_timeframes`
--

DROP TABLE IF EXISTS `email_timeframes`;
CREATE TABLE `email_timeframes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(100) CHARACTER SET utf8 NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email_timeframes`
--

/*!40000 ALTER TABLE `email_timeframes` DISABLE KEYS */;
INSERT INTO `email_timeframes` (`id`,`desc`,`value`) VALUES 
 (1,'Immediately',1),
 (2,'Within 5 minutes',5),
 (3,'Within 10 minutes',10),
 (4,'Within 15 minutes',15),
 (5,'Within 30 minutes',30),
 (6,'Within 1 hour',60),
 (7,'Within 2 hours',120),
 (8,'Within 5 hours',300);
/*!40000 ALTER TABLE `email_timeframes` ENABLE KEYS */;


--
-- Definition of table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `source` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  `extension` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` (`id`,`name`,`source`,`size`,`extension`) VALUES 
 (1,'Wildlife.wmv','films/d2f3dfba105dcc50cf6e79a31a22b60180adbe8a.wmv',26246026,'wmv'),
 (2,'chu bao hong_clip1.mp4','films/c8069f83875bccabb9372f88be514e5851bf8824.mp4',487159,'mp4'),
 (3,'chu bao hong_clip2.mp4','films/04a3ae7d8e156a5af673f9d7f0b8813145ecb09c.mp4',882809,'mp4'),
 (4,'chu bao hong_clip3.mp4','films/9b912347892f2dd12ccdf41f69d18b5734b12d06.mp4',742381,'mp4'),
 (5,'chu bao hong_clip4.mp4','films/dfebc75181ee1b6e9d82f70c43b811b0b8296e8f.mp4',810132,'mp4'),
 (6,'chu bao hong_clip5.mp4','films/2521fb8ad6529e03ff70cb1209ac0151a1ee2431.mp4',784639,'mp4'),
 (7,'chu bao hong_clip6.mp4','films/546058646921aea3b292ab3e4abcd0441c1334ea.mp4',783526,'mp4'),
 (8,'chu bao hong_clip7.mp4','films/8d4df7a30f47fe9fe8ce779a5a999f075d446ffb.mp4',1058643,'mp4'),
 (9,'chu bao hong_clip8.mp4','films/c9ac18479627bb6f5cd8c1433d3eef435ff09d03.mp4',1009570,'mp4'),
 (10,'chu bao hong_clip8.mp4','films/a839c3539fdfb2218ef5e7bfa2caec57da1f5fe2.mp4',1009570,'mp4'),
 (11,'chu bao hong_clip11.mp4','films/b5d6554154779f068e6425af39ef3a6942742796.mp4',759446,'mp4'),
 (12,'chu bao hong_clip12.mp4','films/ff51d55f19cafe835f45be09e12b166ac63bf8bb.mp4',834761,'mp4'),
 (13,'chu bao hong_clip21.mp4','films/386796f702a028778779a22c2c9a83be664b140f.mp4',501683,'mp4'),
 (14,'chu bao hong_clip105.mp4','films/f038c81c69b66d008c7f9130d44874396f4a949c.mp4',801893,'mp4'),
 (15,'chu bao hong_clip44.mp4','films/186a8ac7bb90e2d54e37e89c8f446f7a7e537725.mp4',580440,'mp4'),
 (16,'chu bao hong_clip35.mp4','films/910fc8280e09d5e4347ec07df607553e491ac012.mp4',572554,'mp4'),
 (17,'chu bao hong_clip52.mp4','films/91616348a63d09637fe09fb503ea01c367330180.mp4',519590,'mp4'),
 (18,'chu bao hong_clip49.mp4','films/09f3fa2c4a994478446e8ca8b589ad49af690bd4.mp4',542813,'mp4'),
 (19,'chu bao hong_clip61.mp4','films/a5e8782fcfae3d1b28bd3583eb92fa2033f76afe.mp4',683438,'mp4'),
 (20,'chu bao hong_clip89.mp4','films/b0f52ff2aafc8faf6cb45050c4eedf0176091fdc.mp4',617043,'mp4'),
 (21,'chu bao hong_clip40.mp4','films/123b076cba6642b9ee27bc53814a979c94cb60ff.mp4',653187,'mp4'),
 (22,'chu bao hong_clip48.mp4','films/b6c5d24ea9bc4f4288e0b1f444019facec3ddc4e.mp4',556266,'mp4'),
 (23,'chu bao hong_clip67.mp4','films/7d8d466fff2b4a53eb676a214053b9601ffeaf56.mp4',731461,'mp4'),
 (24,'chu bao hong_clip49.mp4','films/ddfc28c32affef2fed52bef59da51b6914cd5ca4.mp4',542813,'mp4'),
 (25,'chu bao hong_clip57.mp4','films/8d8bd5f552f172e635341a2df5d0bd03f6262839.mp4',591420,'mp4'),
 (26,'chu bao hong_clip42.mp4','films/77b83b4767971c53c334315052fc5bb9894ecad5.mp4',501404,'mp4'),
 (27,'chu bao hong_clip42.mp4','films/09c8de1c5f8e2405bf33cb0783aa09cc77a5c725.mp4',501404,'mp4'),
 (28,'chu bao hong_clip43.mp4','films/845aec350c32778456864fe5c0d7135615d9ec13.mp4',557899,'mp4'),
 (29,'chu bao hong_clip45.mp4','films/86ab3ed96e4eeb60c80829ec1e3563fa361e7f5a.mp4',510963,'mp4'),
 (30,'chu bao hong_clip45.mp4','films/dfd7544931ecbcf707c9952249495f966760cdb4.mp4',510963,'mp4'),
 (31,'chu bao hong_clip47.mp4','films/258c80d0f0060b46d0b8e68b23d485b1629ef51a.mp4',556404,'mp4'),
 (32,'chu bao hong_clip61.mp4','films/c8f6ae773f50443c0aa29e0769d2dad39b82647d.mp4',683438,'mp4'),
 (33,'chu bao hong_clip62.mp4','films/e0a475d0e730174220fa182f1a659f32bc0cd459.mp4',722983,'mp4'),
 (34,'chu bao hong_clip63.mp4','films/61e7735667ac06243c09d3550f24edb1fdabd0c9.mp4',886492,'mp4'),
 (35,'chu bao hong_clip63.mp4','films/edb3cc367f8d9063fd47755b1519d9931128e3eb.mp4',886492,'mp4'),
 (36,'chu bao hong_clip64.mp4','films/fa267d62999b74ccf8157184f7415341ff6b4feb.mp4',817430,'mp4'),
 (37,'chu bao hong_clip65.mp4','films/1f9eeb08499de7059eb7a190312f0178b750c404.mp4',753529,'mp4'),
 (38,'chu bao hong_clip66.mp4','films/72e77693575af2ebe21712c8442107eeeef56f8d.mp4',745949,'mp4'),
 (39,'chu bao hong_clip67.mp4','films/90891e10f25937a13f9e45f9b726a4b3560eb721.mp4',731461,'mp4'),
 (40,'chu bao hong_clip68.mp4','films/77382ec148b593dae7b4c9ff36ef324566d9ee08.mp4',676087,'mp4'),
 (41,'chu bao hong_clip69.mp4','films/0b683235608fae0e70f2b5c491a6184e271739d8.mp4',621093,'mp4'),
 (42,'chu bao hong_clip70.mp4','films/433058826fbeaafbef0bff12f7213151f399862a.mp4',825843,'mp4'),
 (43,'chu bao hong_clip71.mp4','films/cdab269bcc4de6cf6b3fd05311a0158f83709175.mp4',645748,'mp4'),
 (44,'chu bao hong_clip71.mp4','films/75b2475b072df4f2f7c7541d01b1ac773e2877dc.mp4',645748,'mp4'),
 (45,'chu bao hong_clip72.mp4','films/2b4508f06d8644ed2b1d33c3ae16a2d6e69fa643.mp4',569317,'mp4'),
 (46,'chu bao hong_clip73.mp4','films/f4ab3206e89c84f38afcf0403413bbbb681949a2.mp4',659932,'mp4'),
 (47,'chu bao hong_clip74.mp4','films/8199c4c9b60264bb8ab8d0a4ee46eac1d88dbde9.mp4',640376,'mp4'),
 (48,'chu bao hong_clip75.mp4','films/9a7acd40db38913a9a91930d6fd2209b40f41cc6.mp4',1034331,'mp4'),
 (49,'chu bao hong_clip76.mp4','films/4bfda2434ebcf8852a82f92b908da7d8ff56de08.mp4',837437,'mp4'),
 (50,'chu bao hong_clip77.mp4','films/80229e0bcb85ca47a1711f655f2e409bbebbcd32.mp4',550965,'mp4'),
 (51,'chu bao hong_clip108.mp4','films/23e9289dc6fb36d053ee595be2b34747f44452d8.mp4',673330,'mp4'),
 (52,'chu bao hong_clip107.mp4','films/d3c9499b9344910481b25fffbf318450a980e5de.mp4',725989,'mp4'),
 (53,'chu bao hong_clip1.mp4','films/01ca95728439e282c678a1a7689555857895cd54.mp4',487159,'mp4'),
 (54,'chu bao hong_clip2.mp4','films/1fb40b84667747a51ccb78468c9477344e4d1840.mp4',882809,'mp4'),
 (55,'chu bao hong_clip3.mp4','films/702a59dea28a0fdade255477c8c73f6f60826c8e.mp4',742381,'mp4'),
 (56,'chu bao hong_clip4.mp4','films/7ea67ef2c1c449a482193bece2ad2add261b06ef.mp4',810132,'mp4'),
 (57,'chu bao hong_clip6.mp4','films/2201775dd5531419c46cbecd6ad94e9445f90f47.mp4',783526,'mp4'),
 (58,'chu bao hong_clip6.mp4','films/f3b21bd59d3c5b9c7bc8db3295a92b38419d5ecb.mp4',783526,'mp4'),
 (59,'chu bao hong_clip7.mp4','films/c10209b75ab7c3a81d5926e19928da1130a0d3f4.mp4',1058643,'mp4'),
 (60,'chu bao hong_clip8.mp4','films/064521a70ed05efa9b712072c70f5f2c961f9fb8.mp4',1009570,'mp4'),
 (61,'chu bao hong_clip8.mp4','films/178503a7e176d3f5f86e8e097b121f48983e266f.mp4',1009570,'mp4'),
 (62,'chu bao hong_clip23.mp4','films/3d39f7d18ec51d6677b75d30ac4f044d76dfd2ce.mp4',607459,'mp4'),
 (63,'chu bao hong_clip1.flv','films/f71537d2783ee1cfc50d50d3d8e7ee2c0f596cda.flv',546904,'flv'),
 (64,'Chu Bao Hong Clip2-1.m4v','films/5bd20cb790fc264b786c88ec54dae2206d536092.m4v',1113917,'m4v'),
 (65,'Chu Bao Hong Clip2-1.m4v','films/3fac478b2d4cead385254f4d8351b64428fa78e3.m4v',1113917,'m4v'),
 (66,'Chu Bao Hong Clip3-1.m4v','films/b1ed914c117f6a5bfc36e1ce3142177cc1b830f2.m4v',895615,'m4v'),
 (67,'Chu Bao Hong Clip4-1.m4v','films/b420db0e6be6f74c4d909cbc1e092cb662af43c9.m4v',895615,'m4v'),
 (68,'Chu Bao Hong Clip5.m4v','films/8c8556679a220035480a38e398d96aca17b067fa.m4v',953234,'m4v'),
 (69,'Chu Bao Hong Clip6-1.m4v','films/e0844759a0905edf7b692926581759744150c4ee.m4v',910547,'m4v'),
 (70,'Chu Bao Hong Clip7-1.m4v','films/1f7dcc679f067fe34fa887afeb6fceb01185f7f6.m4v',1266535,'m4v'),
 (71,'Chu Bao Hong Clip8-1.m4v','films/1f9ede30186eca2492f0063b0dfbade69855eb62.m4v',1118369,'m4v'),
 (72,'Chu Bao Hong Clip9-1.m4v','films/47c4b2569a8e721f0061962141decbcf0063a76f.m4v',842003,'m4v'),
 (73,'Chu Bao Hong Clip10-1.m4v','films/01e17a42422eda5b8c7c9a48152d1336b1fd8d38.m4v',907124,'m4v'),
 (74,'Chu Bao Hong Clip10-1.m4v','films/00eb250eb6786a08500fe45ec006df5da4842210.m4v',907124,'m4v'),
 (75,'Chu Bao Hong Clip11-1.m4v','films/9547964765ae454d75558d92e85e90cc13368b69.m4v',887169,'m4v'),
 (76,'Chu Bao Hong Clip12-1.m4v','films/3654f6ad09ec4885be8f17a8660f38e364a2ad67.m4v',962159,'m4v'),
 (77,'Chu Bao Hong Clip13-1.m4v','films/24c1d0dc251df21a62813ff8321e68ed802a7c49.m4v',1254478,'m4v'),
 (78,'Chu Bao Hong Clip14-1.m4v','films/d4ce709017f990bb82933b1196491bd614af925c.m4v',915436,'m4v'),
 (79,'Chu Bao Hong Clip15-1.m4v','films/4e1d454c203e47a6e48810d2c824266d94db1087.m4v',901049,'m4v'),
 (80,'Chu Bao Hong Clip16-1.m4v','films/5030f2a412e0e84873b66155d55e6a74562a4a4c.m4v',973309,'m4v'),
 (81,'Chu Bao Hong Clip17-1.m4v','films/14ef87c3bb9097df13648ee456f16c2d124bec2b.m4v',749945,'m4v'),
 (82,'Chu Bao Hong Clip18-1.m4v','films/ebf9be0305d9def3884b7921e38115df95e6ba31.m4v',1021262,'m4v'),
 (83,'Chu Bao Hong Clip19-1.m4v','films/0d467bab9c3442560404d61fdf1014cc1b694378.m4v',815514,'m4v'),
 (84,'Chu Bao Hong Clip20-1.m4v','films/4a41a50b4cada693412100941fc124726812e3bb.m4v',570853,'m4v'),
 (85,'Chu Bao Hong Clip21-1.m4v','films/8020dfefb3e654245bf61060e5b31a60ee88612f.m4v',655125,'m4v'),
 (86,'Chu Bao Hong Clip22-1.m4v','films/e3a9abf694b40b20d98079ecc45ebed2d3672f3f.m4v',805139,'m4v'),
 (87,'Chu Bao Hong Clip23-1.m4v','films/2b568acd38976f8480cf3055554570cb26846f3d.m4v',790627,'m4v'),
 (88,'Chu Bao Hong Clip24-1.m4v','films/6216f95aecae174e0b27f52dec4efd0d624c715b.m4v',934982,'m4v'),
 (89,'Chu Bao Hong Clip24-1.m4v','films/7207322a2fb2b1665bafef02441c65d2699c7d4b.m4v',934982,'m4v'),
 (90,'Chu Bao Hong Clip25-1.m4v','films/9e484a731d03dca60a1f5dbfb0f88dc867803176.m4v',1130747,'m4v'),
 (91,'Chu Bao Hong Clip26-1.m4v','films/efcb29066e571f3688ad77af0dbf59ec24f6d2a4.m4v',917635,'m4v'),
 (92,'Chu Bao Hong Clip27-1.m4v','films/a2a9ed25c3c39b249bc550a784e36e716d7973eb.m4v',839308,'m4v'),
 (93,'Chu Bao Hong Clip28-1.m4v','films/d46081a22d9f9f7e64d70f7f0a353f6be5b0a6d3.m4v',871043,'m4v'),
 (94,'Chu Bao Hong Clip30-1.m4v','films/f9da4821dd07347e50b8f38520d2bc3f8bb89a22.m4v',578763,'m4v'),
 (95,'Chu Bao Hong Clip32-1.m4v','films/c0fd70bc26fbe349706c3c70c77fda879e7b5d7d.m4v',780315,'m4v'),
 (96,'Chu Bao Hong Clip33-1.m4v','films/1d5ee460d04afc8c23595c58bdac2ad7fb9c8971.m4v',706565,'m4v'),
 (97,'Chu Bao Hong Clip34-1.m4v','films/30d2c04c33f25723bc5332d830cc4d5dd1c18f38.m4v',795961,'m4v'),
 (98,'Chu Bao Hong Clip34-1.m4v','films/24029226845c5cf8a4e8be5590518e28dfbb137c.m4v',795961,'m4v'),
 (99,'Chu Bao Hong Clip35-1.m4v','films/0278f239e90c5bd96b5be17bd708af4be6d67b24.m4v',718141,'m4v'),
 (100,'Chu Bao Hong Clip36-1.m4v','films/7cc49a42b06f58e40f5d8d444ae6da01b2a44975.m4v',871222,'m4v'),
 (101,'Chu Bao Hong Clip37-1.m4v','films/548d67279029ef053c0cfbc0264731332d4ffa8a.m4v',898202,'m4v');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


--
-- Definition of table `films`
--

DROP TABLE IF EXISTS `films`;
CREATE TABLE `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `credits` int(11) NOT NULL DEFAULT '0',
  `director` varchar(64) NOT NULL,
  `author` varchar(32) NOT NULL,
  `date_publish` datetime NOT NULL,
  `file_id` int(11) NOT NULL,
  `updated_date` int(10) DEFAULT NULL,
  `created_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `films`
--

/*!40000 ALTER TABLE `films` DISABLE KEYS */;
INSERT INTO `films` (`id`,`category_id`,`title`,`description`,`credits`,`director`,`author`,`date_publish`,`file_id`,`updated_date`,`created_date`,`status`) VALUES 
 (2,1,'Bao Hong 2','',1,'Dirctor 1','Author 2','2013-12-03 00:00:00',2,1388222767,1388210599,1),
 (3,1,'Bao Hong 3','',3,'Dirctor 3','Author 3','2013-12-11 00:00:00',3,1388222787,1388210619,1),
 (4,1,'Bao Hong 4','',4,'Dirctor 2','Author 4','2013-12-04 00:00:00',4,1388222796,1388210646,1),
 (5,1,'Bao Hong 5','',4,'Dirctor 5','Author 5','2013-12-04 00:00:00',5,1388222809,1388210668,1),
 (6,1,'Bao Hong 6','',3,'Dirctor 6','Author6','2013-11-27 00:00:00',6,1388222817,1388210688,1),
 (7,1,'Bao Hong 37','<p>dsdfsdf</p>',3,'Dirctor 17','Author 7','2013-11-25 00:00:00',7,1388222824,1388210710,1),
 (8,1,'Bao Hong 6','<p>sd</p>',34,'Dirctor 8','Author 8','2013-12-04 00:00:00',8,1388222831,1388210734,1),
 (9,1,'Bao Hong 9','',9,'Dirctor 9','Author 9','2013-12-03 00:00:00',9,1388222837,1388210755,1),
 (10,1,'Bao Hong 10','',9,'Dirctor 10','Author 10','2013-12-03 00:00:00',10,1388222845,1388210779,1),
 (11,1,'Bao Hong 11','',5,'Dirctor 11','Author 11','2013-12-11 00:00:00',11,1388222854,1388210801,1),
 (12,1,'Bao Hong 12','',2,'Dirctor 12','Author 12','2013-12-03 00:00:00',12,1388222921,1388210913,1),
 (13,1,'Bao Hong 13','',2,'Dirctor 13','Author 13','2013-12-03 00:00:00',13,1388222927,1388210962,1),
 (14,1,'Bao Hong 14','',2,'Dirctor 14','Author 14','2013-12-05 00:00:00',14,1388222934,1388211017,1),
 (15,1,'Bao Hong 15','',2,'Dirctor 15','Author 15','2013-12-14 00:00:00',15,1388211065,1388211065,1),
 (16,1,'Bao Hong 16','',4,'Dirctor 16','Author 16','2013-12-13 00:00:00',16,1388222944,1388211111,1),
 (17,1,'Bao Hong 17','',5,'Dirctor 7','Author 17','2013-12-20 00:00:00',17,1388222950,1388211164,1),
 (18,1,'Bao Hong 18','',3,'Dirctor 17','Author 17','2013-12-05 00:00:00',18,1388222958,1388211208,1),
 (19,1,'Bao Hong 18','',6,'Dirctor 18','Author 18','2013-12-07 00:00:00',19,1388222964,1388211244,1),
 (20,1,'Bao Hong 19','',2,'Dirctor 19','Author 19','2013-12-13 00:00:00',20,1388222970,1388211330,1),
 (21,1,'Bao Hong 21','',34,'Dirctor 21','Author 21','2013-12-07 00:00:00',21,1388222980,1388211608,1),
 (22,1,'Bao Hong 22','',4,'Dirctor 22','Davil vila','2013-12-03 00:00:00',22,1388223030,1388211675,1),
 (23,1,'Bao Hong 23','',3,'Wiliam','Davil vila','2013-12-13 00:00:00',23,1388223037,1388211727,1),
 (24,1,'Bao Hong 24','',5,'Wiliam','Davil vila','2013-12-27 00:00:00',24,1388223044,1388211800,1),
 (25,1,'Bao Hong 25','',5,'Wiliam','Davil vila','2013-12-02 00:00:00',25,1388223050,1388211858,1),
 (26,1,'Bao Hong 26','',4,'Gragreed','Taylyoin','2013-12-03 00:00:00',26,1388223057,1388211947,1),
 (27,1,'Bao Hong 27','',3,'Dirctor 27','Taylyoin','2013-12-05 00:00:00',27,1388212026,1388212026,1),
 (28,1,'Bao Hong 28','',4,'Wiliam','Davil vila','2013-12-18 00:00:00',28,1388223069,1388212086,1),
 (29,1,'Bao Hong 29','',5,'Wiliam','Davil vila','2013-12-12 00:00:00',29,1388223075,1388212147,1),
 (30,1,'Bao Hong 30','',4,'Wiliam','Davil vila','2013-12-18 00:00:00',30,1388223082,1388212192,1),
 (31,1,'Bao Hong 31','',4,'Wiliam','Davil vila','2013-12-11 00:00:00',31,1388223087,1388212277,1),
 (32,1,'Bao Hong 32','',12,'Wiliam','Davil vila','2013-12-19 00:00:00',32,1388223131,1388212340,1),
 (33,1,'Bao Hong 35','',3,'Wiliam','Davil vila','2013-12-10 00:00:00',33,1388223138,1388212379,1),
 (34,1,'Bao Hong 34','',3,'Wiliam','Davil vila','2013-12-03 00:00:00',34,1388223144,1388212427,1),
 (35,1,'Bao Hong 36','',4,'Wiliam','Davil vila','2013-12-18 00:00:00',35,1388223151,1388212469,1),
 (36,1,'Bao Hong 36','',34,'Wiliam','Davil vila','2013-12-18 00:00:00',36,1388223157,1388212575,1),
 (37,1,'Bao Hong 37','',3,'Wiliam','Davil vila','2013-12-24 00:00:00',37,1388223163,1388212643,1),
 (38,1,'Bao Hong 38','',3,'Wiliam','Davil vila','2013-12-24 00:00:00',38,1388223170,1388212696,1),
 (39,1,'Bao Hong 39','',3,'Wiliam','Davil vila','2013-12-26 00:00:00',39,1388223176,1388212756,1),
 (40,1,'Bao Hong 40','',3,'Wiliam','Davil vila','2013-12-05 00:00:00',40,1388223182,1388212826,1),
 (41,2,'The king of fighter','',12,'Gragreed','Taylyoin','2013-12-02 00:00:00',41,1388223188,1388212979,1),
 (42,2,'The king of fighter','',12,'Gragreed','Davil vila','2013-12-03 00:00:00',42,1388223261,1388213025,1),
 (43,2,'The king of fighter','',12,'Dirctor 17','Author 17','2013-12-12 00:00:00',43,1388223267,1388213061,1),
 (44,2,'The king of fighter','',3,'Dirctor 17','Davil vila','2013-12-04 00:00:00',44,1388223273,1388213110,1),
 (45,2,'The king of fighter','',12,'Wiliam','Davil vila','2013-11-27 00:00:00',45,1388223279,1388213149,1),
 (46,2,'The king of fighter 40','',4,'Dirctor 17','Davil vila','2013-12-04 00:00:00',46,1388223285,1388213195,1),
 (47,2,'The king of fighter 47','',12,'Wiliam','Davil vila','2013-11-25 00:00:00',47,1388223291,1388213339,1),
 (48,2,'The king of fighter 48','',12,'Dirctor 17','Davil vila','2013-11-30 00:00:00',48,1388223296,1388213378,1),
 (49,2,'The king of fighter 49','',12,'Dirctor 10','Taylyoin','2013-12-04 00:00:00',49,1388223302,1388213458,1),
 (50,2,'The king of fighter 50','',12,'Wiliam','Davil vila','2013-12-02 00:00:00',50,1388223308,1388213496,1),
 (51,2,'The king of fighter 51','',23,'Wiliam','Davil vila','2013-11-25 00:00:00',51,1388223316,1388213549,1),
 (52,2,'The king of fighter 52','',23,'Dirctor 17','Author 17','2013-12-03 00:00:00',52,1388223376,1388213609,1),
 (53,1,'The king of fighter 53','',23,'Wiliam','Davil vila','2013-12-03 00:00:00',53,1388223382,1388213641,1),
 (54,2,'The king of fighter 54','',23,'Dirctor 17','Author 17','2013-12-18 00:00:00',54,1388223388,1388213668,1),
 (55,2,'The king of fighter 55','',23,'Dirctor 10','Davil vila','2013-12-03 00:00:00',55,1388223394,1388213693,1),
 (56,2,'The king of fighter 56','',23,'Dirctor 17','Taylyoin','2013-11-27 00:00:00',56,1388223400,1388213720,1),
 (57,2,'The king of fighter 56','',23,'Wiliam','Davil vila','2013-11-26 00:00:00',57,1388223432,1388213748,1),
 (58,2,'The king of fighter 57','',23,'Wiliam','Davil vila','2013-11-26 00:00:00',58,1388223426,1388213774,1),
 (59,2,'The king of fighter 57','',34,'Dirctor 17','Author','2013-12-03 00:00:00',59,1388223418,1388213800,1),
 (60,2,'The king of fighter 58','',34,'Dirctor 12','Author 10','2013-12-04 00:00:00',60,1388223412,1388213825,1),
 (61,2,'The king of fighter 59','',34,'Wiliam','Taylyoin','2013-12-03 00:00:00',61,1388223406,1388213849,1),
 (62,2,'The king of fighter 60','',34,'Dirctor 17','Author','2013-11-26 00:00:00',62,1388223449,1388214021,1),
 (63,2,'The king of fighter 100','',23,'Dirctor 17','Davil vila','2013-11-26 00:00:00',63,1388215337,1388215337,1),
 (64,2,'The king of fighter 101','',3,'Dirctor 1','Davil vila','2013-11-26 00:00:00',64,1388223438,1388217893,1),
 (65,3,'O nhà 1 mình','',23,'Dirctor 17','wiliam','2013-11-25 00:00:00',65,1388221139,1388221139,1),
 (66,3,'Toi o nhà 1 mình 2','',23,'Wiliam','gradgreed','2013-11-27 00:00:00',66,1388221211,1388221211,1),
 (67,3,'ai cũng phải cười','',23,'Wiliam','gradgreed','2013-11-25 00:00:00',67,1388221277,1388221277,1),
 (68,3,'ai cũng phải cười','',34,'Wiliam','Taylyoin','2013-12-02 00:00:00',68,1388221333,1388221333,1),
 (69,3,'ai cũng phải cười 4','',24,'Wiliam','gradgreed','2013-11-26 00:00:00',69,1388221358,1388221358,1),
 (70,3,'ai cũng phải cười 5','',35,'Wiliam','gradgreed','2013-11-26 00:00:00',70,1388221383,1388221383,1),
 (71,3,'ai cũng phải cười 6','',34,'Wiliam','gradgreed','2013-11-25 00:00:00',71,1388221406,1388221406,1),
 (72,3,'ai cũng phải cười 8','',3,'Dirctor 17','Author 17','2013-11-25 00:00:00',72,1388221440,1388221440,1),
 (73,3,'ai cũng phải cười 7','',23,'Dirctor 17','Author','2013-12-06 00:00:00',73,1388221468,1388221468,1),
 (74,3,'ai cũng phải cười 10','',4,'Dirctor 17','Author 17','2013-11-26 00:00:00',74,1388221499,1388221499,1),
 (75,3,'ai cũng phải cười 11','',3,'Wiliam','gradgreed','2013-11-30 00:00:00',75,1388221523,1388221523,1),
 (76,3,'ai cũng phải cười 12','',34,'Wiliam','gradgreed','2013-12-26 00:00:00',76,1388221552,1388221552,1),
 (77,3,'ai cũng phải cười 13','',4,'Wiliam','gradgreed','2013-12-05 00:00:00',77,1388221577,1388221577,1),
 (78,3,'ai cũng phải cười 14','',4,'Dirctor','Davil vila','2013-11-25 00:00:00',78,1388221682,1388221682,1),
 (79,3,'ai cũng phải cười 15','',4,'Dirctor 11','Author','2013-11-26 00:00:00',79,1388221705,1388221705,1),
 (80,3,'ai cũng phải cười 16','',4,'Wiliam','Davil vila','2013-12-11 00:00:00',80,1388221729,1388221729,1),
 (81,3,'ai cũng phải cười 17','',4,'Dirctor 10','Davil vila','2013-11-25 00:00:00',81,1388221758,1388221758,1),
 (82,3,'ai cũng phải cười 17','',5,'Dirctor 12','Author 17','2013-11-26 00:00:00',82,1388221791,1388221791,1),
 (83,3,'ai cũng phải cười 18','',4,'Wiliam','gradgreed','2013-11-25 00:00:00',83,1388221811,1388221811,1),
 (84,3,'ai cũng phải cười 18','',4,'Dirctor 17','Author 17','2013-11-25 00:00:00',84,1388221831,1388221831,1),
 (85,3,'ai cũng phải cười 19','',5,'Dirctor 17','Davil vila','2013-11-26 00:00:00',85,1388221852,1388221852,1),
 (86,3,'ai cũng phải cười 19','',34,'Dirctor 17','Davil vila','2013-11-25 00:00:00',86,1388221875,1388221875,1),
 (87,3,'ai cũng phải cười 20','',4,'Dirctor 17','Author','2013-12-18 00:00:00',87,1388221901,1388221901,1),
 (88,3,'ai cũng phải cười 21','',5,'Dirctor 10','Davil vila','2013-11-26 00:00:00',88,1388221922,1388221922,1),
 (89,3,'ai cũng phải cười 22','',4,'Dirctor 10','Author 17','2013-11-26 00:00:00',89,1388221944,1388221944,1),
 (90,3,'ai cũng phải cười 23','',5,'Dirctor 17','Author 17','2013-12-11 00:00:00',90,1388221966,1388221966,1),
 (91,3,'ai cũng phải cười 12','',4,'Dirctor 17','Author 17','2013-11-26 00:00:00',91,1388222009,1388222009,1),
 (92,3,'ai cũng phải cười 24','',4,'4','Davil vila','2013-11-27 00:00:00',92,1388222034,1388222034,1),
 (93,3,'ai cũng phải cười 25','',4,'Dirctor 11','Davil vila','2013-11-27 00:00:00',93,1388222058,1388222058,1),
 (94,3,'ai cũng phải cười 26','',6,'Dirctor 17','Author 17','2013-11-27 00:00:00',94,1388222094,1388222094,1),
 (95,3,'ai cũng phải cười 27','',6,'Wiliam','gradgreed','2013-12-18 00:00:00',95,1388222118,1388222118,1),
 (96,3,'ai cũng phải cười 28','',6,'Wiliam','Author 17','2013-11-26 00:00:00',96,1388222140,1388222140,1),
 (97,3,'ai cũng phải cười 29','',7,'Dirctor 17','Davil vila','2013-11-25 00:00:00',97,1388222165,1388222165,1),
 (98,3,'ai cũng phải cười 30','',4,'Dirctor 1','Davil vila','2013-11-27 00:00:00',98,1388222188,1388222188,1),
 (99,3,'ai cũng phải cười 31','',4,'Wiliam','Author 17','2013-12-04 00:00:00',99,1388222207,1388222207,1),
 (100,3,'ai cũng phải cười 36','',6,'Dirctor 10','Author 17','2013-11-26 00:00:00',100,1388222230,1388222230,1),
 (101,3,'ai cũng phải cười 40','',3,'Dirctor 12','Davil vila','2013-11-25 00:00:00',101,1388222254,1388222254,1);
/*!40000 ALTER TABLE `films` ENABLE KEYS */;


--
-- Definition of table `films_category`
--

DROP TABLE IF EXISTS `films_category`;
CREATE TABLE `films_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `film_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `updated_date` int(10) DEFAULT NULL,
  `created_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `films_category`
--

/*!40000 ALTER TABLE `films_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `films_category` ENABLE KEYS */;


--
-- Definition of table `packet_of_cretdit`
--

DROP TABLE IF EXISTS `packet_of_cretdit`;
CREATE TABLE `packet_of_cretdit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `packet_of_cretdit`
--

/*!40000 ALTER TABLE `packet_of_cretdit` DISABLE KEYS */;
/*!40000 ALTER TABLE `packet_of_cretdit` ENABLE KEYS */;


--
-- Definition of table `payment_historys`
--

DROP TABLE IF EXISTS `payment_historys`;
CREATE TABLE `payment_historys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_User` int(11) NOT NULL DEFAULT '0',
  `real_Price` decimal(10,0) DEFAULT NULL,
  `id_Packet` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_historys`
--

/*!40000 ALTER TABLE `payment_historys` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_historys` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `credit` int(11) NOT NULL DEFAULT '0',
  `address` varchar(500) DEFAULT NULL,
  `activation_key` varchar(32) DEFAULT NULL,
  `updated_date` int(10) DEFAULT NULL,
  `created_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


--
-- Definition of table `watch_full_history`
--

DROP TABLE IF EXISTS `watch_full_history`;
CREATE TABLE `watch_full_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `latest_date` date NOT NULL,
  `watched_times` int(11) NOT NULL,
  `real_credit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `watch_full_history`
--

/*!40000 ALTER TABLE `watch_full_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `watch_full_history` ENABLE KEYS */;


--
-- Definition of table `watch_trainer_history`
--

DROP TABLE IF EXISTS `watch_trainer_history`;
CREATE TABLE `watch_trainer_history` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_User` int(10) unsigned NOT NULL,
  `id_Film` int(10) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `times` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `watch_trainer_history`
--

/*!40000 ALTER TABLE `watch_trainer_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `watch_trainer_history` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
