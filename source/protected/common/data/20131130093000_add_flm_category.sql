CREATE TABLE `category`
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(64) NOT NULL,
	`description` text NULL,
	`updated_date` int(10) NULL,
	`created_date` int(10) NOT NULL,
	`status` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `films_category`
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`film_id` int(11) NOT NULL,
	`category_id` int(11) NOT NULL,
	`updated_date` int(10) NULL,
	`created_date` int(10) NOT NULL,
	`status` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);