CREATE TABLE `films`
(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(64) NOT NULL,
	`description` text NULL,
    `credits` int(11) NOT NULL DEFAULT 0,
	`director` varchar(64) NOT NULL,
	`author` varchar(32) NOT NULL,
	`date_publish` datetime NOT NULL,
	`path` text NOT NULL,
	`updated_date` int(10) NULL,
	`created_date` int(10) NOT NULL,
	`status` tinyint(1) NOT NULL,
	PRIMARY KEY (`id`)
);