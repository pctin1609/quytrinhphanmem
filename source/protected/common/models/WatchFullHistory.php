<?php
/**
* This is the model class for table "watch_full_history".
*
* The followings are the available columns in table 'watch_full_history':
* @property integer $id
* @property integer $user_id
* @property integer $film_id
* @property string $latest_date
* @property integer $watched_times
* @property integer $real_credit
*/
class WatchFullHistory extends CActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return WatchFullHistory the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'watch_full_history';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('user_id, film_id, latest_date, watched_times, real_credit', 'required'),
			array('user_id, film_id, watched_times, real_credit', 'numerical', 'integerOnly' => true),
			array('id, user_id, film_id, latest_date, watched_times, real_credit', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
        return array(
            'film' => array(self::BELONGS_TO, 'Film', 'film_id'),
		);
//		return array(
//		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'film_id' => 'Film',
			'latest_date' => 'Latest Date',
			'watched_times' => 'Watched Times',
            'real_credit' => 'Real Credit',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('film_id', $this->film_id);
		$criteria->compare('latest_date', $this->latest_date, true);
		$criteria->compare('watched_times', $this->watched_times);
                $criteria->compare('real_credit', $this->real_credit);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}