<?php
/**
* This is the model class for table "download_history".
*
* The followings are the available columns in table 'download_history':
* @property integer $id
* @property integer $user_id
* @property integer $film_id
* @property string $created_date
*/
class DownloadHistory extends ActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return DownloadHistory the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'download_history';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
                        array('user_id, film_id', 'required'),
			array('user_id, film_id,created_date', 'numerical', 'integerOnly' => true),
			array('created_date', 'safe'),
			array('id, user_id, film_id, created_date', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
            'film' => array(self::BELONGS_TO, 'Film', 'film_id'),
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'film_id' => 'Film',
			'created_date' => 'Created Date',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('film_id', $this->film_id);
		$criteria->compare('created_date', $this->created_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}