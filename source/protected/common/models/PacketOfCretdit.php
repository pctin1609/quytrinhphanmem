<?php
/**
* This is the model class for table "packet_of_cretdit".
*
* The followings are the available columns in table 'packet_of_cretdit':
* @property integer $id
* @property integer $amount
* @property string $price
*/
class PacketOfCretdit extends ActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return PacketOfCretdit the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'packet_of_cretdit';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('amount', 'numerical', 'integerOnly' => true),
			array('price', 'length', 'max' => 10),
			array('id, amount, price', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'amount' => 'Amount',
			'price' => 'Price',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('amount', $this->amount);
		$criteria->compare('price', $this->price, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}