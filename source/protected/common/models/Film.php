<?php

/**
 * This is the model class for table "films".
 *
 * The followings are the available columns in table 'films':
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $description
 * @property integer $credits
 * @property string $director
 * @property string $author
 * @property string $date_publish
 * @property integer $file_id
 * @property integer $updated_date
 * @property integer $created_date
 * @property integer $status
 */
class Film extends ActiveRecord
{
    public $thumb;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Film the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'films';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('category_id, credits, title, director, author, date_publish, file_id, status', 'required'),
            array('category_id, credits, file_id, updated_date, created_date, status', 'numerical', 'integerOnly' => true),
            array('title, director', 'length', 'max' => 64),
            array('author', 'length', 'max' => 32),
            array('thumb', 'file', 'types' => 'jpg, jpeg, png', 'maxSize' => 1024 * 5, 'allowEmpty' => true),
            array('description, thumb', 'safe'),
            array('id, category_id, title, description, credits, director, author, date_publish, file_id, updated_date, created_date, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'file' => array(self::BELONGS_TO, 'File', 'file_id'),
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'category_id' => 'Category',
            'title' => 'Title',
            'description' => 'Description',
            'credits' => 'Credits',
            'director' => 'Director',
            'author' => 'Author',
            'date_publish' => 'Date Publish',
            'file_id' => 'File',
            'thumb' => 'Thumb',
            'updated_date' => 'Updated Date',
            'created_date' => 'Created Date',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('credits', $this->credits);
        $criteria->compare('director', $this->director, true);
        $criteria->compare('author', $this->author, true);
        $criteria->compare('date_publish', $this->date_publish, true);
        $criteria->compare('file_id', $this->file_id);
        $criteria->compare('updated_date', $this->updated_date);
        $criteria->compare('created_date', $this->created_date);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize'),
            ),
        ));
    }

}