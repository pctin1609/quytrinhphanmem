<?php

/**
 * This is the model class for table "files".
 *
 * The followings are the available columns in table 'files':
 * @property string $id
 * @property string $name
 * @property string $source
 * @property integer $size
 * @property string $extension
 */
class File extends ActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return File the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'files';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, source, size, extension', 'required'),
            array('size', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 100),
            array('source', 'length', 'max' => 200),
            array('extension', 'length', 'max' => 5),
            array('id, name, source, size, extension', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'fileMeta' => array(self::HAS_ONE, 'FileMeta', 'file_id'),
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'source' => 'Source',
            'size' => 'Size',
            'extension' => 'Extension',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('source', $this->source, true);
        $criteria->compare('size', $this->size);
        $criteria->compare('extension', $this->extension, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => param('pageSize'),
                    ),
                ));
    }

    public function afterDelete()
    {
        $ref = parent::afterDelete();
        $uploadFolder = Utils::uploadPath();

        if (is_file($uploadFolder . $this->source))
            unlink($uploadFolder . $this->source);

        return $ref;
    }

}