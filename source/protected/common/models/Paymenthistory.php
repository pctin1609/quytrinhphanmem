<?php
/**
* This is the model class for table "payment_historys".
*
* The followings are the available columns in table 'payment_historys':
* @property integer $id
* @property integer $id_User
* @property string $real_Price
* @property integer $id_Packet
* @property string $date
*/
class PaymentHistory extends ActiveRecord
{
	/**
	* Returns the static model of the specified AR class.
	* @param string $className active record class name.
	* @return PaymentHistory the static model class
	*/
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	* @return string the associated database table name
	*/
	public function tableName()
	{
		return 'payment_historys';
	}

	/**
	* @return array validation rules for model attributes.
	*/
	public function rules()
	{
		return array(
			array('date', 'required'),
			array('id_User, id_Packet', 'numerical', 'integerOnly' => true),
			array('real_Price', 'length', 'max' => 10),
			array('id, id_User, real_Price, id_Packet, date', 'safe', 'on' => 'search'),
		);
	}

	/**
	* @return array relational rules.
	*/
	public function relations()
	{
		return array(
            'Packet' => array(self::BELONGS_TO, 'PacketOfCretdit', 'id_Packet'),
		);
	}

	/**
	* @return array customized attribute labels (name => label)
	*/
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_User' => 'Id User',
			'real_Price' => 'Real Price',
			'id_Packet' => 'Id Packet',
			'date' => 'Date',
		);
	}

	/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('id_User', $this->id_User);
		$criteria->compare('real_Price', $this->real_Price, true);
		$criteria->compare('id_Packet', $this->id_Packet);
		$criteria->compare('date', $this->date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => param('pageSize'),
			),
		));
	}
}