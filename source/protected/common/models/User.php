<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property integer $credit
 * @property string $address
 * @property integer $updated_date
 * @property integer $created_date
 * @property integer $status
 */
class User extends ActiveRecord
{
    public $passwordConfirm;
    public $verify_code;
    public $term_condition;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            // Required
            array('email, first_name, last_name', 'required'),
            array('phone, address', 'safe', 'on' => 'add, create, register'),
            array('password, passwordConfirm', 'required', 'on' => 'add'),
            array('verify_code, term_condition', 'required', 'on' => 'register'),
            array('password', 'required', 'on' => 'create'),
            // Length
            array('email', 'length', 'max' => 200),
            array('password', 'length', 'min' => 6),
            array('phone', 'length', 'max' => 20),
            array('address', 'length', 'max' => 500),
            array('first_name, last_name', 'length', 'max' => 50),
            // Others
            array('email', 'email'),
            array('credit', 'safe'),
            array('email', 'unique', 'on' => 'create, register'),
            array('verify_code', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'register'),
            array('password', 'match', 'pattern' => '/^[A-Za-z0-9_!@#$%^&*()+=?.,]+$/u', 'message' => 'Spaces characters are not allowed', 'on' => 'add'),
            array('passwordConfirm', 'compare', 'compareAttribute' => 'password', 'on' => 'add'),
            array('status, updated_date, created_date', 'numerical', 'integerOnly' => true),
            array('id, email, password, first_name, last_name, phone, address, updated_date, created_date, status', 'safe', 'on' => 'search'),
        );
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->password = Utils::md5($this->password);
            $this->status = User::STATUS_ACTIVE;
        }
        return parent::beforeSave();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => "ID",
            'email' => "Email",
            'password' => "Password",
            'passwordConfirm' => "Password Confirm",
            'first_name' => "First Name",
            'last_name' => "Last Name",
            'phone' => "Phone",
            'credit' => 'Credit',
            'address' => "Address",
            'updated_date' => 'Updated Date',
            'created_date' => 'Created Date',
            'status' => "Status",
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('credit', $this->credit, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('updated_date', $this->updated_date);
        $criteria->compare('created_date', $this->created_date);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize'),
            ),
        ));
    }

    public function getFullname()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

}