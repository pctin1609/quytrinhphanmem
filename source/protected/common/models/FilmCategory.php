<?php

/**
 * This is the model class for table "films_category".
 *
 * The followings are the available columns in table 'films_category':
 * @property integer $id
 * @property integer $film_id
 * @property integer $category_id
 * @property integer $updated_date
 * @property integer $created_date
 * @property integer $status
 */
class FilmCategory extends ActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return FilmCategory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'films_category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('film_id, category_id, created_date, status', 'required'),
            array('film_id, category_id, updated_date, created_date, status', 'numerical', 'integerOnly' => true),
            array('id, film_id, category_id, updated_date, created_date, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'film_id' => 'Film',
            'category_id' => 'Category',
            'updated_date' => 'Updated Date',
            'created_date' => 'Created Date',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('film_id', $this->film_id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('updated_date', $this->updated_date);
        $criteria->compare('created_date', $this->created_date);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => param('pageSize'),
                    ),
                ));
    }

}