function topmenu() {
	var $navSubMenu = $('[aria-haspopup="true"] > a'), onSubMenuClick = function() {
		$(this).parent('li').toggleClass('is-open').find('.sub-menu').attr('aria-hidden', !$(this).hasClass('is-open'));
		return false;
	};
	// Using .click to comply with legacy jQuery 1.2something
	// This is UNACCEPTABLE and should be fixed ASAP
	$navSubMenu.click(onSubMenuClick);
	if ($navSubMenu.length) {
		// Hide "more" menu when clicking outside of it
		$(document).click(function(e) {
			if (e.target !== $navSubMenu.children('a:first-child').get(0)) {
				$navSubMenu.parent('li').removeClass('is-open').find('.sub-menu').attr('aria-hidden', true);
			}
		});
		$(function() {
			$('iframe').bind('load', function() {
				try {
					var $doc = $(this).contents();
					if ($doc) {
						$doc.click(function() {
							$navSubMenu.parent('li').removeClass('is-open').find('.sub-menu').attr('aria-hidden', true);
						});
					}
				} catch(e) {
				}
			});
		});
	}
};

$('document').ready(function() {
	$('ul.list li:first-child').addClass('first');
	$('ul.list li:last-child').addClass('last');
	topmenu();
});
